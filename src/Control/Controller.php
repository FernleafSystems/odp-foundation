<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\OdpFoundationClasses\Control;

use FernleafSystems\Wordpress\Services\Services;

class Controller extends \FernleafSystems\Wordpress\Plugin\Foundation\Control\Controller {

	public function onWpShutdown() {
		$this->maintainPluginLoadPosition();
		parent::onWpShutdown();
	}

	/**
	 * Move this plugin to always load last (and ensure its autoload lib takes precedence)
	 */
	private function maintainPluginLoadPosition() {
		$WPP = Services::WpPlugins();
		$rootFile = $this->getRootFile()->getPluginBaseFile();
		$active = \is_array( $WPP->getActivePlugins() ) ? $WPP->getActivePlugins() : [];
		if ( \array_key_last( \array_flip( $active ) ) != $rootFile ) {
			$WPP->setActivePluginLoadLast( $rootFile );
		}
	}
}