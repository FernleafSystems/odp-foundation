<?php

namespace FernleafSystems\Wordpress\OdpFoundationClasses\ExchangeRates;

class ExchangeRatesApiProvider extends \Ultraleet\CurrencyRates\Providers\FixerProvider {
	protected $url = 'https://api.exchangeratesapi.io';
}