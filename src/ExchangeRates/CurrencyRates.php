<?php

namespace FernleafSystems\Wordpress\OdpFoundationClasses\ExchangeRates;

/**
 * Extends the original CurrencyRates to provide access to ExchangeRatesApi Driver
 */
class CurrencyRates extends \Ultraleet\CurrencyRates\CurrencyRates {

	/**
	 * @return ExchangeRatesApiProvider
	 */
	protected function createEraDriver() {
		return new ExchangeRatesApiProvider( new \GuzzleHttp\Client() );
	}
}