<?php declare( strict_types=1 );
/** @var string $rootFile */

/** NEVER RENAME THIS GLOBAL */
global $o1dpFoundationController;
if ( isset( $o1dpFoundationController ) ) {
	error_log( 'Attempting to load the ODP Foundation Classes plugin twice?' );
	return;
}

require_once( __DIR__.'/vendor/autoload.php' );

try {
	$o1dpFoundationController = new \FernleafSystems\Wordpress\OdpFoundationClasses\Control\Controller(
		new \FernleafSystems\Wordpress\Plugin\Foundation\Root\File( $rootFile )
	);
	$o1dpFoundationController->setFoundationController( $o1dpFoundationController );
}
catch ( Exception $e ) {
	error_log( $rootFile.': '.$e->getMessage() );
//	echo $e->getMessage();
}