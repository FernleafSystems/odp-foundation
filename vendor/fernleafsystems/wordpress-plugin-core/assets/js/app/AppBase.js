import { BaseAutoExecComponent } from "../components/BaseAutoExecComponent";
import { AptoEventsHandler } from "../services/AptoEventsHandler";

export class AppBase extends BaseAutoExecComponent {

	canRun() {
		return this.getPrimaryVarName() in window
	}

	getPrimaryVarName() {
		return 'apto_vars_main_override';
	}

	getPrimaryVar() {
		return window[ this.getPrimaryVarName() ];
	}

	run() {
		window.addEventListener( 'load', () => {
			this.initEvents();
			this.initComponents();
		}, false );
	}

	initComponents() {
		this.components = {};
		const comps = this.getPrimaryVar().comps;
	}

	initEvents() {
		if ( !( 'aptoEventsHandler' in global ) ) {
			global.aptoEventsHandler = new AptoEventsHandler( {
				events_container_selector: 'body'
			} );
		}
	}

	getComponent( component ) {
		return component in this.components ? this.components[ component ] : null;
	}
}