import { AppMainBase } from "./AppMainBase";
import { AptoPluginServices } from "../services/AptoPluginServices";
import { BootstrapTooltips } from "../components/ui/BootstrapTooltips";
import { OffCanvasService } from "../components/ui/OffCanvasService";
import { OptionsHandler } from "../components/options/OptionsHandler";
import { Navigation } from "../components/general/Navigation";
import { SuperSearchService } from "../components/search/SuperSearchService";

export class AppMainPluginBase extends AppMainBase {

	initComponents() {
		this.components = {};

		const comps = this.getPrimaryVar().comps;

		this.components.offcanvas = new OffCanvasService();
		this.components.bootstrap_tooltips = new BootstrapTooltips();

		this.components.mod_options = ( 'mod_options' in comps ) ? new OptionsHandler( comps.mod_options ) : null;
		this.components.navi = ( 'navi' in comps ) ? new Navigation( comps.navi ) : null;
		this.components.super_search = ( 'super_search' in comps ) ? new SuperSearchService( comps.super_search ) : null;
	}

	initEvents() {
		super.initEvents();
		if ( !( 'aptoPluginServices' in global ) ) {
			global.aptoPluginServices = new AptoPluginServices();
		}
	}
}