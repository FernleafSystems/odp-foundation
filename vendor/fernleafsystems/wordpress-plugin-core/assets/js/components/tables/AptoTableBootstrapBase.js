import 'datatables.net-bs5';
import 'datatables.net-buttons-bs5';
import 'datatables.net-searchpanes-bs5';
import 'datatables.net-select-bs5';
import { AptoTableBase } from "./AptoTableBase";

export class AptoTableBootstrapBase extends AptoTableBase {
}