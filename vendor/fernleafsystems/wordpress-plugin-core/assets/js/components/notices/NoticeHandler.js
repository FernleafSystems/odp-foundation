import { AjaxService } from "../services/AjaxService";
import { BaseComponent } from "../BaseComponent";
import { ObjectOps } from "../../util/ObjectOps";

export class NoticeHandler extends BaseComponent {

	init() {
		aptoEventsHandler.add_Click( 'a.apto_admin_notice_action', ( targetEl ) => {
			( new AjaxService() )
			.send( this._base_data.ajax[ targetEl.dataset.notice_action ] )
			.finally();
		} );

		aptoEventsHandler.add_Click( '.apto-notice-container .apto-notice-dismiss', ( targetEl ) => this.sendDismiss( targetEl ) );
		aptoEventsHandler.add_Click( '.apto-notice-container .notice-dismiss', ( targetEl ) => this.sendDismiss( targetEl ) );
	}

	sendDismiss( targetEl ) {
		const container = targetEl.closest( '.shield-notice-container' );
		( new AjaxService() )
		.bg( ObjectOps.Merge( this._base_data.ajax.dismiss_admin_notice, container.dataset ) )
		.finally( () => container.remove() );
	}
}