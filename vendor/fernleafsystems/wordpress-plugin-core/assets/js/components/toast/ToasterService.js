import $ from 'jquery';
import { Toast } from 'bootstrap';

export class ToasterService {

	showMessage( msg, success ) {
		let toastDIV = document.getElementById( 'aptoToast' ) || false;
		if ( toastDIV ) {

			if ( !this.toastie ) {
				toastDIV.addEventListener( 'hidden.bs.toast', () => {
					toastDIV.style[ 'z-index' ] = -10;
				} )
			}

			this.toastie = Toast.getOrCreateInstance( toastDIV, {
				autohide: true,
				delay: 3000
			} );

			const $toast = $( toastDIV );
			$toast.removeClass( 'text-bg-success text-bg-warning' );
			$toast.addClass( success ? 'text-bg-success' : 'text-bg-warning' );
			let $toastBody = $( '.toast-body', this.$toast );
			$toastBody.html( '' );
			$( '<span></span>' )
			.html( msg )
			.appendTo( $toastBody );
			$toast.css( 'z-index', 100000000 );

			this.toastie.show();
		}
	};
}