<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Config\Modules\Ops;

class StringsModules extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	/**
	 * @return array{name:string, subtitle:string, description:string[]}
	 */
	public function getFor( string $modSlug ) :array {
		$cfg = $this->ctr()->modules[ $modSlug ]->cfg;
		return [
			'name'        => __( $cfg->properties[ 'name' ], 'wp-simple-firewall' ),
			'subtitle'    => __( $cfg->properties[ 'tagline' ] ?? '', 'wp-simple-firewall' ),
			'description' => [],
		];
	}
}