<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Config\Modules\Ops;

class StringsSections extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	/**
	 * @return array{title:string, title_short:string, summary:array}
	 */
	public function getFor( string $key ) :array {
		$def = $this->con()->cfg->configuration->sections[ $key ];
		return [
			'title'       => __( $def[ 'title' ] ?? 'No Title', $this->con()->cfg->properties[ 'language' ] ?? '' ),
			'title_short' => __( $def[ 'title_short' ] ?? 'No Title', $this->con()->cfg->properties[ 'language' ] ?? '' ),
			'summary'     => $def[ 'summary' ] ?? [],
		];
	}
}