<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Config\Modules\Ops;

use FernleafSystems\Wordpress\Services\Services;

class BuildOptionsForFormDisplay extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	private $focusOption;

	private $focusSection;

	private $modSlug;

	/**
	 * Will initiate the plugin options structure for use by the UI builder.
	 * It doesn't set any values, just populates the array created in buildOptions()
	 * with values stored.
	 * It has to handle the conversion of stored values to data to be displayed to the user.
	 */
	public function standard( string $modSlug, string $focusSection = '', string $focusOption = '' ) :array {

		$this->modSlug = $modSlug;
		$this->focusSection = $focusSection;
		$this->focusOption = $focusOption;

		return \array_filter( \array_map(
			function ( array $section ) {
				$notices = new SectionNotices();

				foreach ( $section[ 'options' ] as $optKey => $opt ) {
					$opt[ 'is_value_default' ] = $opt[ 'value' ] === $opt[ 'default' ];
					$section[ 'options' ][ $optKey ] = $this->buildOptionForUi( $opt );
					$section[ 'options' ][ $optKey ][ 'is_focus' ] = $opt[ 'key' ] === $this->focusOption;
				}

				if ( empty( $section[ 'options' ] ) ) {
					$section = null;
				}
				else {
					/** @var StringsSections $stringsOptions */
					$stringsSections = $this->ctr()->get( 'handler.strings.mod_sections' );
					$section = \array_merge( $section, $stringsSections->getFor( $section[ 'slug' ] ) );
					$section[ 'is_focus' ] = $section[ 'slug' ] === $this->focusSection;
					$section[ 'notices' ] = $notices->notices( $section[ 'slug' ] );
					$section[ 'warnings' ] = $notices->warnings( $section[ 'slug' ] );
					$section[ 'critical_warnings' ] = $notices->critical( $section[ 'slug' ] );
				}

				return $section;
			},
			$this->buildAvailableSections()
		) );
	}

	protected function buildAvailableSections() :array {
		return \array_filter( \array_map(
			function ( array $nonHiddenSection ) {

				$nonHiddenSection = \array_merge( [
					'primary'   => false,
					'options'   => $this->buildOptionsForSection( $nonHiddenSection[ 'slug' ] ),
					'beacon_id' => false,
				], $nonHiddenSection );

				if ( $this->con()->labels->is_whitelabelled ) {
					$nonHiddenSection[ 'beacon_id' ] = false;
				}

				return empty( $nonHiddenSection[ 'options' ] ) ? null : $nonHiddenSection;
			},
			$this->con()->cfg->configuration->sectionsForModule( $this->modSlug )
		) );
	}

	protected function buildOptionsForSection( string $section ) :array {
		$con = $this->con();

		$isPremiumActive = $con->isPremiumActive();

		$allOptions = [];

		foreach ( $section === 'section_hidden' ? [] : $con->cfg->configuration->optsForSection( $section ) as $optDef ) {

			if ( $optDef[ 'section' ] !== $section ) {
				continue;
			}

			$optDef = \array_merge( [
				'link_info'     => '',
				'link_blog'     => '',
				'value_options' => [],
				'premium'       => false,
				'advanced'      => false,
				'beacon_id'     => false
			], $optDef );

			$optDef[ 'value' ] = $this->ctr()->opts->optGet( $optDef[ 'key' ] );

			if ( \in_array( $optDef[ 'type' ], [ 'select', 'multiple_select' ] ) ) {
				$available = [];
				$converted = [];
				foreach ( $optDef[ 'value_options' ] as $valueOpt ) {

					$isDisabled = ( !empty( $valueOpt[ 'premium' ] ) && !$isPremiumActive )
								  || ( !empty( $valueOpt[ 'cap' ] ) && !$this->ctr()->caps->hasCap( $valueOpt[ 'cap' ] ) );

					$converted[ $valueOpt[ 'value_key' ] ] = [
						'name'         => esc_html( __( $valueOpt[ 'text' ], 'wp-simple-firewall' ) ),
						'is_available' => !$isDisabled,
					];

					if ( $converted[ $valueOpt[ 'value_key' ] ][ 'is_available' ] ) {
						$available[] = $valueOpt[ 'value_key' ];
					}
				}
				$optDef[ 'value_options' ] = $converted;

				/** For multi-selects, only show available options as checked on. */
				if ( \is_array( $optDef[ 'value' ] ) ) {
					$optDef[ 'value' ] = \array_intersect( $optDef[ 'value' ], $available );
				}
			}

			if ( $con->labels->is_whitelabelled ) {
				$optDef[ 'beacon_id' ] = false;
			}

			$allOptions[] = $optDef;
		}
		return $allOptions;
	}

	protected function buildOptionForUi( array $option ) :array {
		$value = $option[ 'value' ];

		switch ( $option[ 'type' ] ) {

			case 'password':
				if ( !empty( $value ) ) {
					$value = '';
				}
				break;

			case 'array':
				if ( empty( $value ) || !\is_array( $value ) ) {
					$value = [];
				}
				$option[ 'rows' ] = \count( $value ) + 2;
				$value = \stripslashes( \implode( "\n", $value ) );
				break;

			case 'multiple_select':
				if ( !\is_array( $value ) ) {
					$value = [];
				}
				break;

			case 'text':
				$value = \stripslashes( $this->ctr()->opts->optGet( $option[ 'key' ] ) );
				break;
		}

		$isOptDisabled = ( !empty( $option[ 'premium' ] ) && !$this->con()->isPremiumActive() )
						 || ( !empty( $option[ 'cap' ] ) && !$this->ctr()->caps->hasCap( $option[ 'cap' ] ) );
		$params = [
			'value'    => \is_scalar( $value ) ? esc_attr( $value ) : $value,
			'disabled' => $isOptDisabled,
		];
		$params[ 'enabled' ] = !$params[ 'disabled' ];
		$option = \array_merge( [ 'rows' => '2' ], $option, $params );

		// add strings
		try {
			/** @var StringsOptions $stringsOptions */
			$stringsOptions = $this->ctr()->get( 'handler.strings.mod_options' );
			$optStrings = $stringsOptions->getFor( $option[ 'key' ] );
			if ( !\is_array( $optStrings[ 'description' ] ) ) {
				$optStrings[ 'description' ] = [ $optStrings[ 'description' ] ];
			}
			$option = Services::DataManipulation()->mergeArraysRecursive( $option, $optStrings );
		}
		catch ( \Exception $e ) {
		}

		return $this->addPerOptionCustomisation( $option );
	}

	private function addPerOptionCustomisation( array $option ) :array {
		return $option;
	}
}