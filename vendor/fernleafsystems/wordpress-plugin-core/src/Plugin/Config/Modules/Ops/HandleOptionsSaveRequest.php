<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Config\Modules\Ops;

class HandleOptionsSaveRequest extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	private $form;

	public function __construct( array $form = null ) {
		$this->form = $form;
	}

	public function handleSave() :bool {
		try {
			if ( !$this->con()->isPluginAdmin() ) {
				throw new \Exception( __( "You don't currently have permission to save settings.", 'wp-simple-firewall' ) );
			}
			if ( empty( $this->form[ 'all_opts_keys' ] ) ) {
				throw new \Exception( 'all_opts_keys form element not provided.' );
			}
			$this->storeOptions();

			do_action( $this->ctr()->prefix->hook( 'after_form_submit_options_save' ), $this->form );

			$success = true;
		}
		catch ( \Exception $e ) {
			$success = false;
		}
		return $success;
	}

	/**
	 * @throws \Exception
	 */
	private function storeOptions() {

		$optsCon = $this->ctr()->opts;

		foreach ( \explode( ',', $this->form[ 'all_opts_keys' ] ?? [] ) as $optKey ) {

			if ( !$optsCon->optExists( $optKey ) || $optsCon->optDef( $optKey )[ 'section' ] === 'section_hidden' ) {
				continue;
			}

			$optType = $optsCon->optType( $optKey );
			if ( $optType === 'noneditable_text' ) {
				continue;
			}

			$optValue = $this->form[ $optKey ] ?? null;
			if ( \is_null( $optValue ) ) {

				if ( \in_array( $optType, [ 'text', 'email' ] ) ) { //text box, and it's null, don't update
					continue;
				}
				elseif ( $optType == 'checkbox' ) { //if it was a checkbox, and it's null, it means 'N'
					$optValue = 'N';
				}
				elseif ( $optType == 'integer' ) { //if it was a integer, and it's null, it means '0'
					$optValue = 0;
				}
				elseif ( $optType == 'multiple_select' ) {
					$optValue = [];
				}
			}
			elseif ( $optType == 'password' ) {
				$tempValue = \trim( $optValue );
				if ( empty( $tempValue ) ) {
					continue;
				}

				$confirm = $this->form[ $optKey.'_confirm' ] ?? null;
				if ( $tempValue !== $confirm ) {
					throw new \Exception( __( 'Password values do not match.', 'wp-simple-firewall' ) );
				}

				$optValue = \md5( $tempValue );
			}
			elseif ( $optType == 'array' ) { //arrays are textareas, where each is separated by newline
				$optValue = \array_filter( \explode( "\n", esc_textarea( $optValue ) ), '\trim' );
			}

			$optsCon->optSet( $optKey, $optValue );
		}

		$optsCon->store();
	}
}