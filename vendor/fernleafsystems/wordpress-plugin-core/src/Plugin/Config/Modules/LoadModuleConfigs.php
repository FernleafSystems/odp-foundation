<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Config\Modules;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Exceptions\PluginConfigInvalidException;

class LoadModuleConfigs extends PluginControllerConsumerBase {

	/**
	 * @throws PluginConfigInvalidException
	 */
	public function run() :ConfigurationVO {
		$cfgSpec = $this->con()->cfg->config_spec ?? null;

		if ( !\is_array( $cfgSpec ) ) {
			throw new PluginConfigInvalidException( 'invalid specification of modules' );
		}
		if ( empty( $cfgSpec[ 'modules' ] ) ) {
			throw new PluginConfigInvalidException( 'No modules specified in the plugin config.' );
		}

		/** @var array[] $defaultConfig */
		$defaultConfig = $this->ctr()->get( 'cfg.default_modules_config' );

		$normalizer = new NormaliseConfigComponents();
		$configuration = ( new ConfigurationVO() )->applyFromArray( $cfgSpec );
		$configuration->modules = $normalizer->indexModules( \array_merge( $defaultConfig[ 'modules' ], $configuration->modules ) );
		$configuration->sections = $normalizer->indexSections( \array_merge( $defaultConfig[ 'sections' ] ?? [], $configuration->sections ) );
		$configuration->options = $normalizer->indexOptions( \array_merge( $defaultConfig[ 'options' ] ?? [], $configuration->options ) );

		/**
		 * There's always at least 1 module: the 'plugin' module. In here we can create a core operation of the plugin
		 * that is outside the Controller, but typically common to all Apto plugins.
		 */
		$modules = [];
		foreach ( $configuration->modules as $slug => $moduleProps ) {
			$modules[ $slug ] = \array_merge( [
				'storage_key'   => $slug,
				'load_priority' => 10,
				'menu_priority' => 100,
			], $moduleProps );
		}

		// Order Modules based on their load priority
		\uasort( $modules, function ( array $a, array $b ) {
			if ( $a[ 'load_priority' ] == $b[ 'load_priority' ] ) {
				return 0;
			}
			return ( $a[ 'load_priority' ] < $b[ 'load_priority' ] ) ? -1 : 1;
		} );

		$configuration->modules = $modules;

		// Set a primary section for each module, if there isn't 1 set.
		foreach ( \array_keys( $configuration->modules ) as $modSlug ) {
			$hasPrimary = false;
			foreach ( $configuration->sections as $section ) {
				if ( $section[ 'module' ] == $modSlug && !empty( $section[ 'primary' ] ) ) {
					$hasPrimary = true;
					break;
				}
			}
			if ( !$hasPrimary ) {
				$sections = $configuration->sections;
				$sections[ \key( $sections ) ][ 'primary' ] = true;
				$configuration->sections = $sections;
			}
		}

		return $configuration;
	}
}