<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Config\Modules;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Exceptions\PluginModuleClassNotDefinedException;
use League\Container\Argument\Literal\ObjectArgument;

class ModulesBuildDefs extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	/**
	 * @throws \Exception
	 */
	public function buildDef( string $slug ) :void {
		$modClass = $this->ctr()->get( 'cfg.enum_modules' )[ $slug ] ?? null;
		// check modClass is not empty or throw exception
		if ( empty( $modClass ) ) {
			throw new PluginModuleClassNotDefinedException( sprintf( 'Module class not found for slug: %s', $slug ) );
		}
		$this->ctr()
			 ->add( 'handler.mod_con.'.$slug, $modClass )
			 ->addArgument( new ObjectArgument( $this->buildCfgForModule( $slug ) ) )
			 ->addTag( 'tag.mod_cons' );
	}

	private function buildCfgForModule( string $slug ) :ModConfigVO {
		$configuration = $this->con()->cfg->configuration;

		$cfg = new ModConfigVO();
		$cfg->slug = $slug;
		$cfg->properties = $configuration->modules[ $slug ];
		$cfg->sections = $configuration->sectionsForModule( $cfg->slug );
		$cfg->options = $configuration->optsForModule( $cfg->slug );

		return apply_filters( $this->ctr()->prefix->hook( 'load_mod_cfg' ), $cfg, $slug );
	}
}