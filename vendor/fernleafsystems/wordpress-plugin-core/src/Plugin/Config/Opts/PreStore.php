<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Config\Opts;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumer;

class PreStore {

	use PluginControllerConsumer;

	public function run() {
	}
}