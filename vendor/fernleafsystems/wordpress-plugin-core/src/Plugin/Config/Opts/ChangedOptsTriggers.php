<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Config\Opts;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumer;

class ChangedOptsTriggers {

	use PluginControllerConsumer;

	public function run( array $changes ) {
	}
}