<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Config;

use FernleafSystems\Wordpress\Services\Services;

class OptsLookup extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	public function getReportEmail() :string {
		return Services::WpGeneral()->getSiteAdminEmail();
	}

	public function isAuthorisedPluginAdmin() :bool {
		$con = $this->con();
		return apply_filters( $con->cntnr->prefix->hook( 'bypass_is_plugin_admin' ), false )
			   || apply_filters( $con->cntnr->prefix->hook( 'is_plugin_admin' ), $con->getMeetsBasePermissions() );
	}

	public function isModEnabled( string $slug ) :bool {
		return $this->ctr()->opts->optIs( 'enable_'.$slug, 'Y' );
	}

	public function isModFromOptEnabled( string $optKey ) :bool {
		return $this->isModEnabled( $this->con()->cfg->configuration->modFromOpt( $optKey ) );
	}

	/**
	 * shortcut for doing optIs() check alongside module enabled check for said option.
	 */
	public function optIsAndModForOptEnabled( string $optKey, $optIs ) :bool {
		return $this->ctr()->opts->optIs( $optKey, $optIs ) && $this->isModFromOptEnabled( $optKey );
	}
}