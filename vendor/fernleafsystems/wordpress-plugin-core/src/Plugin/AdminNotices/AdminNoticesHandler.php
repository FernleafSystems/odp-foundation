<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\AdminNotices;

use FernleafSystems\Utilities\Logic\ExecOnce;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Render\Components\AdminNotice;
use FernleafSystems\Wordpress\Services\Services;
use FernleafSystems\Wordpress\Services\Utilities\Users\UserMeta;

class AdminNoticesHandler extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	use ExecOnce;

	private $count = 0;

	protected function run() {
		if ( $this->con()->getIsPage_PluginAdmin() ) {
			remove_all_filters( 'admin_notices' );
			remove_all_filters( 'network_admin_notices' );
		}
		add_action( 'admin_notices', [ $this, 'onWpAdminNotices' ] );
		add_action( 'network_admin_notices', [ $this, 'onWpNetworkAdminNotices' ] );
		add_filter( 'login_message', [ $this, 'onLoginMessage' ] );
	}

	protected function userMetaAvailable() :bool {
		return !empty( $con->user_metas );
	}

	/**
	 * TODO doesn't handle error message highlighting
	 * @param string $loginMsg
	 * @return string
	 */
	public function onLoginMessage( $loginMsg ) {
		$msg = $this->retrieveFlashMessage();
		if ( \is_array( $msg ) && isset( $msg[ 'show_login' ] ) && $msg[ 'show_login' ] ) {
			$loginMsg .= \sprintf( '<p class="message">%s</p>', sanitize_text_field( $msg[ 'message' ] ) );
			$this->clearFlashMessage();
		}
		return $loginMsg;
	}

	/**
	 * @param \WP_User|null $user
	 * @param bool          $isError
	 * @param bool          $bShowOnLoginPage
	 */
	public function addFlash( string $msg, $user = null, $isError = false, $bShowOnLoginPage = false ) :void {
		if ( $this->userMetaAvailable() ) {
			$con = $this->con();
			$meta = $user instanceof \WP_User ? $con->user_metas->for( $user ) : $con->user_metas->current();

			$msg = \trim( sanitize_text_field( $msg ) );
			if ( !empty( $msg ) && $meta instanceof UserMeta ) {
				$meta->flash_msg = [
					'message'    => sprintf( '[%s] %s', $this->con()->labels->Name, $msg ),
					'expires_at' => Services::Request()->ts() + 20,
					'error'      => $isError,
					'show_login' => $bShowOnLoginPage,
				];
			}
		}
	}

	public function onWpAdminNotices() {
		$this->displayNotices();
	}

	public function onWpNetworkAdminNotices() {
		$this->displayNotices();
	}

	protected function displayNotices() {
		foreach ( $this->buildNotices() as $notice ) {
			echo $this->ctr()->actions->render( AdminNotice::class, [
				'raw_notice_data' => $notice->getRawData()
			] );
		}
	}

	private function getFlashNotice() :?NoticeVO {
		$notice = null;
		if ( $this->userMetaAvailable() ) {
			$msg = $this->retrieveFlashMessage();
			if ( \is_array( $msg ) ) {
				$notice = new NoticeVO();
				$notice->type = $msg[ 'error' ] ? 'error' : 'updated';
				$notice->render_data = [
					'notice_classes' => [
						'flash',
						$notice->type
					],
					'message'        => sanitize_text_field( $msg[ 'message' ] ),
				];
				$notice->template = '/notices/flash-message.twig';
				$notice->display = true;
				$this->clearFlashMessage();
			}
		}
		return $notice;
	}

	private function retrieveFlashMessage() :?array {
		$msg = null;
		if ( $this->userMetaAvailable() ) {
			$meta = $this->con()->user_metas->current();
			if ( !empty( $meta ) && \is_array( $meta->flash_msg ) ) {
				if ( empty( $meta->flash_msg[ 'expires_at' ] ) || Services::Request()
																		  ->ts() < $meta->flash_msg[ 'expires_at' ] ) {
					$msg = $meta->flash_msg;
				}
				else {
					$this->clearFlashMessage();
				}
			}
		}
		return $msg;
	}

	private function clearFlashMessage() :void {
		if ( $this->userMetaAvailable() ) {
			$meta = $this->con()->user_metas->current();
			if ( !empty( $meta ) ) {
				$meta->flash_msg = null;
			}
		}
	}

	/**
	 * @return NoticeVO[]
	 */
	private function buildNotices() :array {
		$notices = \array_filter(
			\array_map(
				function ( NoticeVO $notice ) {
					$this->preProcessNotice( $notice );
					if ( $notice->display ) {
						try {
							$this->processNotice( $notice );
						}
						catch ( \Exception $e ) {
						}
					}
					return $notice;
				},
				$this->getAdminNotices()
			),
			function ( NoticeVO $notice ) {
				return $notice->display;
			}
		);

		$notices[] = $this->getFlashNotice();

		return \array_filter( $notices );
	}

	/**
	 * @return NoticeVO[]
	 */
	public function getAdminNotices() :array {
		return \array_map(
			function ( $noticeDef ) {
				$noticeDef = Services::DataManipulation()->mergeArraysRecursive( [
					'schedule'         => 'conditions',
					'type'             => 'promo',
					'plugin_page_only' => true,
					'valid_admin'      => true,
					'plugin_admin'     => 'yes',
					'can_dismiss'      => true,
					'per_user'         => false,
					'display'          => false,
					'min_install_days' => 0,
					'twig'             => true,
				], $noticeDef );
				return ( new NoticeVO() )->applyFromArray( $noticeDef );
			},
			$this->con()->cfg->configuration->admin_notices
		);
	}

	protected function preProcessNotice( NoticeVO $notice ) :void {
		$con = $this->con();

		$installedAt = $con->cntnr->opts_lookup->getInstalledAt();
		if ( empty( $installedAt ) ) {
			return;
		}
		$installDays = (int)\round( ( Services::Request()->ts() - $installedAt )/\DAY_IN_SECONDS );

		if ( $notice->plugin_page_only && !$con->isPluginAdminPageRequest() ) {
			$notice->non_display_reason = 'plugin_page_only';
		}
		elseif ( $notice->type == 'promo' && !$con->cntnr->opts->optIs( 'enable_upgrade_admin_notice', 'Y' ) ) {
			$notice->non_display_reason = 'promo_hidden';
		}
		elseif ( $notice->valid_admin && !$con->isValidAdminArea() ) {
			$notice->non_display_reason = 'not_admin_area';
		}
		elseif ( $notice->plugin_admin == 'yes' && !$con->isPluginAdmin() ) {
			$notice->non_display_reason = 'not_plugin_admin';
		}
		elseif ( $notice->plugin_admin == 'no' && $con->isPluginAdmin() ) {
			$notice->non_display_reason = 'is_plugin_admin';
		}
		elseif ( $notice->min_install_days > 0 && $notice->min_install_days > $installDays ) {
			$notice->non_display_reason = 'min_install_days';
		}
		elseif ( $this->count > 0 && $notice->type !== 'error' ) {
			$notice->non_display_reason = 'max_nonerror_count';
		}
		elseif ( $notice->can_dismiss && $this->isNoticeDismissed( $notice ) ) {
			$notice->non_display_reason = 'dismissed';
		}
		elseif ( !$this->isDisplayNeeded( $notice ) ) {
			$notice->non_display_reason = 'not_needed';
		}
		else {
			$this->count++;
			$notice->display = true;
			$notice->non_display_reason = 'n/a';
		}

		$notice->template = '/notices/'.$notice->id;
	}

	private function isNoticeDismissed( NoticeVO $notice ) :bool {
		$dismissedUser = $this->isNoticeDismissedForCurrentUser( $notice );

		$at = ( $this->getDismissed()[ $notice->id ] ?? 0 ) > 0;

		if ( !$notice->per_user && $dismissedUser && !$at ) {
			$this->setNoticeDismissed( $notice );
		}

		return $dismissedUser || $at;
	}

	private function isNoticeDismissedForCurrentUser( NoticeVO $notice ) :bool {
		$dismissed = false;

		if ( $this->userMetaAvailable() ) {
			$meta = $this->con()->user_metas->current();
			if ( !empty( $meta ) ) {
				$noticeMetaKey = $this->getNoticeMetaKey( $notice );

				if ( isset( $meta->{$noticeMetaKey} ) ) {
					$dismissed = true;

					// migrate from old-style array storage to plain Timestamp
					if ( \is_array( $meta->{$noticeMetaKey} ) ) {
						$meta->{$noticeMetaKey} = $meta->{$noticeMetaKey}[ 'time' ];
					}
				}
			}
		}

		return $dismissed;
	}

	public function setNoticeDismissed( NoticeVO $notice ) {
		if ( $notice->per_user && $this->userMetaAvailable() ) {
			$meta = $this->con()->user_metas->current();
			$noticeMetaKey = $this->getNoticeMetaKey( $notice );
			if ( !empty( $meta ) ) {
				$meta->{$noticeMetaKey} = Services::Request()->ts();
			}
		}
		else {
			$allDismissed = $this->getDismissed();
			$allDismissed[ $notice->id ] = Services::Request()->ts();
			$this->ctr()->opts->optSet( 'dismissed_notices', $allDismissed );
		}
	}

	/**
	 * @return string[]
	 */
	public function getDismissed() :array {
		return $this->ctr()->opts->optGet( 'dismissed_notices' );
	}

	private function getNoticeMetaKey( NoticeVO $notice ) :string {
		return 'notice_'.\str_replace( [ '-', '_' ], '', $notice->id );
	}

	/**
	 * @throws \Exception
	 */
	private function processNotice( NoticeVO $notice ) :void {
		switch ( $notice->id ) {
			default:
				throw new \Exception( 'Unsupported Notice ID: '.$notice->id );
		}
	}

	protected function isDisplayNeeded( NoticeVO $notice ) :bool {
		switch ( $notice->id ) {
			default:
				$needed = false;
				break;
		}
		return $needed;
	}
}