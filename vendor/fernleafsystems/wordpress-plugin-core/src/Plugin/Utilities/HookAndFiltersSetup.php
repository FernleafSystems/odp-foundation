<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Utilities;

class HookAndFiltersSetup {

	public static function RunHooksSetup( array $hooksDef, $object ) {
		foreach ( $hooksDef as $def ) {
			if ( !empty( $def[ 'cb' ] ) ) {
				$cb = $def[ 'cb' ];
				$cb = \is_callable( $cb ) ? $cb :
					( ( \is_string( $cb ) && \is_object( $object ) && \method_exists( $object, $cb ) ) ?
						[ $object, $cb ] : null );
				if ( !empty( $cb ) ) {
					( $def[ 'type' ] ?? 'action' ) === 'action' ?
						add_action( $def[ 'hook' ], $cb, $cb[ 'p' ] ?? 9, $cb[ 'args' ] ?? 0 ) :
						add_filter( $def[ 'hook' ], $cb, $cb[ 'p' ] ?? 9, $cb[ 'args' ] ?? 0 );
				}
			}
		}
	}
}