<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Request;

/**
 * Data sent in requests via the REST API are already formatted as an array and don't need decoded.
 */
class FormParams {

	public const ENC_NONE = 'none';
	public const ENC_BASE64 = 'b64';
	public const ENC_JSON = 'json';
	public const ENC_OBSCURE = 'obscure';

	public static function Retrieve( ?array $rawReq = null ) :array {
		$formParams = [];

		if ( \is_array( $rawReq ) ) {

			$rawForm = $rawReq[ 'form_params' ] ?? null;
			if ( !empty( $rawForm ) ) {

				if ( \is_string( $rawForm ) ) {
					$formEnc = $rawReq[ 'form_enc' ] ?? null;
					if ( !\is_array( $formEnc ) ) {
						$formEnc = [ self::ENC_BASE64 ];
					}

					if ( \in_array( self::ENC_BASE64, $formEnc ) ) {
						$rawForm = \base64_decode( $rawForm );
					}
					if ( \in_array( self::ENC_OBSCURE, $formEnc ) ) {
						$rawForm = \str_replace( 'apto-', '', $rawForm );
					}
					if ( \in_array( self::ENC_JSON, $formEnc ) ) {
						$rawForm = \json_decode( $rawForm, true );
					}
				}

				// the data is already available - this is the case with REST API calls
				if ( \is_array( $rawForm ) ) {
					$formParams = $rawForm;
				}
				else {
					\parse_str( (string)$rawForm, $formParams );
				}
			}
		}

		return \is_array( $formParams ) ? $formParams : [];
	}
}