<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Request;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\{
	PluginControllerAwareInterface,
	PluginControllerConsumer
};

/**
 * @property bool $is_plugin_admin
 */
class ThisRequest extends \FernleafSystems\Wordpress\Services\Request\ThisRequest implements PluginControllerAwareInterface {

	use PluginControllerConsumer;

	public function __get( string $key ) {
		$val = parent::__get( $key );
		switch ( $key ) {
			case 'is_plugin_admin':
				if ( $val === null ) {
					$val = $this->isAuthorisedPluginAdmin();
					if ( $val === true ) {
						$this->is_plugin_admin = true;
					}
				}
				break;
			default:
				break;
		}
		return $val;
	}

	public function isAuthorisedPluginAdmin() :bool {
		return $this->ctr()->opts_lookup->isAuthorisedPluginAdmin();
	}
}