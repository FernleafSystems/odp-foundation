<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Exceptions;

class PluginPageHandlerNotProvidedException extends \LogicException {

}