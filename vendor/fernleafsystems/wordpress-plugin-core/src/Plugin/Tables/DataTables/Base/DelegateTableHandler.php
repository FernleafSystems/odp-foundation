<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Tables\DataTables\Base;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase;
use FernleafSystems\Wordpress\Services\Services;

abstract class DelegateTableHandler extends PluginControllerConsumerBase {

	/**
	 * @var string
	 */
	protected $action;

	/**
	 * @var array
	 */
	protected $tableData;

	/**
	 * @var array
	 */
	protected $spec;

	/**
	 * @var ?TableRecordsLoader
	 */
	private $recordsLoader = null;

	public function __construct( array $spec ) {
		$this->spec = \array_merge( [
			'table_config'            => TableConfig::class,
			'data_loader'             => TableDataLoader::class,
			'records_loader'          => TableRecordsLoader::class,
			'data_columns_extractor'  => TableDataColumnsExtractor::class,
			'data_search_panes_build' => TableDataSearchPanesBuild::class,
		], $spec );
	}

	/**
	 * @throws \Exception
	 */
	public function run( string $action, array $tableData ) :array {
		$this->action = $action;
		$this->tableData = $tableData;
		return $this->action === 'retrieve_table_data' ? $this->retrieveTableData() : $this->performAction();
	}

	/**
	 * @throws \Exception
	 */
	abstract protected function performAction() :array;

	protected function retrieveTableData() :array {
		$builder = $this->getTableDataLoader();
		$builder->table_data = $this->tableData;
		return [
			'success'        => true,
			'datatable_data' => $builder->build(),
		];
	}

	public function getTablePermissions( ?\WP_User $forUser = null ) :array {
		$WPU = Services::WpUsers();
		if ( $forUser === null ) {
			$forUser = $WPU->getCurrentWpUser();
		}
		return [
			'read'  => !empty( $forUser ) && $WPU->isUserAdmin( $forUser ),
			'write' => !empty( $forUser ) && $WPU->isUserAdmin( $forUser ),
		];
	}

	/**
	 * @return TableConfig|mixed
	 */
	public function getTableConfig() {
		return ( new $this->spec[ 'table_config' ]() )->setHandler( $this );
	}

	/**
	 * @return TableDataLoader|mixed
	 */
	public function getTableDataLoader() {
		return ( new $this->spec[ 'data_loader' ]() )->setHandler( $this );
	}

	/**
	 * @return TableDataColumnsExtractor|mixed
	 */
	public function getTableDataColumnsExtractor() {
		return ( new $this->spec[ 'data_columns_extractor' ]() )->setHandler( $this );
	}

	/**
	 * @return TableDataSearchPanesBuild|mixed
	 */
	public function getTableDataSearchPanes() {
		return ( new $this->spec[ 'data_search_panes_build' ]() )->setHandler( $this );
	}

	/**
	 * @return TableRecordsLoader|mixed
	 */
	public function getTableRecordsLoader( bool $reload = false ) {
		return ( $this->recordsLoader === null || $reload ) ? $this->recordsLoader = ( new $this->spec[ 'records_loader' ]() )->setHandler( $this ) : $this->recordsLoader;
	}
}