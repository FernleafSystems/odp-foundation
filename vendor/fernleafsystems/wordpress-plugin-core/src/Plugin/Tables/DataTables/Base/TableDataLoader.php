<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Tables\DataTables\Base;

use FernleafSystems\Utilities\Data\Adapter\DynProperties;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Tables\DataTables\SearchPanes\BuildDataForDays;
use FernleafSystems\Wordpress\Services\Services;

/**
 * @property array $table_data
 */
class TableDataLoader extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	use DelegateTableHandlerConsumer;
	use DynProperties;

	protected function countTotalRecords() :int {
		return $this->canLoadTableData() ? $this->prepareDefaultRecordsLoader()->totalAll() : 0;
	}

	protected function countTotalRecordsFiltered() :int {
		return $this->canLoadTableData() ? $this->prepareDefaultRecordsLoader()->totalFromWheres() : 0;
	}

	protected function buildTableRowsFromRawRecords( array $records ) :array {
		$forDisplay = [];

		$config = $this->getHandler()->getTableConfig();
		$extractor = $this->getHandler()->getTableDataColumnsExtractor();
		foreach ( $records as $record ) {
			$recordColumn = [];
			foreach ( $config->getColumnsToDisplay() as $column ) {
				$recordColumn[ $column ] = $extractor->forCol( $column, $record );
			}
			$forDisplay[] = $recordColumn;
		}
		return $forDisplay;
	}

	public function build() :array {
		return [
			'data'            => $this->loadForRecords(),
			'recordsTotal'    => $this->countTotalRecords(),
			'recordsFiltered' => $this->countTotalRecordsFiltered(),
			'searchPanes'     => $this->getSearchPanesData(),
		];
	}

	protected function getSearchPanesData() :array {
		return $this->canLoadTableData() ? $this->getHandler()->getTableDataSearchPanes()->build() : [];
	}

	public function loadForRecords() :array {
		if ( empty( $this->table_data[ 'search' ][ 'value' ] ) ) {
			return $this->loadRecordsWithDirectQuery();
		}
		else {
			return $this->loadRecordsWithSearch();
		}
	}

	protected function loadRecordsWithDirectQuery() :array {
		return $this->buildTableRowsFromRawRecords(
			$this->getRecords(
				$this->buildWheres(),
				(int)$this->table_data[ 'start' ],
				(int)$this->table_data[ 'length' ]
			)
		);
	}

	protected function loadRecordsWithSearch() :array {
		$start = (int)$this->table_data[ 'start' ];
		$length = (int)$this->table_data[ 'length' ];
		$search = (string)$this->table_data[ 'search' ][ 'value' ] ?? '';
		$wheres = $this->buildWheres();

		$searchableColumns = \array_flip( $this->getSearchableColumns() );

		// We keep building logs and filtering by the search string until we have
		// enough records built to return in order to satisfy the start + length.
		$results = [];
		$page = 0;
		$pageLength = 100;
		do {
			$interimResults = $this->buildTableRowsFromRawRecords(
				$this->getRecords( $wheres, $page*$pageLength, $pageLength )
			);
			// no more table results to process, so go with what we have.
			if ( empty( $interimResults ) ) {
				break;
			}

			foreach ( $interimResults as $result ) {

				if ( empty( $search ) ) {
					$results[] = $result;
				}
				else {
					$searchable = \array_intersect_key( $result, $searchableColumns );
					foreach ( $searchable as $value ) {
						$value = wp_strip_all_tags( $value );
						if ( !\is_string( $search ) ) {
//							error_log( var_export( $search, true ) );
							continue;
						}
						if ( \stripos( $value, $search ) !== false ) {
							$results[] = $result;
							break;
						}
					}
				}
			}

			$page++;
		} while ( \count( $results ) < $start + $length );

		$results = \array_values( $results );
		if ( \count( $results ) < $start ) {
			$results = [];
		}
		else {
			$results = \array_splice( $results, $start, $length );
		}

		return \array_values( $results );
	}

	protected function buildWheres() :array {
		return [];
	}

	protected function getOrderBy() :string {
		$orderBy = '';
		if ( !empty( $this->table_data[ 'order' ] ) ) {
			$col = $this->table_data[ 'order' ][ 0 ][ 'column' ];
			$sortCol = $this->table_data[ 'columns' ][ $col ][ 'data' ];
			$orderBy = \is_array( $sortCol ) ? $sortCol[ 'sort' ] : $sortCol;
		}
		return $orderBy;
	}

	protected function getOrderDirection() :string {
		$dir = 'ASC';
		if ( !empty( $this->table_data[ 'order' ] ) ) {
			$dir = \strtoupper( $this->table_data[ 'order' ][ 0 ][ 'dir' ] );
			if ( !\in_array( $dir, [ 'ASC', 'DESC' ] ) ) {
				$dir = 'ASC';
			}
		}
		return $dir;
	}

	protected function getRecords( array $wheres = [], int $offset = 0, int $limit = 0 ) :array {
		$loader = $this->getHandler()->getTableRecordsLoader();
		$loader->offset = $offset;
		$loader->limit = $limit;
		$loader->wheres = $wheres;
		$loader->order_by = $this->getOrderBy();
		$loader->order_dir = $this->getOrderDirection();
		return $loader->retrieve();
	}

	protected function getSearchableColumns() :array {
		return [];
	}

	protected function buildSqlWhereForDaysSearch( array $selectedDays, string $tableAbbr, string $column = 'created_at' ) :string {
		$splitDates = \array_map(
			function ( $selectedDay ) use ( $tableAbbr, $column ) {
				if ( $selectedDay === BuildDataForDays::ZERO_DATE_FORMAT ) {
					return sprintf( "(`%s`.`%s`=0)", $tableAbbr, $column );
				}
				else {
					[ $year, $month, $day ] = \explode( '-', $selectedDay );
					$carbon = Services::Request()->carbon( true )->setDate( $year, $month, $day );
					return sprintf( "(`%s`.`%s`>%s AND `%s`.`%s`<%s)",
						$tableAbbr,
						$column,
						$carbon->startOfDay()->timestamp,
						$tableAbbr,
						$column,
						$carbon->endOfDay()->timestamp
					);
				}
			},
			$selectedDays
		);
		return sprintf( '(%s)', \implode( ' OR ', \array_filter( $splitDates ) ) );
	}

	/**
	 * @return TableRecordsLoader|mixed
	 */
	protected function prepareDefaultRecordsLoader() {
		$loader = $this->getHandler()->getTableRecordsLoader();
		$loader->limit = $this->table_data[ 'length' ] ?? 0;
		$loader->offset = $this->table_data[ 'start' ] ?? 0;
		$loader->wheres = $this->buildWheres();
		$loader->order_by = $this->getOrderBy();
		$loader->order_dir = $this->getOrderDirection();
		return $loader;
	}

	protected function canLoadTableData() :bool {
		return !empty( $this->getHandler()->getTablePermissions()[ 'read' ] );
	}
}