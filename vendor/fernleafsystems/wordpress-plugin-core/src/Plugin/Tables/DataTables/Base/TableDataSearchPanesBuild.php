<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Tables\DataTables\Base;

class TableDataSearchPanesBuild {

	use DelegateTableHandlerConsumer;

	public function build() :array {
		return [];
	}
}