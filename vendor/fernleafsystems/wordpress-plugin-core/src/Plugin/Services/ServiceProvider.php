<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumer;

class ServiceProvider extends \League\Container\ServiceProvider\AbstractServiceProvider {

	use PluginControllerConsumer;

	/**
	 * @var ServiceProviderDef
	 */
	private $def;

	public function __construct( ServiceProviderDef $def ) {
		$this->def = $def;
	}

	public function register() :void {
		$c = $this->getContainer();
		foreach ( $this->def->enum() as $id => $serviceClass ) {

			/** @var ServiceDefs\DefBase $serviceObj */
			$serviceObj = new $serviceClass();
			$serviceObj->setCon( $this->con() );

			$service = \array_merge( [
				'arg'       => [
					'alias' => $id,
					'type'  => 'resolvable',
					'value' => null,
				],
				'shared'    => true,
				'auto_exec' => false,
				'args'      => [],
				'tags'      => $serviceObj->tags(),
			], $serviceObj() );

			$ARG = $service[ 'arg' ];
			if ( ( $ARG[ 'type' ] ?? 'resolvable' ) === 'resolvable' ) {
				$value = $ARG[ 'value' ];
			}
			else {
				$value = new $ARG[ 'type' ]( $ARG[ 'value' ] );
			}

			$def = empty( $service[ 'shared' ] ) ? $c->add( $id, $value ) : $c->addShared( $id, $value );

			foreach ( $service[ 'args' ] as $arg ) {
				if ( \is_string( $arg ) && \class_exists( $arg ) && !$c->has( $arg ) ) {
					$c->add( $arg );
				}
				$def->addArgument( $arg );
			}

			foreach ( $service[ 'tags' ] as $tag ) {
				$def->addTag( \str_starts_with( $tag, 'tag.' ) ? $tag : 'tag.'.$tag );
			}
		}
	}

	public function provides( string $id ) :bool {
		return \in_array( $id, $this->def->ids() );
	}
}