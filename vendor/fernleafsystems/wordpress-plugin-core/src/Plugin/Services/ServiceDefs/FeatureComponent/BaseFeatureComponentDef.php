<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\FeatureComponent;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\{
	DefBase,
	Traits
};
use League\Container\Argument\Literal\ArrayArgument;

abstract class BaseFeatureComponentDef extends DefBase {

	use Traits\TraitFeatureComponent;

	public function __invoke() :array {
		return [
			'arg'  => [
				'value' => $this->getFeatureClass(),
			],
			'args' => [
				new ArrayArgument( $this->getFeatureConfig() ),
			],
		];
	}

	abstract protected function getFeatureClass() :string;

	protected function getFeatureConfig() :array {
		return [];
	}
}