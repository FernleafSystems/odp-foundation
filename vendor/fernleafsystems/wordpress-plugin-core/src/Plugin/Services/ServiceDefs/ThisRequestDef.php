<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs;

class ThisRequestDef extends DefBase {

	public const ID = 'this_req';

	public function __invoke() :array {
		return [
			'arg' => [
				'value' => \FernleafSystems\Wordpress\Plugin\Core\Plugin\Request\ThisRequest::class,
			],
		];
	}
}