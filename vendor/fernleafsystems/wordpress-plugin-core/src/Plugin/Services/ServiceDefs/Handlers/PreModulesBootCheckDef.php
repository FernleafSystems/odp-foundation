<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Handlers;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\{
	DefBase,
	Traits
};

class PreModulesBootCheckDef extends DefBase {

	use Traits\TraitHandler;

	public const ID = 'pre_modules_boot_check';

	public function __invoke() :array {
		return [
			'arg' => [
				'value' => \FernleafSystems\Wordpress\Plugin\Core\Plugin\Checks\PreModulesBootCheck::class,
			],
		];
	}
}