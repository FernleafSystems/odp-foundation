<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Handlers;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\{
	DefBase,
	Traits
};
use League\Container\Argument\Literal\ArrayArgument;

class RestConDef extends DefBase {

	use Traits\TraitHandler;

	public const ID = 'rest';

	public function __invoke() :array {
		return [
			'arg'  => [
				'value' => \FernleafSystems\Wordpress\Plugin\Core\Plugin\Rest\RestCon::class,
			],
			'args' => [
				new ArrayArgument( $this->enumCommands() ),
			],
		];
	}

	protected function enumCommands() :array {
		return $this->ctr()->get( 'cfg.enum_rest_routes' );
	}
}