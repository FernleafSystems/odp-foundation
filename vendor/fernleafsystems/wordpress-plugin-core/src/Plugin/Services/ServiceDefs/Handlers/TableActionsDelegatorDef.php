<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Handlers;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\{
	DefBase,
	Traits
};
use League\Container\Argument\Literal\ArrayArgument;

class TableActionsDelegatorDef extends DefBase {

	use Traits\TraitHandler;

	public const ID = 'table_delegate_handlers';

	public function __invoke() :array {
		return [
			'arg'  => [
				'value' => \FernleafSystems\Wordpress\Plugin\Core\Plugin\Tables\DataTables\TableActionsDelegator::class,
			],
			'args' => [
				new ArrayArgument( $this->enum() ),
			],
		];
	}

	protected function enum() :array {
		return $this->ctr()->get( 'cfg.enum_datatable_handlers' );
	}
}