<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Handlers;

use FernleafSystems\Wordpress\Plugin\Core\AptoWpPluginCoreRoot;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Assets\Paths;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\{
	DefBase,
	Traits
};
use League\Container\Argument\Literal\ArrayArgument;

class RenderHandlerDef extends DefBase {

	use Traits\TraitHandler;

	public const ID = 'render';

	public function __invoke() :array {
		return [
			'arg'  => [
				'value' => \FernleafSystems\Wordpress\Plugin\Core\Plugin\Render\RenderHandler::class,
			],
			'args' => [
				new ArrayArgument( \array_values( $this->getTemplateRoots() ) ),
			],
		];
	}

	/**
	 * Important: we can't use $this->ctr()->paths as it results in a fatal error with the Container
	 * unable to resolve the service. So we go direct to the container.
	 */
	protected function getTemplateRoots() :array {
		/** @var Paths $paths */
		$paths = $this->ctr()->get( 'handler.paths' );
		try {
			$pluginCore = trailingslashit( path_join(
				\dirname( ( new \ReflectionClass( AptoWpPluginCoreRoot::class ) )->getFileName(), 2 ),
				'templates'
			) );
		}
		catch ( \Exception $e ) {
			$pluginCore = null;
		}
		return \array_filter( [
			'this_plugin' => $paths->forTemplate(),
			'plugin_core' => $pluginCore,
		] );
	}
}