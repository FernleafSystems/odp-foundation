<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Handlers;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Config\Opts\{
	ChangedOptsTriggers,
	PreSetOptSanitizer,
	PreStore
};
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\{
	DefBase,
	Traits
};

class OptsHandlerDef extends DefBase {

	use Traits\TraitHandler;

	public const ID = 'opts';

	public function __invoke() :array {
		return [
			'arg'  => [
				'value' => \FernleafSystems\Wordpress\Plugin\Core\Plugin\Config\OptsHandler::class,
			],
			'args' => [
				PreStore::class,
				PreSetOptSanitizer::class,
				ChangedOptsTriggers::class,
			],
		];
	}
}