<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Handlers;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\{
	DefBase,
	Traits
};

class ModulesConfigsLoader extends DefBase {

	use Traits\TraitHandler;

	public const ID = 'modules_configs_loader';

	public function __invoke() :array {
		return [
			'arg' => [
				'value' => \FernleafSystems\Wordpress\Plugin\Core\Plugin\Config\Modules\LoadModuleConfigs::class,
			],
		];
	}
}