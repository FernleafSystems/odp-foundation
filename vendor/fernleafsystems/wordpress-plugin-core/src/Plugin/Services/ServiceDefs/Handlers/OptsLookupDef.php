<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Handlers;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\{
	DefBase,
	Traits
};

class OptsLookupDef extends DefBase {

	use Traits\TraitHandler;

	public const ID = 'opts_lookup';

	public function __invoke() :array {
		return [
			'arg' => [
				'value' => \FernleafSystems\Wordpress\Plugin\Core\Plugin\Config\OptsLookup::class,
			],
		];
	}
}