<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Handlers;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\{
	DefBase,
	Traits
};
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\{
	DynamicPageLoad,
	ModuleOptionsSave,
	PluginSuperSearch,
	Render,
};
use League\Container\Argument\Literal\ArrayArgument;

class AssetsCustomizerDef extends DefBase {

	use Traits\TraitHandler;

	public const ID = 'assets_customizer';

	public function __invoke() :array {
		return [
			'arg'  => [
				'value' => \FernleafSystems\Wordpress\Plugin\Core\Plugin\Assets\Customizer ::class,
			],
			'args' => [
				new ArrayArgument( $this->builders() ),
			],
		];
	}

	protected function builders() :array {
		return [
			'mod_options'  => [
				'key'     => 'mod_options',
				'handles' => [
					'main',
				],
				'data'    => function () {
					$actionsBuilder = $this->ctr()->actions->dataBuilder();
					return [
						'ajax' => [
							'form_save'        => $actionsBuilder->build( ModuleOptionsSave::class ),
							'render_offcanvas' => $actionsBuilder->buildAjaxRender( Render\Components\OffCanvas\ModConfig::class ),
						]
					];
				},
			],
			'navi'         => [
				'key'     => 'navi',
				'handles' => [
					'main',
				],
				'data'    => function () {
					return [
						'ajax' => [
							'dynamic_load' => $this->ctr()->actions->dataBuilder()->build( DynamicPageLoad::class ),
						],
					];
				},
			],
			'offcanvas'    => [
				'key'     => 'offcanvas',
				'handles' => [
					'main',
				],
				'data'    => function () {
					return [
						'ajax' => [
							'dynamic_load' => $this->ctr()->actions->dataBuilder()->buildAjaxRender(),
						],
					];
				},
			],
			'super_search' => [
				'key'     => 'super_search',
				'handles' => [
					'main',
				],
				'data'    => function () {
					$actionsBuilder = $this->ctr()->actions->dataBuilder();
					return [
						'ajax'    => [
							'render_search_results' => $actionsBuilder->buildAjaxRender( Render\SuperSearchResults::class ),
							'select_search'         => $actionsBuilder->build( PluginSuperSearch::class ),
						],
						'strings' => [
							'enter_at_least_3_chars' => __( 'Search using whole words of at least 3 characters...' ),
							'placeholder'            => sprintf( '%s (%s)',
								__( 'Search for anything', 'wp-simple-firewall' ),
								'e.g. '.\implode( ', ', [
									__( 'IPs', 'wp-simple-firewall' ),
									__( 'options', 'wp-simple-firewall' ),
									__( 'tools', 'wp-simple-firewall' ),
									__( 'help', 'wp-simple-firewall' ),
								] )
							),
						],
					];
				},
			],
		];
	}
}