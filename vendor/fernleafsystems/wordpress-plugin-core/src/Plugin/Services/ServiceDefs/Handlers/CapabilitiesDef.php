<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Handlers;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\{
	DefBase,
	Traits
};
use League\Container\Argument\Literal\ArrayArgument;

class CapabilitiesDef extends DefBase {

	use Traits\TraitHandler;

	public const ID = 'caps';

	public function __invoke() :array {
		return [
			'arg'  => [
				'value' => \FernleafSystems\Wordpress\Plugin\Core\Plugin\Capabilities\Capabilities::class,
			],
			'args' => [
				new ArrayArgument( $this->getPremiumOnlyCaps() ),
			],
		];
	}

	protected function getPremiumOnlyCaps() :array {
		return [];
	}
}