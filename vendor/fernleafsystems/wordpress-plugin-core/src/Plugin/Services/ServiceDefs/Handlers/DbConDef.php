<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Handlers;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\{
	DefBase,
	Traits
};
use League\Container\Argument\Literal\ArrayArgument;

class DbConDef extends DefBase {

	use Traits\TraitHandler;

	public const ID = 'db_con';

	public function __invoke() :array {
		return [
			'arg'  => [
				'value' => \FernleafSystems\Wordpress\Plugin\Core\Plugin\Database\DbCon::class,
			],
			'args' => [
				new ArrayArgument( $this->getDbMap() ),
			],
		];
	}

	protected function getDbMap() :array {
		return [];
	}
}