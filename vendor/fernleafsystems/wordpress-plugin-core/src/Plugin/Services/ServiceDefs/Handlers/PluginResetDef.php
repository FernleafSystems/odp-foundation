<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Handlers;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\{
	DefBase,
	Traits
};

class PluginResetDef extends DefBase {

	use Traits\TraitHandler;

	public const ID = 'plugin_reset';

	public function __invoke() :array {
		return [
			'arg' => [
				'value' => \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\Plugin\PluginReset::class,
			],
		];
	}
}