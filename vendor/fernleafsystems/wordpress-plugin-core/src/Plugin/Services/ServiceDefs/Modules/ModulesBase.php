<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Modules;

abstract class ModulesBase extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\DefBase {

	public static function ID() :string {
		return 'handler.modules.'.parent::ID();
	}
}