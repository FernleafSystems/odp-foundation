<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Modules;

class BuildOptionsForFormDisplayDef extends ModulesBase {

	public const ID = 'build_options_for_form_display';

	public function __invoke() :array {
		return [
			'arg' => [
				'value' => \FernleafSystems\Wordpress\Plugin\Core\Plugin\Config\Modules\Ops\BuildOptionsForFormDisplay::class,
			],
		];
	}
}