<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Modules;

class ModuleBuildDefsDef extends ModulesBase {

	public const ID = 'build_defs';

	public function __invoke() :array {
		return [
			'arg'  => [
				'value' => \FernleafSystems\Wordpress\Plugin\Core\Plugin\Config\Modules\ModulesBuildDefs::class,
			],
		];
	}
}