<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Traits;

trait TraitHandler {

	public static function ID() :string {
		return 'handler.'.parent::ID();
	}
}