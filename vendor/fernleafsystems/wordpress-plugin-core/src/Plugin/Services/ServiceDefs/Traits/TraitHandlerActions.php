<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Traits;

trait TraitHandlerActions {

	public static function ID() :string {
		return 'handler.actions.'.parent::ID();
	}
}