<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Traits;

trait TraitFeatureComponent {

	public static function ID() :string {
		return 'handler.feature_component.'.parent::ID();
	}
}