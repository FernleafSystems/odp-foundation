<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Cfg;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\Plugin\PluginPrefix;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\{
	DefBase,
	Traits
};
use League\Container\Argument\Literal;

class CfgPluginActionNameDef extends DefBase {

	use Traits\TraitCfg;

	public const ID = 'plugin_action_name';

	public function __invoke() :array {
		/** @var PluginPrefix $pfx */
		$pfx = $this->ctr()->get( 'handler.plugin_prefix' );
		return [
			'arg' => [
				'type'  => Literal\StringArgument::class,
				'value' => $pfx->_(),
			],
		];
	}
}