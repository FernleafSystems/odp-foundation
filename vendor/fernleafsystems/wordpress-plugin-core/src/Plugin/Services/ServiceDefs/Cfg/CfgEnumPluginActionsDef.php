<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Cfg;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\{
	DefBase,
	Traits
};
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\{
	AjaxRender,
	AptoTableAction,
	DynamicLoad,
	DynamicPageLoad,
	FullPageDisplay,
	ModuleOptionsSave,
	Render
};
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Render\{
	Components,
	PluginAdminPages,
};
use League\Container\Argument\Literal;

class CfgEnumPluginActionsDef extends DefBase {

	use Traits\TraitCfg;

	public const ID = 'enum_plugin_actions';

	public function __invoke() :array {
		return [
			'arg' => [
				'type'  => Literal\ArrayArgument::class,
				'value' => $this->enum(),
			],
		];
	}

	protected function enum() :array {
		return [
			AjaxRender::class,
			DynamicPageLoad::class,
			ModuleOptionsSave::class,
			AptoTableAction::class,

			Components\AdminNotice::class,
			Components\OffCanvas\ModConfig::class,
			Components\Options\OptionsForm::class,
			Components\WaitSpinner::class,

			DynamicLoad\Config::class,

			Render::class,
			Render\PageAdminPlugin::class,
			Render\Components\OffCanvas\OffCanvasContainer::class,

			PluginAdminPages\PageDynamicLoad::class,
			PluginAdminPages\PagePluginAdminRestricted::class,

			FullPageDisplay\FullPageDisplayDynamic::class,
			FullPageDisplay\FullPageDisplayNonTerminating::class,
			FullPageDisplay\FullPageDisplayStatic::class,
		];
	}
}