<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Cfg;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Modules\Plugin\ModCon;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\{
	DefBase,
	Traits
};
use League\Container\Argument\Literal;

class CfgEnumModulesDef extends DefBase {

	use Traits\TraitCfg;

	public const ID = 'enum_modules';

	public function __invoke() :array {
		return [
			'arg' => [
				'type'  => Literal\ArrayArgument::class,
				'value' => $this->enumModules(),
			],
		];
	}

	protected function enumModules() :array {
		return [
			'plugin' => ModCon::class,
		];
	}
}