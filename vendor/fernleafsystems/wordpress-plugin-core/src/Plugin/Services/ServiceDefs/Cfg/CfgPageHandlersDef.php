<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Cfg;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Render\PluginAdminPages\PageConfig;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\Plugin\PluginPages;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\{
	DefBase,
	Traits
};
use League\Container\Argument\Literal;

class CfgPageHandlersDef extends DefBase {

	use Traits\TraitCfg;

	public const ID = 'page_handlers';

	public function __invoke() :array {
		return [
			'arg' => [
				'type'  => Literal\ArrayArgument::class,
				'value' => $this->enum(),
			],
		];
	}

	protected function enum() :array {
		return [
			PluginPages::NAV_OPTIONS_CONFIG => [
				'default' => PageConfig::class,
			],
		];
	}
}