<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Cfg;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\{
	DefBase,
	Traits
};
use FernleafSystems\Wordpress\Plugin\Core\Plugin\WpCli\Cmds\{
	ConfigOptGet,
	ConfigOptSet,
	ConfigOptsList
};
use League\Container\Argument\Literal;

class CfgEnumWpCliCommandsDef extends DefBase {

	use Traits\TraitCfg;

	public const ID = 'enum_wpcli_commands';

	public function __invoke() :array {
		return [
			'arg' => [
				'type'  => Literal\ArrayArgument::class,
				'value' => $this->enum(),
			],
		];
	}

	protected function enum() :array {
		return [
			ConfigOptGet::class,
			ConfigOptSet::class,
			ConfigOptsList::class,
		];
	}
}