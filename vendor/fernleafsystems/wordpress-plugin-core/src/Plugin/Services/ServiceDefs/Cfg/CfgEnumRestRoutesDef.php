<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Cfg;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\{
	DefBase,
	Traits
};
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Rest\Route\{
	AptoPluginAction
};
use League\Container\Argument\Literal;

class CfgEnumRestRoutesDef extends DefBase {

	use Traits\TraitCfg;

	public const ID = 'enum_rest_routes';

	public function __invoke() :array {
		return [
			'arg' => [
				'type'  => Literal\ArrayArgument::class,
				'value' => $this->enum(),
			],
		];
	}

	protected function enum() :array {
		return [
			AptoPluginAction::class,
		];
	}
}