<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Actions;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\Plugin\PluginPages;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\{
	DefBase,
	Traits
};
use FernleafSystems\Wordpress\Services\Services;

class ActionsCaptureRedirectsDef extends DefBase {

	use Traits\TraitHandlerActions;

	public const ID = 'capture_redirects';

	public function __invoke() :array {
		return [
			'arg'  => [
				'value' => \FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Capture\CaptureRedirects::class,
			],
			'args' => [
				$this->getRedirectRules(),
			]
		];
	}

	private function getRedirectRules() :array {
		return [
			'invalid_plugin_pages' => function () {
				$con = $this->con();
				$cntnr = $con->cntnr;
				$pages = $cntnr->plugin_pages;
				$URLs = $cntnr->plugin_urls;
				$req = Services::Request();

				$page = (string)$req->query( 'page' );
				$navID = (string)$req->query( PluginPages::FIELD_NAV );
				$subNav = (string)$req->query( PluginPages::FIELD_SUBNAV );

				$redirectTo = null;
				if ( $page === $URLs->rootAdminPageSlug() ) {
					if ( !$pages->exists( $navID ) ) {
						$redirectTo = $URLs->adminHome();
					}
					elseif ( empty( $subNav ) || $subNav === PluginPages::SUBNAV_INDEX || !$pages->exists( $navID, $subNav ) ) {
						$redirectTo = $URLs->adminTopNav( $navID );
					}
				}
				elseif ( \preg_match( sprintf( '#^%s-([a-z_]+)$#', \preg_quote( $this->ctr()->prefix->pfx(), '#' ) ), $page, $matches ) ) {
					[ $navID, $subNav ] = \explode( '_', $matches[ 1 ] );
					$redirectTo = $pages->exists( $navID, $subNav ) ? $URLs->adminTopNav( $navID, $subNav ) : $URLs->adminHome();
				}

				return empty( $redirectTo ) ? '' : $redirectTo;
			},
		];
	}
}