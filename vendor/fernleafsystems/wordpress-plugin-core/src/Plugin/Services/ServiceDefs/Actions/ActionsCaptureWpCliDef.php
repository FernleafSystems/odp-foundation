<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Actions;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\{
	DefBase,
	Traits
};

class ActionsCaptureWpCliDef extends DefBase {

	use Traits\TraitHandlerActions;

	public const ID = 'capture_wpcli';

	public function __invoke() :array {
		return [
			'arg' => [
				'value' => \FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Capture\CaptureWpCliAction::class,
			],
		];
	}
}