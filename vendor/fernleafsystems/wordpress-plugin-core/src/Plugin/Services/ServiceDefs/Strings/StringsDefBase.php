<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Strings;

abstract class StringsDefBase extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\DefBase {

	public static function ID() :string {
		return 'handler.strings.'.parent::ID();
	}
}