<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Strings;

class StringsModulesDef extends StringsDefBase {

	public const ID = 'modules';

	public function __invoke() :array {
		return [
			'arg' => [
				'value' => \FernleafSystems\Wordpress\Plugin\Core\Plugin\Config\Modules\Ops\StringsModules::class,
			],
		];
	}
}