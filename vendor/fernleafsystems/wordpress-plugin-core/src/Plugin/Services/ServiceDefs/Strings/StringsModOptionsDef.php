<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\ServiceDefs\Strings;

class StringsModOptionsDef extends StringsDefBase {

	public const ID = 'mod_options';

	public function __invoke() :array {
		return [
			'arg' => [
				'value' => \FernleafSystems\Wordpress\Plugin\Core\Plugin\Config\Modules\Ops\StringsOptions::class,
			],
		];
	}
}