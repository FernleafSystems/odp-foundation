<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Navigation;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\Plugin\PluginPages;
use FernleafSystems\Wordpress\Services\Services;

class NavMenuBuilder extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	public function build() :array {
		$menu = [];

		try {
			$pages = $this->ctr()->plugin_pages->getPages();
		}
		catch ( \Exception $e ) {
			$pages = [];
		}

		$isPluginAdmin = $this->con()->isPluginAdmin();
		foreach ( $pages as $pageID => $page ) {

			$menuItem = $this->buildForTopLevelPage( $page );
			if ( empty( $menuItem ) ) {
				continue;
			}

			$item = Services::DataManipulation()->mergeArraysRecursive( [
				'slug'      => 'no-slug',
				'title'     => __( 'NO TITLE', 'wp-simple-firewall' ),
				'href'      => 'javascript:{}',
				'classes'   => [],
				'id'        => '',
				'active'    => PluginPages::GetNav() === $pageID,
				'sub_items' => [],
				'target'    => '',
				'data'      => [],
				'badge'     => [],
				'introjs'   => [],
			], $menuItem );

			if ( !empty( $item[ 'introjs' ] ) ) {
				$item[ 'classes' ][] = 'tour-'.$this->getIntroJsTourID();
				if ( empty( $item[ 'introjs' ][ 'title' ] ) ) {
					$item[ 'introjs' ][ 'title' ] = $item[ 'title' ];
				}
			}

			if ( empty( $item[ 'sub_items' ] ) ) {
				$item[ 'classes' ][] = 'body_content_link';
			}
			else {
				$item[ 'sub_items' ] = \array_map( function ( $sub ) use ( $isPluginAdmin ) {
					if ( empty( $sub[ 'classes' ] ) ) {
						$sub[ 'classes' ] = [];
					}
					if ( $sub[ 'active' ] ?? false ) {
						$sub[ 'classes' ][] = 'active';
					}
					if ( !$isPluginAdmin ) {
						$sub[ 'classes' ][] = 'disabled';
					}
					return $sub;
				}, $item[ 'sub_items' ] );

				// Set parent active if any sub-items are active
				if ( !$item[ 'active' ] ) {
					$item[ 'active' ] = \count( \array_filter( $item[ 'sub_items' ], function ( $sub ) {
						return $sub[ 'active' ] ?? false;
					} ) );
				}
			}

			if ( $item[ 'active' ] ) {
				$item[ 'classes' ][] = 'active';
			}

			if ( !$isPluginAdmin ) {
				$item[ 'classes' ][] = 'disabled';
			}

			$menu[ $pageID ] = $item;
		}

		return $menu;
	}

	protected function buildForTopLevelPage( array $page ) :array {
		$menu = [];
		if ( $page[ 'plugin_menu' ][ 'show' ] ) {
			$ctr = $this->ctr();
			$menu = [
				'slug'      => $page[ 'id' ],
				'title'     => $page[ 'name' ],
				'subtitle'  => $page[ 'subtitle' ],
				'img'       => $ctr->svgs->raw( $page[ 'plugin_menu' ][ 'icon' ] ?? '' ),
				'img_hover' => $ctr->svgs->raw( $page[ 'plugin_menu' ][ 'icon_hover' ] ?? ( $page[ 'plugin_menu' ][ 'icon' ] ?? '' ) ),
				'href'      => $ctr->plugin_urls->adminTopNav( $page[ 'id' ] ),
				'active'    => PluginPages::GetNav() === $page[ 'id' ],
				'sub_items' => [],
			];

			foreach ( $page[ 'sub_navs' ] as $subNav ) {
				if ( $subNav[ 'plugin_menu' ][ 'show' ] ) {
					$pageSlug = $page[ 'id' ].'_'.$subNav[ 'id' ];
					$subNavItem = [
						'slug'    => $pageSlug,
						'title'   => $subNav[ 'plugin_menu' ][ 'name' ],
						'href'    => $ctr->plugin_urls->adminTopNav( $page[ 'id' ], $subNav[ 'id' ] ),
						'active'  => PluginPages::IsNavs( $page[ 'id' ], $subNav[ 'id' ] ),
						'classes' => $subNav[ 'plugin_menu' ][ 'classes' ] ?? [],
						'data'    => $subNav[ 'plugin_menu' ][ 'data' ] ?? [],
					];

					if ( $subNav[ 'plugin_menu' ][ 'dynamic_load' ] ) {
						$subNavItem[ 'classes' ] = \array_merge( $subNavItem[ 'classes' ], [
							'dynamic_body_load',
							'body_content_link'
						] );

						$subNavItem[ 'data' ][ 'dynamic_load_data' ] = \json_encode( [
							'nav'      => $page[ 'id' ],
							'subnav'   => $subNav[ 'id' ],
							// TODO: this is a hack. We need to use plugin.json to build data[] for dynamic load
							'mod_slug' => $subNav[ 'id' ],
						] );
					}

					$menu[ 'sub_items' ][ $pageSlug ] = $subNavItem;
				}
			}
		}
		return $menu;
	}

	private function getIntroJsTourID() :string {
		return 'navigation_v1';
	}
}