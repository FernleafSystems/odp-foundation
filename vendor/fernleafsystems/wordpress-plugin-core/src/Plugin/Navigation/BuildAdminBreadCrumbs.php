<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Navigation;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\Plugin\PluginPages;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase;

class BuildAdminBreadCrumbs extends PluginControllerConsumerBase {

	public function current() :array {
		return $this->for( PluginPages::GetNav(), PluginPages::GetSubNav() );
	}

	public function for( string $nav, string $subnav ) :array {
		try {
			$crumbs = $this->parse( $nav, $subnav );
		}
		catch ( \Exception $e ) {
			$crumbs = [];
		}
		return $crumbs;
	}

	/**
	 * @throws \Exception
	 */
	public function parse( string $nav, string $subNav ) :array {
		$pluginNav = $this->ctr()->plugin_pages;
		$urls = $this->ctr()->plugin_urls;

		$crumbs = [];
		if ( empty( $nav ) ) {
			throw new \Exception( 'No nav provided.' );
		}
		if ( empty( $subNav ) ) {
			$subNav = PluginPages::SUBNAV_INDEX;
		}

		$hierarchy = $pluginNav->getNavHierarchy();
		$navStruct = $hierarchy[ $nav ] ?? null;
		if ( empty( $navStruct ) ) {
			throw new \Exception( 'Not a valid nav: '.$nav );
		}
		if ( empty( $navStruct[ 'sub_navs' ][ $subNav ] ) ) {
			throw new \Exception( 'Not a valid sub_nav: '.$subNav );
		}

		$crumbs[] = [
			'text'  => $navStruct[ 'name' ],
			'title' => sprintf( '%s: %s', __( 'Navigation' ), sprintf( __( '%s Home', 'wp-simple-firewall' ), $navStruct[ 'name' ] ) ),
			'href'  => $urls->adminTopNav( $nav, \key( $hierarchy[ $nav ][ 'sub_navs' ] ) ),
		];

		foreach ( $navStruct[ 'parents' ] as $parentNav ) {
			if ( $parentNav !== $nav ) {
				$name = $hierarchy[ $parentNav ][ 'name' ];
				\array_unshift( $crumbs, [
					'text'  => $name,
					'title' => sprintf( '%s: %s', __( 'Navigation' ), sprintf( __( '%s Home', 'wp-simple-firewall' ), $name ) ),
					'href'  => $urls->adminTopNav( $parentNav, \key( $hierarchy[ $parentNav ][ 'sub_navs' ] ) ),
				] );
			}
		}

		return $crumbs;
	}
}