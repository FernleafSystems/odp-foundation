<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Rest\Route;

class AptoPluginAction extends RouteBase {

	protected function getRouteArgsDefaults() :array {
		return [
			'ex'      => [
				'description' => 'Action To Execute',
				'type'        => 'string',
				'required'    => true,
				'readonly'    => true,
			],
			'exnonce' => [
				'description' => 'Action Nonce',
				'type'        => 'string',
				'required'    => false,
				'readonly'    => true,
			],
			'payload' => [
				'description' => 'Action Payload',
				'type'        => 'object',
				'default'     => [],
				'required'    => true,
				'readonly'    => true,
			],
		];
	}

	/**
	 * @inheritDoc
	 */
	protected function verifyPermission( \WP_REST_Request $req ) {
		return true;
	}

	public function getRouteMethods() :array {
		return [ 'GET', 'POST' ];
	}

	public function getRoutePath() :string {
		return '/action/(?P<ex>[a-z_-]{3,})';
	}

	protected function getRequestProcessorClass() :string {
		return \FernleafSystems\Wordpress\Plugin\Core\Plugin\Rest\RequestProcess\AptoPluginActionProcess::class;
	}
}