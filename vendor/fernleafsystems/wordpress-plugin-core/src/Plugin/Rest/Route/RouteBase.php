<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Rest\Route;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Rest\RequestProcess\ProcessBase;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\{
	PluginControllerAwareInterface,
	PluginControllerConsumer
};

abstract class RouteBase extends \FernleafSystems\Wordpress\Plugin\Core\Rest\Route\RouteBase implements PluginControllerAwareInterface {

	use PluginControllerConsumer;

	protected function getNamespace() :string {
		return $this->ctr()->prefix->pfx();
	}

	protected function getConfigDefaults() :array {
		$cfg = parent::getConfigDefaults();
		$cfg[ 'authorization' ][ 'user_cap' ] = 'manage_options';
		return $cfg;
	}

	/**
	 * @return ProcessBase|mixed
	 */
	protected function getRequestProcessor() {
		/** @var ProcessBase $processorClass */
		$processorClass = $this->getRequestProcessorClass();
		return ( new $processorClass( $this ) )->setCon( $this->con() );
	}

	public function isRouteAvailable() :bool {
		return true;
	}
}