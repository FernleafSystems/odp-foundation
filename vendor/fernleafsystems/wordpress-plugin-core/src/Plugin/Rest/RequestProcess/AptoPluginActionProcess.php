<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Rest\RequestProcess;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\ActionsController;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Exceptions\ActionException;

class AptoPluginActionProcess extends ProcessBase {

	protected function process() :array {
		$req = $this->getWpRestRequest();
		$params = $req->get_params();

		try {
			$response = $this->ctr()->actions->action( $params[ 'ex' ], $params[ 'payload' ], ActionsController::ACTION_REST );
			$data = $response->action_response_data;
			if ( !isset( $data[ 'success' ] ) ) {
				$data[ 'success' ] = $response->success;
			}
		}
//		catch ( ActionDoesNotExistException $e ) {
//		}
//		catch ( ActionTypeDoesNotExistException $e ) {
//		}
//		catch ( InvalidActionNonceException $e ) {
//		}
//		catch ( SecurityAdminRequiredException $e ) {
//		}
		catch ( ActionException $e ) {
//			error_log( $e->getMessage() );
			$data = [
				'error'   => $e->getMessage(),
				'message' => $e->getMessage(),
				'success' => false,
			];
		}

		/** See AJAX normalised data */
		return [
			'success' => $data[ 'success' ],
			'data'    => \array_merge( [
				'page_reload' => false,
				'message'     => '',
				'html'        => '',
			], $data )
		];
	}
}