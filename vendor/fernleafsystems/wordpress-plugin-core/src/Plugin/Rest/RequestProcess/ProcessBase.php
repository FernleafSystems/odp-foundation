<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Rest\RequestProcess;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\{
	PluginControllerAwareInterface,
	PluginControllerConsumer
};

abstract class ProcessBase extends \FernleafSystems\Wordpress\Plugin\Core\Rest\Request\Process implements PluginControllerAwareInterface {

	use PluginControllerConsumer;
}