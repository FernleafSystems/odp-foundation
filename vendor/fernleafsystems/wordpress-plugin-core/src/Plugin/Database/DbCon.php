<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Database;

use FernleafSystems\Utilities\Data\Adapter\DynPropertiesClass;
use FernleafSystems\Utilities\Logic\ExecOnce;
use FernleafSystems\Wordpress\Plugin\Core\Databases\{
	Base\Handler,
	Common
};
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\{
	PluginControllerAwareInterface,
	PluginControllerConsumer
};
use FernleafSystems\Wordpress\Services\Services;

class DbCon extends DynPropertiesClass implements PluginControllerAwareInterface {

	use ExecOnce;
	use PluginControllerConsumer;

	protected $map;

	public function __construct( array $dbMap = [] ) {
		$this->map = $dbMap;
	}

	private $dbHandlers = null;

	/**
	 * @return array[]
	 */
	public function getHandlers() :array {
		if ( $this->dbHandlers === null ) {
			$this->dbHandlers = [];
			$dbSpecs = $this->con()->cfg->configuration->databases;
			foreach ( $this->map as $dbKey => $dbDef ) {
				$dbDef[ 'def' ] = $dbSpecs[ 'tables' ][ $dbKey ];
				$dbDef[ 'handler' ] = null;
				$this->dbHandlers[ $dbKey ] = $dbDef;
			}
		}
		return $this->dbHandlers;
	}

	/**
	 * @return array[]
	 */
	public function loadAll() :array {
		foreach ( \array_keys( $this->getHandlers() ) as $dbhKey ) {
			try {
				$this->load( $dbhKey );
			}
			catch ( \Exception $e ) {
			}
		}
		return $this->getHandlers();
	}

	/**
	 * @return Handler|mixed|null
	 * @throws \Exception
	 */
	public function load( string $dbKey ) {
		return $this->loadDbH( $this->getHandlers()[ $dbKey ][ 'slug' ] );
	}

	/**
	 * @return Handler|mixed|null
	 * @throws \Exception
	 */
	public function loadDbH( string $dbSlug, bool $reload = false ) {
		$con = $this->con();

		$dbKey = null;
		foreach ( $this->getHandlers() as $key => $handlerSpec ) {
			if ( $handlerSpec[ 'slug' ] === $dbSlug ) {
				$dbKey = $key;
				$dbh = $handlerSpec;
				break;
			}
		}

		if ( empty( $dbh ) ) {
			throw new \Exception( '' );
		}

		if ( $reload || empty( $dbh[ 'handler' ] ) ) {
			/**
			 * We need to ensure that any dependent (foreign key references) tables are initiated before
			 * attempting to initiate ourselves.
			 */
			$dbDef = $dbh[ 'def' ];
			foreach ( $dbDef[ 'cols_custom' ] as $colDef ) {
				if ( ( $colDef[ 'macro_type' ] ?? '' ) === Common\Types::MACROTYPE_FOREIGN_KEY_ID ) {
					$table = $colDef[ 'foreign_key' ][ 'ref_table' ];
					if ( \str_starts_with( $table, $con->getPluginPrefix( '_' ) ) ) {
						$this->loadDbH( \str_replace( $con->getPluginPrefix( '_' ).'_', '', $table ) );
					}
				}
			}

			$dbDef[ 'table_prefix' ] = $con->getPluginPrefix( '_' );

			$modPlug = $con->getModule_Plugin();
			/** @var Handler|mixed $dbh */
			$dbh = new $dbh[ 'handler_class' ]( $dbDef );
			$dbh->use_table_ready_cache = !$con->plugin_reset
										  && $con->comps->opts_lookup->getActivatedPeriod() > Common\TableReadyCache::READY_LIFETIME
										  && ( Services::Request()
													   ->ts() - $modPlug->getTracking()->last_upgrade_at > 10 );
			$dbh->execute();

			$this->dbHandlers[ $dbKey ][ 'handler' ] = $dbh;
		}

		return $this->dbHandlers[ $dbKey ][ 'handler' ];
	}

	public function reset() :void {
		$this->dbHandlers = null;
	}

	/**
	 * @throws \Exception
	 */
	public function __get( string $key ) {
		$value = parent::__get( $key );

		if ( isset( $this->map[ $key ] ) ) {
			$value = $this->load( $key );
		}

		return $value;
	}
}