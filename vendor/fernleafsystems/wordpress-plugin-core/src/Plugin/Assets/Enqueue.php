<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Assets;

use FernleafSystems\Utilities\Logic\ExecOnce;
use FernleafSystems\Wordpress\Services\Services;
use FernleafSystems\Wordpress\Services\Utilities\File\Paths;

/**
 * Process to enqueue custom assets.
 * 1) plugin.json should have a "dist" key with an array of assets to enqueue.
 * 2) Each asset should have a "handle" key, a "zones" key (array of zones to enqueue in), a "types" key (array of types to enqueue as), and a "deps" key (array of dependencies).
 * 3) Each asset can have a "flags" key with an "admin_only" key (bool) to only enqueue in admin.
 * 4) Each asset can have a "zones" key with an array of zones to enqueue in, such as plugin_admin, wp_admin, page_login.
 * 5) Each asset can have a "deps" key with an array of dependencies
 * 6) Each asset can have a "types" key with an array of types to enqueue as (css, js)
 * 7) To mark an asset as needing to be enqueued, use the filter "custom_enqueue_assets" and return an array of handles.
 * 8) To localise an asset, use the filter "custom_localisations" and return an array of localisations. It's better
 *    to use the filter "custom_localisations/components" within the Customizer class to provide the localisations
 * 	  directly, as this uses the "custom_locations" filter already.
 * 9) To dequeue an asset, use the filter "custom_dequeues" and return an array of handles.
 * 10) To remove conflicting admin assets, use the filter "conflict_assets_to_dequeue" and return an array of handles.
 */
class Enqueue extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	use ExecOnce;

	public const CSS = 'css';
	public const JS = 'js';

	private $adminHookSuffix = '';

	private $enqueuedHandles = [];

	protected function canRun() :bool {
		$WP = Services::WpGeneral();
		return !$WP->isAjax() && !$WP->isCron() && !empty( $this->con()->cfg->includes[ 'dist' ] );
	}

	protected function run() {

		add_action( 'login_enqueue_scripts', function () {
			$this->enqueue();
			add_action( 'login_footer', function () {
				$this->dequeue();
			}, -1000 );
		}, 1000 );

		add_action( 'wp_enqueue_scripts', function () {
			$this->enqueue();
			add_action( 'wp_footer', function () {
				$this->dequeue();
			}, -1000 );
		}, 1000 );

		add_action( 'admin_enqueue_scripts', function ( $hook_suffix ) {
			$this->adminHookSuffix = (string)$hook_suffix;
			/** ALWAYS cast to string when setting this property */
			$this->enqueue();
			add_action( 'admin_footer', function () {
				$this->dequeue();
			}, -1000 );
		}, 1000 );

		add_action( 'admin_enqueue_scripts', function () {
			global $wp_scripts;
			global $wp_styles;
			$this->removeConflictingAdminAssets( $wp_scripts );
			$this->removeConflictingAdminAssets( $wp_styles );
		}, \PHP_INT_MAX );

		$this->compatibility();
	}

	private function compatibility() {
		/** https://kb.mailpoet.com/article/365-how-to-solve-3rd-party-css-menu-conflict */
		add_filter( 'mailpoet_conflict_resolver_whitelist_script', function ( $scripts ) {
			$scripts[] = \dirname( $this->con()->base_file );
			return $scripts;
		} );
		add_filter( 'mailpoet_conflict_resolver_whitelist_style', function ( $scripts ) {
			$scripts[] = \dirname( $this->con()->base_file );
			return $scripts;
		} );
	}

	protected function getPluginAdminHookSuffix() :string {
		return 'toplevel_page_'.$this->ctr()->plugin_urls->rootAdminPageSlug();
	}

	/**
	 * @param \WP_Styles|\WP_Scripts $depContainer
	 */
	private function removeConflictingAdminAssets( $depContainer ) {
		$toDequeue = [];

		if ( $this->con()->getIsPage_PluginAdmin() ) {
			$default = [
				'cerber_css',
				'bootstrap',
				'convesio-caching-custom-styles', // fixed-admin-menu
				'custom-admin-style', // fixed-admin-menu
				'wp-notes',
				'wpforo',
				'fs_common',
				'this-day-in-history',
				'mo_oauth_admin_settings_style',
				'mo_oauth_admin_settings_phone_style',
				'mo_oauth_admin_settings_datatable',
				'workreap',
				'core_functions', //workreap
				'wc_connect_banner',
				'wc-stripe-blocks-checkout-style',
				'wcpay-admin-css',
				'ce4wp',
				'mailjet',
				'monsterinsights',
				'udb-admin',
			];
		}
		else {
			$default = [];
		}

		$filtered = apply_filters( $this->ctr()->prefix->hook( 'conflict_assets_to_dequeue' ), $default, $depContainer );
		$conflictHandlesRegEx = \implode( '|', \array_map( 'preg_quote', \is_array( $filtered ) ? $filtered : $default ) );

		if ( !empty( $conflictHandlesRegEx ) ) {
			foreach ( $depContainer->queue as $script ) {
				$handle = (string)$depContainer->registered[ $script ]->handle;
				if ( \strpos( $handle, $this->ctr()->prefix->pfx() ) === false
					 && \preg_match( sprintf( '/(%s)/i', $conflictHandlesRegEx ), $handle ) ) {
					$toDequeue[] = $handle;
				}
			}
		}
		$depContainer->dequeue( $toDequeue );
	}

	protected function dequeue() {
		foreach ( apply_filters( $this->ctr()->prefix->hook( 'custom_dequeues' ), [] ) as $handle ) {
			$handle = $this->normaliseHandle( $handle );
			wp_dequeue_style( $handle );
			wp_dequeue_script( $handle );
		}
	}

	protected function enqueue() {

		// Register all plugin assets
		$this->registerAssets();
		$assets = $this->buildAssetsToEnqueue();

		// Combine enqueues and enqueue assets
		foreach ( [ self::CSS, self::JS ] as $type ) {
			foreach ( $assets[ $type ] as $asset ) {
				$asset = $this->normaliseHandle( $asset );
				$type == self::CSS ? wp_enqueue_style( $asset ) : wp_enqueue_script( $asset );
			}
		}

		// Get module localisations
		$this->localise();
	}

	/**
	 * Registers all assets in the plugin with the global population of assets (if not already included)
	 * This allows us to easily share assets amongst plugins and especially to use the Foundation Classes
	 * plugin to cater for most shared assets.
	 */
	private function registerAssets() {
		$ctr = $this->ctr();
		foreach ( [ self::CSS, self::JS ] as $type ) {

			foreach ( $this->con()->cfg->includes[ 'tp' ] as $tpKey => $includes ) {
				$url = $includes[ $type ] ?? null;
				if ( !empty( $url ) ) {
					$this->register(
						$ctr->prefix->slash( 'tp/'.$tpKey ),
						$url,
						$this->normaliseHandles( $dist[ 'deps' ] ?? [] )
					);
				}
			}

			foreach ( $this->con()->cfg->includes[ 'dist' ] as $dist ) {
				$adminOnly = !empty( $dist[ 'flags' ][ 'admin_only' ] );
				if ( !$adminOnly || is_admin() || is_network_admin() ) {
					$this->register(
						$this->normaliseHandle( $dist[ 'handle' ] ),
						$this->ctr()->urls->forAsset( sprintf( 'dist/%s.bundle.%s', $ctr->prefix->pfx( $dist[ 'handle' ] ), $type ) ),
						$this->normaliseHandles( $dist[ 'deps' ] ?? [] )
					);
				}
			}
		}
	}

	private function register( string $handle, string $url, array $deps = [] ) :void {
		$path = \parse_url( $url, \PHP_URL_PATH );
		if ( !empty( $path ) ) {
			Paths::Ext( $path ) === 'js' ?
				wp_register_script( $handle, $url, $deps, $this->con()->cfg->version(), true )
				: wp_register_style( $handle, $url, [], $this->con()->cfg->version() );
		}
	}

	/**
	 * We use the idea of "zones" into which a collection of handles will be enqueued, or you can enqueue directly
	 * using the handle (adjusted using the filter).
	 */
	private function buildAssetsToEnqueue() :array {
		$con = $this->con();

		$enqueueZones = [];
		switch ( current_action() ) {
			case 'admin_enqueue_scripts':
				if ( $this->adminHookSuffix === $this->getPluginAdminHookSuffix() ) {
					$enqueueZones[] = 'plugin_admin';
				}
				else {
					$enqueueZones[] = 'wp_admin';
				}
				break;
			case 'wp_enqueue_scripts':
				$enqueueZones[] = 'badge';
				break;
			case 'login_enqueue_scripts':
				$enqueueZones[] = 'page_login';
				break;
			default:
				break;
		}

		$customAssets = apply_filters( $this->ctr()->prefix->hook( 'custom_enqueue_assets' ), [], $this->adminHookSuffix );

		$assets = [
			self::JS  => [],
			self::CSS => [],
		];
		foreach ( $con->cfg->includes[ 'dist' ] as $dist ) {

			if ( \count( \array_intersect( $enqueueZones, $dist[ 'zones' ] ?? [] ) ) > 0
				 || \in_array( $dist[ 'handle' ], $customAssets ) ) {

				$this->enqueuedHandles[] = $dist[ 'handle' ];

				foreach ( \array_keys( $assets ) as $type ) {
					if ( \in_array( $type, $dist[ 'types' ] ) ) {
						$assets[ $type ][] = $this->normaliseHandle( $dist[ 'handle' ] );
					}
				}
			}
		}

		$this->enqueuedHandles = \array_unique( $this->enqueuedHandles );

		return $assets;
	}

	private function localise() {
		$locals = apply_filters( $this->ctr()->prefix->hook( 'custom_localisations' ), [], $this->adminHookSuffix, $this->enqueuedHandles );
		foreach ( \is_array( $locals ) ? $locals : [] as $local ) {
			if ( \is_array( $local ) && \count( $local ) === 3 ) {
				wp_localize_script( $this->normaliseHandle( $local[ 0 ] ), $local[ 1 ], $local[ 2 ] );
			}
			else {
				\error_log( 'Invalid localisation: '.\var_export( $local, true ) );
			}
		}
	}

	private function normaliseHandles( array $handles ) :array {
		return \array_map(
			function ( $handle ) {
				return $this->normaliseHandle( $handle );
			},
			$handles
		);
	}

	private function normaliseHandle( string $handle ) :string {
		$pfx = $this->ctr()->prefix;
		if ( \str_starts_with( $handle, 'wp-' ) ) {
			$handle = \preg_replace( '#^wp-#', '', $handle );
		}
		elseif ( !\str_starts_with( $handle, $pfx->hook( 'tp' ) ) ) {
			$handle = $pfx->pfx( $handle );
		}
		return $handle;
	}
}