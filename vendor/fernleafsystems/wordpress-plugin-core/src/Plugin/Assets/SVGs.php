<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Assets;

use FernleafSystems\Wordpress\Services\Services;

class SVGs extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	public function raw( string $image ) :string {
		return empty( $image ) ? '' :
			(string)Services::WpFs()->getFileContent( $this->ctr()->paths->forSVG( $image ) );
	}

	public function url( string $image ) :string {
		return $this->ctr()->urls->forImage( $image );
	}
}