<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Registry;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\Controller;

class AptoPluginRegistry extends \FernleafSystems\Utilities\Data\Adapter\DynPropertiesClass {

	private static $Registry = [];

	public static function Register( Controller $con ) {
		self::$Registry[ $con->cntnr->prefix->pfx() ] = $con;
	}

	public static function Registered( Controller $con ) :bool {
		return isset( self::$Registry[ $con->cntnr->prefix->pfx() ] );
	}

	public static function Get( string $prefix ) :?Controller {
		return self::$Registry[ $prefix ] ?? null;
	}
}