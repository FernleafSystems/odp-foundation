<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Crons;

use FernleafSystems\Utilities\Logic\ExecOnce;

abstract class BaseCron extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	use ExecOnce;
	use StandardCron;

	protected function run() {
		$this->setupCron();
	}
}