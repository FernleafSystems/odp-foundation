<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Crons;

use FernleafSystems\Wordpress\Services\Services;

class HourlyCron extends BaseCron {

	/**
	 * @return string
	 */
	protected function getCronFrequency() {
		return 'hourly';
	}

	protected function getCronName() :string {
		return $this->ctr()->prefix->pfx( 'hourly' );
	}

	public function getFirstRunTimestamp() :int {
		return Services::Request()
					   ->carbon( true )
					   ->addHour()
					   ->minute( \rand( 1, 59 ) )
					   ->second( 0 )->timestamp;
	}

	public function runCron() {
		do_action( $this->ctr()->prefix->hook( 'hourly_cron' ) );
	}
}