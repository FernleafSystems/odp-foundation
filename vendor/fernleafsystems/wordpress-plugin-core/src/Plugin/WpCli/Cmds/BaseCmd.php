<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\WpCli\Cmds;

use FernleafSystems\Utilities\Logic\ExecOnce;

abstract class BaseCmd extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	use ExecOnce;

	protected $execCmdFlags;

	protected $execCmdArgs;

	protected function run() {
		try {
			$this->declareCmd();
		}
		catch ( \Exception $e ) {
		}
	}

	/**
	 * @throws \Exception
	 */
	protected function declareCmd() :void {
		\WP_CLI::add_command(
			$this->cmdBuildParts(),
			$this->cmdExec(),
			$this->mergeCommonCmdArgs( [
				'shortdesc' => $this->cmdShortDescription(),
				'synopsis'  => $this->cmdSynopsis(),
			] )
		);
	}

	abstract protected function cmdShortDescription() :string;

	protected function cmdExec() :callable {
		return [ $this, 'execCmd' ];
	}

	abstract protected function cmdParts() :array;

	abstract protected function cmdSynopsis() :array;

	/**
	 * @throws \Exception
	 */
	public function execCmd( array $flags, array $args ) :void {
		$this->execCmdFlags = $flags;
		$this->execCmdArgs = $args;
		$this->preRunCmd();
		$this->runCmd();
		$this->postRunCmd();
	}

	protected function preRunCmd() {
	}

	/**
	 * @throws \WP_CLI\ExitException
	 */
	abstract protected function runCmd() :void;

	protected function postRunCmd() {
	}

	protected function cmdBuildParts() :string {
		return \implode( ' ', \array_merge( $this->getCmdBase(), $this->cmdParts() ) );
	}

	protected function getCmdBase() :array {
		return [
			$this->ctr()->prefix->pfx()
		];
	}

	protected function mergeCommonCmdArgs( array $args ) :array {
		return \array_merge( $this->getCommonCmdArgs(), $args );
	}

	protected function getCommonCmdArgs() :array {
		return [
			'before_invoke' => function () {
				$this->beforeInvokeCmd();
			},
			'after_invoke'  => function () {
				$this->afterInvokeCmd();
			},
			'when'          => 'before_wp_load',
		];
	}

	protected function afterInvokeCmd() {
	}

	protected function beforeInvokeCmd() {
	}

	protected function isForceFlag() :bool {
		return (bool)\WP_CLI\Utils\get_flag_value( $this->execCmdArgs, 'force', false );
	}
}