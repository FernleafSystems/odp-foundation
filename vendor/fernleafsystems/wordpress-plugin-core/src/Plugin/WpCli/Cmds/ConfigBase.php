<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\WpCli\Cmds;

abstract class ConfigBase extends BaseCmd {

	/**
	 * @return string[]
	 */
	protected function getOptionsForWpCli( ?string $module = null ) :array {
		$config = $this->con()->cfg->configuration;
		return \array_filter(
			\array_keys( empty( $module ) ? $config->options : $config->optsForModule( $module ) ),
			function ( $key ) {
				return $this->ctr()->opts->optDef( $key )[ 'section' ] !== 'section_hidden';
			}
		);
	}
}