<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\WpCli;

use FernleafSystems\Utilities\Logic\ExecOnce;
use FernleafSystems\Wordpress\Services\Services;

class WpCliCon extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	use ExecOnce;

	protected $commands;

	public function __construct( array $commands ) {
		$this->commands = $commands;
	}

	protected function canRun() :bool {
		return Services::WpGeneral()->isWpCli();
	}

	protected function run() {
		add_action( 'cli_init', function () {
			try {
				\array_map(
					function ( $handlerClass ) {
						/** @var Cmds\BaseCmd $handlerClass */
						( new $handlerClass() )->execute();
					},
					$this->commands
				);
			}
			catch ( \Exception $e ) {
			}
		} );
	}
}