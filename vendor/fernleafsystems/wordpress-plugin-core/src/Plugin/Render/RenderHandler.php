<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Render;

use FernleafSystems\Utilities\Logic\ExecOnce;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Utilities\HookAndFiltersSetup;
use FernleafSystems\Wordpress\Services\Services;

class RenderHandler extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	use ExecOnce;

	private $templateRoots;

	private $autoRenders = [];

	public function __construct( array $roots = [] ) {
		$this->templateRoots = \array_filter( $roots, function ( $path ) {
			return !empty( $path ) && Services::WpFs()->isAccessibleDir( $path );
		} );
	}

	protected function run() {
		$this->setupAutoRenderHooks();
	}

	protected function setupAutoRenderHooks() {
		HookAndFiltersSetup::RunHooksSetup( $this->getHooks(), $this );
	}

	protected function getHooks() :array {
		return [
			[
				'hook' => 'admin_footer',
				'cb'   => 'processAutoRenders',
			],
			[
				'hook' => 'wp_footer',
				'cb'   => 'processAutoRenders',
			],
		];
	}

	public function addAutoRender( string $renderClass ) :void {
		$this->autoRenders[] = $renderClass;
	}

	public function processAutoRenders() :void {
		foreach ( \array_unique( $this->autoRenders ) as $autoRender ) {
			echo $this->ctr()->actions->render( $autoRender );
		}
	}

	public function getRenderer() :\FernleafSystems\Wordpress\Services\Utilities\Render {
		$render = Services::Render();
		foreach ( $this->templateRoots as $dir ) {
			$render->setTwigTemplateRoot( trailingslashit( $dir ) );
		}
		return $render;
	}
}