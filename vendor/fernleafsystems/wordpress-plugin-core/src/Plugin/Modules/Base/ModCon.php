<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Modules\Base;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Config\Modules\ModConfigVO;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Crons\PluginCronsConsumer;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Utilities\HookAndFiltersSetup;

class ModCon extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	use PluginCronsConsumer;

	/**
	 * @var ModConfigVO
	 */
	public $cfg;

	private $isBooted = false;

	public function __construct( ModConfigVO $cfg ) {
		$this->cfg = $cfg;
	}

	/**
	 * @throws \Exception
	 */
	public function boot() {
		if ( !$this->isBooted ) {
			$this->isBooted = true;
			$this->bootUp();
		}
	}

	protected function bootUp() {
		$this->setupHooks();
	}

	protected function setupHooks() {
		HookAndFiltersSetup::RunHooksSetup( $this->getHooks(), $this );
		$this->setupCronHooks();
	}

	protected function getHooks() :array {
		return [
			[
				'hook' => 'init',
				'cb'   => 'onWpInit',
			],
			[
				'hook' => 'wp_loaded',
				'cb'   => 'onWpLoaded',
			],
			[
				'hook' => 'admin_init',
				'cb'   => 'onWpAdminInit',
			],
			[
				'hook' => 'wp',
				'cb'   => 'onWP',
			],
		];
	}

	public function isModOptEnabled() :bool {
		return self::ctr()->opts->optIs( $this->getEnableModOptKey(), 'Y' );
	}

	public function getEnableModOptKey() :string {
		return 'enable_'.$this->cfg->slug;
	}
}