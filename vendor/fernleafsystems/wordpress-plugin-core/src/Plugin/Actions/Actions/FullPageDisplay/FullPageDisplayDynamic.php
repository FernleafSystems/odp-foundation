<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\FullPageDisplay;

class FullPageDisplayDynamic extends BaseFullPageDisplay {

	public const SLUG = 'display_full_page_dynamic';
}