<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Traits;

trait AnyUserAuthRequired {

	protected function getMinimumUserAuthCapability() :string {
		return 'read';
	}
}