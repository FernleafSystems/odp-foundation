<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Traits;

trait NonceVerifyRequired {

	protected function isNonceVerifyRequired() :bool {
		return true;
	}
}