<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Traits;

trait NonceVerifyNotRequired {

	protected function isNonceVerifyRequired() :bool {
		return false;
	}
}