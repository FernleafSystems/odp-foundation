<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Traits;

trait PluginAdminNotRequired {

	protected function isPluginAdminRequired() :bool {
		return false;
	}
}