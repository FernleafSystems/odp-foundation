<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\ActionDataBuilder;

class AjaxRender extends BaseAction {

	use Traits\AnyUserAuthRequired;
	use Traits\PluginAdminNotRequired;

	public const SLUG = 'ajax_render';

	protected function exec() {
		$response = $this->ctr()->actions->action(
			$this->action_data[ 'render_slug' ],
			$this->getParamsMinusAjax()
		);
		foreach ( [ 'success', 'message', 'error' ] as $item ) {
			if ( isset( $response->action_response_data[ $item ] ) ) {
				$response->{$item} = $response->action_response_data[ $item ];
			}
		}

		$this->setResponse( $response );
	}

	protected function getParamsMinusAjax() :array {
		return \array_diff_key(
			$this->action_data,
			\array_flip( [
				ActionDataBuilder::FIELD_ACTION,
				ActionDataBuilder::FIELD_EXECUTE,
				ActionDataBuilder::FIELD_NONCE,
				ActionDataBuilder::FIELD_WRAP_RESPONSE,
				'render_slug'
			] )
		);
	}

	protected function getRequiredDataKeys() :array {
		return [
			'render_slug'
		];
	}
}