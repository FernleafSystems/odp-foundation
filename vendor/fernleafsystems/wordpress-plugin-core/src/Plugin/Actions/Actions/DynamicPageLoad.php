<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions;

class DynamicPageLoad extends BaseAction {

	use Traits\PluginAdminNotRequired;

	public const SLUG = 'dynamic_page_load';

	protected function exec() {
		$resp = $this->response();

		$params = $this->action_data[ 'dynamic_load_params' ];
		try {
			$handler = $this->ctr()->plugin_pages->getPageHandlerFor( $params[ 'nav' ], $params[ 'subnav' ] );

			$resp->action_response_data = [
				'html'       => $this->ctr()->actions->render( $handler, $params ),
				'page_url'   => $this->ctr()->plugin_urls->adminTopNav( $params[ 'nav' ], $params[ 'subnav' ] ),
				'page_title' => 'No title yet',
			];
			$resp->success = true;
		}
		catch ( \Exception $e ) {
			$resp->success = false;
			$resp->message = $e->getMessage();
		}
	}

	protected function getRequiredDataKeys() :array {
		return [
			'dynamic_load_params',
		];
	}
}