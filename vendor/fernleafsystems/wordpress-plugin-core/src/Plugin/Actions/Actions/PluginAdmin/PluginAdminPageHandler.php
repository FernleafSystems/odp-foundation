<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\PluginAdmin;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Render\PageAdminPlugin;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Traits\NonceVerifyNotRequired;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Traits\PluginAdminNotRequired;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\Plugin\PluginPages;
use FernleafSystems\Wordpress\Services\Services;

class PluginAdminPageHandler extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\BaseAction {

	use NonceVerifyNotRequired;
	use PluginAdminNotRequired;

	public const SLUG = 'plugin_admin_page_handler';

	protected $pageHookSuffix;

	protected $screenID;

	protected function exec() {
		if ( !Services::WpGeneral()->isAjax()
			 && apply_filters( $this->ctr()->prefix->hook( 'show_admin_menu' ), $this->con()->cfg->menu[ 'show' ] ?? true ) ) {

			add_action( 'admin_menu', function () {
				if ( !Services::WpGeneral()->isMultisite() && is_admin() ) {
					$this->createAdminMenu();
				}
			} );

			add_action( 'network_admin_menu', function () {
				if ( Services::WpGeneral()->isMultisite() && is_network_admin() && is_main_network() ) {
					$this->createNetworkAdminMenu();
				}
			} );

			add_filter( 'nocache_headers', [ $this, 'adjustNocacheHeaders' ] );
		}
	}

	/**
	 * In order to prevent certain errors when the back button is used
	 * @param array $h
	 * @return array
	 */
	public function adjustNocacheHeaders( $h ) {
		if ( \is_array( $h ) && !empty( $h[ 'Cache-Control' ] ) && $this->con()->isPluginAdminPageRequest() ) {
			$Hs = \array_map( '\trim', \explode( ',', $h[ 'Cache-Control' ] ) );
			$Hs[] = 'no-store';
			$h[ 'Cache-Control' ] = \implode( ', ', \array_unique( $Hs ) );
		}
		return $h;
	}

	private function createAdminMenu() {
		$con = $this->con();
		$menu = $con->cfg->menu;

		if ( $menu[ 'top_level' ] ) {

			$this->pageHookSuffix = add_menu_page(
				$con->labels->Name,
				$con->labels->MenuTitle,
				$con->cfg->properties[ 'base_permissions' ],
				$con->cntnr->plugin_urls->rootAdminPageSlug(),
				[ $this, 'displayModuleAdminPage' ],
				$con->labels->icon_url_16x16
			);

			if ( $menu[ 'has_submenu' ] ) {
				$this->addSubMenuItems();
			}

			if ( $menu[ 'do_submenu_fix' ] ) {
				global $submenu;
				$menuID = $con->cntnr->plugin_urls->rootAdminPageSlug();
				if ( isset( $submenu[ $menuID ] ) ) {
//					$submenu[ $menuID ][ 0 ][ 0 ] = __( 'Security Dashboard', 'wp-simple-firewall' );
					unset( $submenu[ $menuID ][ 0 ] );
				}
				else {
					// remove entire top-level menu if no submenu items - ASSUMES this plugin MUST have submenu or no menu at all
					remove_menu_page( $menuID );
				}
			}
		}
	}

	private function createNetworkAdminMenu() {
		$this->createAdminMenu();
	}

	protected function addSubMenuItems() {
		$con = $this->con();

		$currentNav = $this->action_data[ PluginPages::FIELD_NAV ] ?? '';
		foreach ( $con->cntnr->plugin_pages->getPages() as $page ) {
			foreach ( $page[ 'sub_navs' ] as $subNav ) {
				if ( $subNav[ 'admin_menu' ][ 'show' ] ) {
					$submenuNavID = $page[ 'id' ].'_'.$subNav[ 'id' ];
					$submenuTitle = $subNav[ 'admin_menu' ][ 'name' ];

					$markupTitle = sprintf( '<span style="color:#fff;font-weight: 600">%s</span>', $submenuTitle );
					$doMarkupTitle = $currentNav === $submenuNavID;

					add_submenu_page(
						$con->cntnr->plugin_urls->rootAdminPageSlug(),
						sprintf( '%s | %s', $submenuTitle, $con->labels->Name ),
						$doMarkupTitle ? $markupTitle : $submenuTitle,
						$con->cfg->properties[ 'base_permissions' ],
						$con->cntnr->prefix->pfx( $submenuNavID ),
						[ $this, 'displayModuleAdminPage' ]
					);
				}
			}
		}
	}

	public function displayModuleAdminPage() {
		echo $this->ctr()->actions->render( PageAdminPlugin::class );
	}
}