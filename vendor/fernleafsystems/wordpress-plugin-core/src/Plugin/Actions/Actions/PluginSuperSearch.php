<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Components\Search\SelectSearchData;

class PluginSuperSearch extends BaseAction {

	public const SLUG = 'super_search_select';

	protected function exec() {
		$this->response()->action_response_data = [
			'success' => true,
			'results' => ( new SelectSearchData() )->build( $this->action_data[ 'search' ] ?? '' ),
		];
	}
}