<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\DynamicLoad;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Traits\PluginAdminNotRequired;

abstract class Base extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\BaseAction {

	use PluginAdminNotRequired;

	protected function exec() {
		$resp = $this->response();
		try {
			$resp->action_response_data = [
				'html'       => $this->getContent(),
				'page_url'   => $this->getPageUrl(),
				'page_title' => $this->getPageTitle(),
			];
			$resp->success = true;
		}
		catch ( \Exception $e ) {
			$resp->success = false;
			$resp->message = $e->getMessage();
		}
	}

	abstract protected function getContent() :string;

	abstract protected function getPageTitle() :string;

	abstract protected function getPageUrl() :string;
}