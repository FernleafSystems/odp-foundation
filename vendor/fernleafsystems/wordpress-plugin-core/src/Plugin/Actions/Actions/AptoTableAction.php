<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Traits\AnyUserAuthRequired;

class AptoTableAction extends BaseAction {

	use AnyUserAuthRequired;

	public const SLUG = 'apto_table_action';

	protected function exec() {
		try {
			/** @var \FernleafSystems\Wordpress\Plugin\Core\Plugin\Tables\DataTables\TableActionsDelegator $del */
			$del = $this->ctr()->get( 'handler.table_delegate_handlers' );
			$response = $del->delegate(
				$this->action_data[ 'table_id' ],
				$this->action_data[ 'table_action' ],
				$this->action_data[ 'table_data' ] ?? []
			);
		}
		catch ( \Exception $e ) {
			$response = [
				'success'     => false,
				'page_reload' => true,
				'message'     => $e->getMessage(),
			];
		}

		$this->response()->action_response_data = $response;
	}
}