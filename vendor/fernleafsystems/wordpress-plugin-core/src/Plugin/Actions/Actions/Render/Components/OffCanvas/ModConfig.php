<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Render\Components\OffCanvas;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\DynamicPageLoad;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Config\Modules\Ops\StringsModules;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\Plugin\PluginPages;

class ModConfig extends OffCanvasBase {

	public const SLUG = 'offcanvas_modconfig';

	protected function buildCanvasTitle() :string {
		/** @var StringsModules $strings */
		$strings = $this->ctr()->get( 'handler.strings.modules' );
		return sprintf( '%s: %s', __( 'Configuration', 'wp-simple-firewall' ), $strings->getFor( $this->findModule() )[ 'name' ] );
	}

	protected function buildCanvasBody() :string {

		$con = $this->con();
		$config = $con->cfg->configuration;

		$configItem = $this->action_data[ 'config_item' ] ?? null;

		// Determine what the config item is. We can link to an option, a section, or a whole module.
		if ( isset( $config->modules[ $configItem ] ) ) {
			$itemType = 'module';
		}
		else {
			$optDef = $config->options[ $configItem ] ?? null;
			if ( empty( $optDef ) ) {
				$itemType = 'section';
			}
			else {
				$itemType = 'option';
			}
		}

		return $this->ctr()->actions->action( DynamicPageLoad::class, [
			'dynamic_load_params' => [
				'nav'             => PluginPages::NAV_OPTIONS_CONFIG,
				'subnav'          => $this->findModule(),
				'mod_slug'        => $this->findModule(),
				'focus_item'      => $configItem,
				'focus_item_type' => $itemType,
				'form_context'    => 'offcanvas',
			]
		] )->action_response_data[ 'html' ];
	}

	private function findModule() :string {
		$config = $this->con()->cfg->configuration;

		$configItem = $this->action_data[ 'config_item' ] ?? null;

		if ( isset( $config->modules[ $configItem ] ) ) {
			$module = $configItem;
		}
		else {
			$optDef = $config->options[ $configItem ] ?? null;
			if ( empty( $optDef ) ) {
				$module = $config->sections[ $configItem ][ 'module' ];
			}
			else {
				$module = $config->sections[ $optDef[ 'section' ] ][ 'module' ];
			}
		}

		return $module;
	}
}