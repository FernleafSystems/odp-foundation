<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Render;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Render\PluginAdminPages\PageDynamicLoad;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Traits\PluginAdminNotRequired;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Exceptions\ActionException;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\Plugin\PluginPages;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Navigation\NavMenuBuilder;

class PageAdminPlugin extends BaseRender {

	use PluginAdminNotRequired;

	public const SLUG = 'page_admin_plugin';
	public const TEMPLATE = '/wpadmin/plugin_admin/top_page/index.twig';

	protected function getRenderData() :array {
		$con = $this->con();

		// The particular renderer for the main page body area, based on navigation
		$nav = $this->determineNav();
		$subNav = $this->determineSubNav();
		try {
			$pages = $this->ctr()->plugin_pages;
			$delegateClass = $pages->isPageDynamic( $this->determineNav(), $this->determineSubNav() ) ?
				PageDynamicLoad::class : $pages->getPageHandlerFor( $this->determineNav(), $this->determineSubNav() );
		}
		catch ( \Exception $e ) {
			throw new ActionException( sprintf( 'Unavailable nav handling: %s %s', $nav, $subNav ) );
		}

		/** @var NavMenuBuilder $navBuilder */
		$navBuilder = $this->ctr()->get( 'handler.nav_menu_builder' );
		return [
			'classes' => [
				'page_container' => 'page-insights page-'.$nav
			],
			'content' => [
				'rendered_page_body' => $this->ctr()->actions->render( $delegateClass, [
					PluginPages::FIELD_NAV    => $nav,
					PluginPages::FIELD_SUBNAV => $subNav,
				] ),
			],
			'flags'   => [
				'is_advanced' => false,
			],
			'imgs'    => [
				'logo_banner' => $con->labels->url_img_pagebanner,
				'logo_small'  => $con->labels->url_img_logo_small,
			],
			'strings' => [
				'top_page_warnings' => $this->buildTopPageWarnings(),
			],
			'vars'    => [
				'active_module_settings' => $subNav,
				'navbar_menu'            => $navBuilder->build(),
			],
		];
	}

	protected function determineNav() :string {
		return $this->action_data[ PluginPages::FIELD_NAV ] ?? '';
	}

	protected function determineSubNav() :string {
		return $this->action_data[ PluginPages::FIELD_SUBNAV ] ?? '';
	}

	protected function buildTopPageWarnings() :array {
		return [];
		return \array_filter(
			( new Handler() )->build(),
			function ( array $issue ) {
				return \in_array( 'shield_admin_top_page', $issue[ 'locations' ] );
			}
		);
	}
}