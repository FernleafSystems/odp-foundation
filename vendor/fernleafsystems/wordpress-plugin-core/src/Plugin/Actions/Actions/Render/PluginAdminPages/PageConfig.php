<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Render\PluginAdminPages;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Render\Components\Options\OptionsForm;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Config\Modules\Ops\StringsModules;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\Plugin\PluginPages;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Navigation\BuildAdminBreadCrumbs;

class PageConfig extends BasePluginAdminPage {

	public const SLUG = 'admin_plugin_page_config';
	public const TEMPLATE = '/wpadmin/plugin_admin/inner_page/mod_config.twig';

	protected function getRenderData() :array {
		/** @var StringsModules $stringsModules */
		$stringsModules = $this->ctr()->get( 'handler.strings.modules' );
		$modStrings = $stringsModules->getFor( $this->action_data[ 'mod_slug' ] );
		return [
			'content' => [
				'options_form' => $this->ctr()->actions->render( OptionsForm::class, $this->action_data ),
			],
			'imgs'    => [
				'inner_page_title_icon' => '' //$this->ctr()->svgs->raw( 'sliders' ),
			],
			'strings' => [
				'inner_page_title'    => $modStrings[ 'name' ],
				'inner_page_subtitle' => $modStrings[ 'subtitle' ],
			],
		];
	}

	/**
	 * Must manually build breadcrumbs for dynamic loaded config.
	 */
	protected function getBreadCrumbs() :array {
		$crumbs = parent::getBreadCrumbs();
		if ( empty( $crumbs ) ) {
			/** @var BuildAdminBreadCrumbs $builder */
			$builder = $this->ctr()->get( 'handler.build_admin_bread_crumbs' );
			$crumbs = $builder->for( PluginPages::NAV_OPTIONS_CONFIG, $this->action_data[ 'mod_slug' ] );
		}
		return $crumbs;
	}
}