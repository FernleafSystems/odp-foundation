<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Render\Components;

class WaitSpinner extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Render\BaseRender {

	public const TEMPLATE = '/components/html/wait_spinner.twig';
}