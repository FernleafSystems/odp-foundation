<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Render\Components;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Traits\PluginAdminNotRequired;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\AdminNotices\NoticeVO;

class AdminNotice extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Render\BaseRender {

	use PluginAdminNotRequired;

	public const SLUG = 'render_admin_notice';
	public const TEMPLATE = '/snippets/prerendered.twig';

	protected function getRenderData() :array {
		$notice = ( new NoticeVO() )->applyFromArray( $this->action_data[ 'raw_notice_data' ] );

		$data = $notice->render_data;

		if ( empty( $data[ 'notice_classes' ] ) || !\is_array( $data[ 'notice_classes' ] ) ) {
			$data[ 'notice_classes' ] = [];
		}
		$data[ 'notice_classes' ][] = $notice->type;
		if ( !\in_array( 'error', $data[ 'notice_classes' ] ) ) {
			$data[ 'notice_classes' ][] = 'updated';
		}
		$data[ 'notice_classes' ][] = 'notice-'.$notice->id;
		$data[ 'notice_classes' ] = \implode( ' ', \array_unique( $data[ 'notice_classes' ] ) );

		$data[ 'unique_render_id' ] = \uniqid( (string)$notice->id );
		$data[ 'notice_id' ] = $notice->id;

		$data[ 'imgs' ] = [
			'icon_shield' => $this->ctr()->svgs->raw( 'shield-shaded.svg' ),
		];

		return $data;
	}

	protected function getRenderTemplate() :string {
		return ( new NoticeVO() )->applyFromArray( $this->action_data[ 'raw_notice_data' ] )->template;
	}

	protected function getRequiredDataKeys() :array {
		return [
			'raw_notice_data'
		];
	}
}