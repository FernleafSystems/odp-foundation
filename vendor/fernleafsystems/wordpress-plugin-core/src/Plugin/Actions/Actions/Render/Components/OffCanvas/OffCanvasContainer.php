<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Render\Components\OffCanvas;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Traits;

class OffCanvasContainer extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Render\BaseRender {

	use Traits\AuthNotRequired;

	public const TEMPLATE = '/components/html/offcanvas_container.twig';
}