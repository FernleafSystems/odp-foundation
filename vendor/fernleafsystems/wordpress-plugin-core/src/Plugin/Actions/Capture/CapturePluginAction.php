<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Capture;

use FernleafSystems\Wordpress\Services\Services;

class CapturePluginAction extends CaptureActionBase {

	protected function canRun() :bool {
		return !$this->ctr()->this_req->wp_is_ajax && parent::canRun();
	}

	protected function theRun() {
		$req = Services::Request();
		try {
			$this->actionResponse = $this->ctr()->actions->action(
				$this->extractActionSlug(),
				\array_merge( $req->query, $req->post )
			);
		}
		catch ( \Exception $e ) {
			error_log( $e->getMessage() );
		}
	}

	protected function postRun() {
		if ( !empty( $this->actionResponse ) && !empty( $this->actionResponse->next_step ) ) {
			switch ( $this->actionResponse->next_step[ 'type' ] ) {
				case 'redirect':
					$url = $this->actionResponse->next_step[ 'url' ];
					Services::Response()->redirect( empty( $url ) ? Services::WpGeneral()->getHomeUrl() : $url );
					break;
				default:
					break;
			}
		}
	}
}