<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Capture;

use FernleafSystems\Utilities\Logic\ExecOnce;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\ActionDataBuilder;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\ActionResponse;
use FernleafSystems\Wordpress\Services\Services;

class CaptureActionBase extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	use ExecOnce;

	/**
	 * @var ?ActionResponse
	 */
	protected $actionResponse;

	protected function canRun() :bool {
		$req = Services::Request();
		$actionBuilder = $this->ctr()->actions->dataBuilder();
		return $req->request( $actionBuilder::FIELD_ACTION ) === $actionBuilder->pluginActionField()
			   && !empty( $req->request( $actionBuilder::FIELD_EXECUTE ) )
			   && \preg_match( '#^[a-z0-9_.:\-]+$#', $req->request( $actionBuilder::FIELD_EXECUTE ) );
	}

	protected function extractActionSlug() :string {
		\preg_match( '#^([a-z0-9_.:\-]+)$#', Services::Request()
													 ->request( ActionDataBuilder::FIELD_EXECUTE ), $matches );
		return $matches[ 1 ];
	}

	protected function run() {
		$this->preRun();
		$this->theRun();
		$this->postRun();
	}

	protected function preRun() {
	}

	protected function theRun() {
	}

	protected function postRun() {
	}
}