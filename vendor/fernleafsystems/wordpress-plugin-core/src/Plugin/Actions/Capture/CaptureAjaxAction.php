<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Capture;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Exceptions\{
	ActionException,
	InvalidActionNonceException,
	PluginAdminRequiredException
};
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Ajax\Response;
use FernleafSystems\Wordpress\Services\Services;

class CaptureAjaxAction extends CaptureActionBase {

	protected function canRun() :bool {
		return $this->ctr()->this_req->wp_is_ajax && parent::canRun();
	}

	protected function theRun() {
		$actionBuilder = $this->ctr()->actions->dataBuilder();
		foreach (
			[
				'wp_ajax_'.$actionBuilder->pluginActionField()        => 1,
				'wp_ajax_nopriv_'.$actionBuilder->pluginActionField() => 1,
			] as $hook => $priority
		) {
			add_action( $hook, function () {
				$this->ajaxAction();
			}, $priority );
		}
	}

	private function ajaxAction() {
		$con = $this->con();

		$req = Services::Request();
		try {
			$router = $con->cntnr->actions;
			\ob_start();
			$response = $this->normaliseAjaxResponse(
				$router
					->action( $this->extractActionSlug(), $req->post, $router::ACTION_AJAX )
					->action_response_data
			);
		}
		catch ( InvalidActionNonceException $e ) {
			$statusCode = 401;
			$msg = __( 'Nonce Failed.', 'wp-simple-firewall' );
			$response = [
				'success' => false,
				'message' => $msg,
				'error'   => $msg,
			];
		}
		catch ( PluginAdminRequiredException $e ) {
			$statusCode = 401;
			$msg = \implode( ' ', [
				__( 'You must be authorised as a Plugin Admin to perform this action.', 'wp-simple-firewall' ),
				__( 'You may need to reload this page to continue.', 'wp-simple-firewall' ),
			] );
			$response = [
				'success' => false,
				'message' => $msg,
				'error'   => $msg,
			];
		}
		catch ( ActionException $e ) {
			$statusCode = empty( $e->getCode() ) ? 400 : $e->getCode();
			$response = [
				'success' => false,
				'message' => $e->getMessage(),
				'error'   => $e->getMessage(),
			];
		}
		finally {
			$noise = \ob_get_clean();
		}

		if ( !empty( $response ) ) {
			( new Response() )->issue( [
				'success'     => $response[ 'success' ] ?? false,
				'data'        => \array_diff_key( $response, \array_flip( [
					'action_data',
					/** TODO: refine action process to ensure that excess data isn't included */
				] ) ),
				'noise'       => $noise,
				'status_code' => $statusCode ?? 200
			] );
		}
	}

	/**
	 * We check for empty since if it's empty, there's nothing to normalize. It's a filter,
	 * so if we send something back non-empty, it'll be treated like a "handled" response and
	 * processing will finish
	 */
	protected function normaliseAjaxResponse( array $ajaxResponse ) :array {
		if ( !empty( $ajaxResponse ) ) {
			$ajaxResponse = \array_merge(
				[
					'success'     => false,
					'page_reload' => false,
					'message'     => 'No AJAX Message provided',
					'html'        => '',
				],
				$ajaxResponse
			);
		}
		return $ajaxResponse;
	}
}