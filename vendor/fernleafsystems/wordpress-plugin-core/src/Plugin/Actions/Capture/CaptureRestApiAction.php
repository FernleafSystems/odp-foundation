<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Capture;

class CaptureRestApiAction extends CaptureActionBase {

	protected function canRun() :bool {
		return $this->con()->is_rest_enabled;
	}

	protected function run() {
		add_action( 'rest_api_init', function () {
			$this->ctr()->rest->execute();
		} );
	}
}