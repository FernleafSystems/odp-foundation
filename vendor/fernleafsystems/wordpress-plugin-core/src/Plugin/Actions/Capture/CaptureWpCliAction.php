<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Capture;

use FernleafSystems\Wordpress\Services\Services;

class CaptureWpCliAction extends CaptureActionBase {

	protected function canRun() :bool {
		return Services::WpGeneral()->isWpCli();
	}

	protected function run() {
		add_action( 'cli_init', function () {
			$this->ctr()->wpcli->execute();
		} );
	}
}