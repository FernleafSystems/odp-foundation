<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions;

use FernleafSystems\Wordpress\Services\Services;
use FernleafSystems\Wordpress\Services\Utilities\{
	PasswordGenerator,
	URL
};

class ActionDataBuilder extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	public const FIELD_ACTION = 'action';
	public const FIELD_AJAXURL = 'ajaxurl';
	public const FIELD_EXECUTE = 'ex';
	public const FIELD_NONCE = 'exnonce';
	public const FIELD_REST_NONCE = '_wpnonce';
	public const FIELD_REST_URL = '_rest_url';
	public const FIELD_WRAP_RESPONSE = 'apto_wrap_response';

	public function pluginActionField() :string {
		return $this->ctr()->prefix->_( 'action' );
	}

	public function build( string $actionClass, bool $isAjax = true, array $aux = [], bool $uniq = false ) :array {
		$vo = new ActionDataVO();
		$vo->action = $actionClass;
		$vo->is_ajax = $isAjax;
		$vo->aux = $aux;
		$vo->unique = $uniq;
		return $this->buildVO( $vo );
	}

	public function buildVO( ActionDataVO $VO ) :array {
		/** @var ActionNonces $noncer */
		$noncer = $this->ctr()->get( 'handler.actions.nonces' );

		$data = \array_merge( [
			self::FIELD_ACTION  => $this->pluginActionField(),
			self::FIELD_EXECUTE => $VO->action::SLUG,
			self::FIELD_NONCE   => $noncer->create( $VO->action, $this->pluginActionField() ),
		], $VO->aux );

		if ( $VO->unique ) {
			$data[ 'uniq' ] = PasswordGenerator::Gen( 4, true, true, false );
		}

		if ( \count( $VO->excluded_fields ) > 0 ) {
			$data = \array_diff_key( $data, \array_flip( $VO->excluded_fields ) );
		}

		if ( $VO->is_ajax ) {
			$data[ self::FIELD_AJAXURL ] = Services::WpGeneral()->ajaxURL();

			$data[ self::FIELD_REST_NONCE ] = wp_create_nonce( 'wp_rest' );
			$data[ self::FIELD_REST_URL ] = URL::Build(
				get_rest_url( null, sprintf( '%s/v1/action/%s', $this->ctr()->prefix->pfx(), $VO->action::SLUG ) ),
				\array_intersect_key(
					$data,
					\array_flip( [
						self::FIELD_NONCE,
						self::FIELD_REST_NONCE,
					] )
				)
			);
		}

		return $data;
	}

	/**
	 * @param Actions\BaseAction|string $actionClass
	 */
	public function buildAjaxRender( string $actionClass = '', array $aux = [] ) :array {
		$aux[ 'render_slug' ] = empty( $actionClass ) ? '' : $actionClass::SLUG;
		return $this->build( Actions\AjaxRender::class, true, $aux );
	}

	public function buildJson( string $actionClass, bool $isAjax = true, array $aux = [] ) :string {
		return \json_encode( (object)$this->build( $actionClass, $isAjax, $aux ) );
	}
}