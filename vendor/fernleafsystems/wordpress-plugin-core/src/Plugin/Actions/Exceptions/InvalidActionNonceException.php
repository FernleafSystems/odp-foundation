<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Exceptions;

class InvalidActionNonceException extends ActionException {

}