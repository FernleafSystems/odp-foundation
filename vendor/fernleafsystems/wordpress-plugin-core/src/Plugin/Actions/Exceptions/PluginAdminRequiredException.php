<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Exceptions;

class PluginAdminRequiredException extends ActionException {

}