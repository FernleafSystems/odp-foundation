<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions;

use FernleafSystems\Utilities\Logic\ExecOnce;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\Render\PluginAdminPages\PagePluginAdminRestricted;
use FernleafSystems\Wordpress\Services\Services;

class ActionsController extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	use ExecOnce;

	public const ACTION_AJAX = 1;
	public const ACTION_SHIELD = 2;
	public const ACTION_REST = 3;

	private $actions;

	private $actionsMap = [];

	public function __construct( array $actions ) {
		$this->actions = $actions;
	}

	protected function run() {
		\array_map(
			function ( string $action ) {
				/** @var Capture\CaptureActionBase $handler */
				$handler = $this->ctr()->get( 'handler.actions.'.$action );
				$handler->execute();
			},
			[
				'capture_redirects',
				'capture_plugin_action',
				'capture_ajax',
				'capture_rest_api',
				'capture_wpcli',
			]
		);
	}

	public function dataBuilder() :ActionDataBuilder {
		return $this->ctr()->get( 'handler.actions.data_builder' );
	}

	/**
	 * @throws Exceptions\ActionDoesNotExistException
	 * @throws Exceptions\ActionException
	 * @throws Exceptions\ActionTypeDoesNotExistException
	 * @throws Exceptions\PluginAdminRequiredException
	 * @throws Exceptions\InvalidActionNonceException
	 */
	public function action( string $classOrSlug, array $data = [], int $type = self::ACTION_SHIELD ) :ActionResponse {

		switch ( $type ) {
			case self::ACTION_AJAX:
				$adapter = new ResponseAdapter\AjaxResponseAdapter();
				break;
			case self::ACTION_SHIELD:
				$adapter = new ResponseAdapter\PluginActionResponseAdapter();
				break;
			case self::ACTION_REST:
				$adapter = new ResponseAdapter\RestApiActionResponseAdapter();
				break;
			default:
				throw new Exceptions\ActionTypeDoesNotExistException( $type );
		}

		try {
			$response = ( new ActionProcessor() )
				->setCon( $this->con() )
				->processAction( $classOrSlug, $data );
		}
		catch ( Exceptions\PluginAdminRequiredException $sare ) {
			if ( Services::WpGeneral()->isAjax() ) {
				throw $sare;
			}
			$response = $this->action( Actions\Render\PluginAdminPages\PagePluginAdminRestricted::class, $data );
		}
		catch ( Exceptions\InvalidActionNonceException $iane ) {
			if ( Services::WpGeneral()->isAjax() ) {
				throw $iane;
			}
			wp_die( sprintf( 'Unexpected data. Please try again. Action Slug: "%s"; Data: "%s"', $classOrSlug, var_export( $data, true ) ) );
		}

		$adapter->adapt( $response );
		return $response;
	}

	/**
	 * This is an alias for calling the Render action directly
	 */
	public function render( string $classOrSlug, array $data = [] ) :string {
		try {
			$output = $this->action(
				Actions\Render::class,
				[
					'render_action_slug' => $classOrSlug,
					'render_action_data' => $data,
				]
			)->action_response_data[ 'render_output' ];
		}
		catch ( Exceptions\PluginAdminRequiredException $e ) {
			error_log( 'render::PluginAdminRequiredException: '.$classOrSlug );
			$output = $this->ctr()->actions->render( PagePluginAdminRestricted::class );
		}
		catch ( Exceptions\UserAuthRequiredException $uare ) {
//			error_log( 'render::UserAuthRequiredException: '.$classOrSlug );
			$output = '';
		}
		catch ( Exceptions\ActionException $e ) {
//			error_log( 'render::ActionException: '.$classOrSlug.' '.$e->getMessage() );
			$output = $e->getMessage();
		}

		return $output;
	}

	/**
	 * @return Actions\BaseAction|string
	 */
	public function getActionFromSlug( string $classOrSlug ) :string {
		if ( !isset( $this->actionsMap[ $classOrSlug ] ) ) {
			if ( \class_exists( $classOrSlug ) ) {
				$this->actionsMap[ $classOrSlug::SLUG ] = $classOrSlug;
				$classOrSlug = $classOrSlug::SLUG;
			}
			else {
				foreach ( $this->actions as $action ) {
					if ( \class_exists( $action ) && $action::SLUG === $classOrSlug ) {
						$this->actionsMap[ $classOrSlug ] = $action;
						break;
					}
				}
			}
		}
		return $this->actionsMap[ $classOrSlug ] ?? '';
	}
}