<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\Actions\BaseAction;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase;
use FernleafSystems\Wordpress\Services\Services;

class ActionNonces extends PluginControllerConsumerBase {

	/**
	 * @param BaseAction|string $action
	 */
	public function create( string $action, string $pluginActionFieldName ) :string {
		return $this->createNonces( $action, $pluginActionFieldName )[ 0 ];
	}

	/**
	 * @param Actions\BaseAction|string $action
	 */
	public function createNonces( string $action, string $pluginActionFieldName ) :array {
		return \array_map(
			function ( int $distance ) use ( $action, $pluginActionFieldName ) {
				$action = $this->ctr()->actions->getActionFromSlug( $action );

				$nonceCfg = \array_merge( [
					'ip'  => true,
					'ttl' => 12,
				], $action::NonceCfg() );

				return \substr( wp_hash( \implode( '|', [
					sprintf( '%s-%s', $pluginActionFieldName, $action::SLUG ),
					Services::WpUsers()->getCurrentWpUserId(),
					$nonceCfg[ 'ip' ] ? Services::Request()->ip() : '-',
					\ceil( Services::Request()->ts()/( \HOUR_IN_SECONDS*$nonceCfg[ 'ttl' ] ) ) - $distance,
				] ), 'nonce' ), -12, 10 );
			},
			[ 0, 1 ]
		);
	}

	public function verify( string $action, string $nonce, string $pluginActionFieldName ) :bool {
		$valid = false;
		foreach ( $this->createNonces( $action, $pluginActionFieldName ) as $expected ) {
			if ( \hash_equals( $expected, $nonce ) ) {
				$valid = true;
				break;
			}
		}
		return $valid;
	}

	public function verifyFromRequest() :bool {
		$req = Services::Request();
		return $this->verify(
			$req->request( ActionDataBuilder::FIELD_EXECUTE ),
			$req->request( ActionDataBuilder::FIELD_NONCE ),
			$req->request( ActionDataBuilder::FIELD_ACTION )
		);
	}
}
