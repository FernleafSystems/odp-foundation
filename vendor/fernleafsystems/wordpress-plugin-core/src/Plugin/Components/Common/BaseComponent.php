<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Components\Common;

use FernleafSystems\Utilities\Data\Adapter\DynProperties;
use FernleafSystems\Utilities\Logic\ExecOnce;

/**
 * @property string $use_custom_assets
 */
class BaseComponent extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	use ExecOnce;
	use DynProperties;
	use Traits\CustomAssetsConsumer;

	public function __construct( array $config = [] ) {
		$this->applyFromArray( \array_merge( [
			'use_custom_assets' => false,
		], $config ) );
	}

	protected function run() {
		if ( $this->use_custom_assets ) {
			$this->setupCustomAssets();
		}
	}
}