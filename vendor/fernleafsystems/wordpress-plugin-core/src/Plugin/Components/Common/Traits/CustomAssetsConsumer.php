<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Components\Common\Traits;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumer;

trait CustomAssetsConsumer {

	use PluginControllerConsumer;

	protected function setupCustomAssets() {
		add_filter( $this->ctr()->prefix->hook( 'custom_enqueue_assets' ), function ( array $assets, $hook ) {
			return \array_merge( $assets, $this->getCustomEnqueueAssets( $hook ) );
		}, 10, 2 );
		add_filter( $this->ctr()->prefix->hook( 'custom_localisations/components' ), function ( array $builders, string $hook, array $handles ) {
			return \array_merge( $builders, $this->getCustomLocaliseComponents( $hook, $handles ) );
		}, 10, 3 );
	}

	protected function getCustomEnqueueAssets( string $hook ) :array {
		return [];
	}

	protected function getCustomLocaliseComponents( string $hook, array $handles ) :array {
		return [];
	}
}