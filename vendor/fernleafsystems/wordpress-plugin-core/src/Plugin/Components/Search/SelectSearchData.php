<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Components\Search;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Config\Modules\Ops\{
	StringsOptions,
	StringsSections
};
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase;

class SelectSearchData extends PluginControllerConsumerBase {

	public function build( string $terms ) :array {
		$terms = \strtolower( \trim( $terms ) );
		return $this->postProcessResults( $this->textSearch( $terms ) );
	}

	private function postProcessResults( array $results ) :array {
		return \array_map(
			function ( array $result ) {
				$result[ 'children' ] = \array_map(
					function ( array $child ) {
						$child[ 'link' ] = \array_merge( [
							'target'  => ( $child[ 'is_external' ] ?? false ) ? '_blank' : false,
							'classes' => [],
							'data'    => [],
						], $child[ 'link' ] ?? [] );
						return $child;
					},
					$result[ 'children' ]
				);
				return $result;
			},
			$results
		);
	}

	/**
	 * Note use of \array_values() throughout. This is required by Select2 when it receives the data.
	 * All arrays must have simple numeric keys starting from 0.
	 */
	protected function textSearch( string $search ) :array {
		// Terms must all be at least 3 characters.
		$terms = \array_filter( \array_unique( \array_map(
			function ( $term ) {
				$term = \strtolower( \trim( $term ) );
				return \strlen( $term ) > 2 ? $term : '';
			},
			\explode( ' ', $search )
		) ) );

		$optionGroups = $this->getOptionsGroups();

		foreach ( $optionGroups as $optGroupKey => $optionGroup ) {
			foreach ( $optionGroup[ 'children' ] as $optKey => $option ) {

				$count = $this->searchString( $option[ 'tokens' ].' '.$optionGroup[ 'text' ], $terms );
				if ( $count > 0 ) {
					$optionGroups[ $optGroupKey ][ 'children' ][ $optKey ][ 'count' ] = $count;
					// Remove unnecessary 'tokens' from data sent back to select2
					unset( $optionGroups[ $optGroupKey ][ 'children' ][ $optKey ][ 'tokens' ] );

					$optionGroups[ $optGroupKey ][ 'children' ][ $optKey ] = \array_merge( [
						'is_external' => false,
						'ip'          => false,
					], $optionGroups[ $optGroupKey ][ 'children' ][ $optKey ] );
				}
				else {
					unset( $optionGroups[ $optGroupKey ][ 'children' ][ $optKey ] );
				}
			}

			// Don't include empty OptGroups
			if ( empty( $optionGroups[ $optGroupKey ][ 'children' ] ) ) {
				unset( $optionGroups[ $optGroupKey ] );
			}
			else {
				$optionGroups[ $optGroupKey ][ 'children' ] = \array_values( $optionGroups[ $optGroupKey ][ 'children' ] );
			}
		}

		return \array_values( $optionGroups );
	}

	protected function getOptionsGroups() :array {
		return \array_merge(
			$this->getExternalSearch(),
			$this->getConfigSearch()
		);
	}

	protected function searchString( string $haystack, array $needles ) :int {
		return \count( \array_intersect( $needles, \array_map( '\trim', \explode( ' ', \strtolower( $haystack ) ) ) ) );
	}

	protected function getExternalSearch() :array {
		$ctr = $this->ctr();
		$con = $this->con();
		return [
			[
				'text'     => __( 'External Links', 'wp-simple-firewall' ),
				'children' => [
					[
						'id'          => 'external_helpdesk',
						'text'        => __( 'Helpdesk and Knowledge Base', 'wp-simple-firewall' ),
						'link'        => [
							'href' => $con->labels->url_helpdesk,
						],
						'is_external' => true,
						'tokens'      => 'help docs helpdesk support knowledge base doc',
						'icon'        => $ctr->svgs->raw( 'life-preserver.svg' ),
					],
				],
			]
		];
	}

	protected function getConfigSearch() :array {
		$ctr = $this->ctr();

		/** @var StringsOptions $strings */
		$strings = $ctr->get( 'handler.strings.mod_options' );

		$opts = \array_keys( \array_filter( $this->con()->cfg->configuration->options, function ( array $optDef ) {
			return $optDef[ 'section' ] !== 'section_hidden';
		} ) );

		$config = [];
		foreach ( $opts as $optKey ) {
			try {
				$config[] = [
					'id'     => 'config_'.$optKey,
					'text'   => $strings->getFor( $optKey )[ 'name' ],
					'link'   => [
						'href' => $ctr->plugin_urls->modCfgOption( $optKey ),
					],
					'icon'   => $ctr->svgs->raw( 'sliders.svg' ),
					'tokens' => $this->getSearchableTextForOption( $optKey ),
				];
			}
			catch ( \Exception $e ) {
			}
		}

		return [
			[
				'text'     => __( 'Config', 'wp-simple-firewall' ),
				'children' => $config
			]
		];
	}

	/**
	 * @throws \Exception
	 */
	private function getSearchableTextForOption( string $optKey ) :string {
		/** @var StringsSections $strings */
		$strings = $this->ctr()->get( 'handler.strings.mod_sections' );
		$strSection = $strings->getFor( $this->ctr()->opts->optDef( $optKey )[ 'section' ] );
		/** @var StringsOptions $strings */
		$strings = $this->ctr()->get( 'handler.strings.mod_options' );
		$strOpts = $strings->getFor( $optKey );

		$allWords = \array_filter( \array_map( '\trim',
			\explode( ' ', \preg_replace( '#\(\):-#', ' ', \strip_tags( \implode( ' ', \array_merge(
				[
					$strOpts[ 'name' ],
					$strOpts[ 'summary' ],
					( \is_array( $strOpts[ 'description' ] ) ? \implode( ' ', $strOpts[ 'description' ] ) : $strOpts[ 'description' ] ),
					$strSection[ 'title' ],
					$strSection[ 'title_short' ],
				],
				$strSection[ 'summary' ]
			) ) ) ) )
		) );

		return \implode( ' ',
			\array_unique( \array_filter(
				\array_merge(
					$allWords,
					\array_map(
						function ( $word ) {
							return \preg_match( '#s$#i', $word ) ? null : $word.'s';
						},
						$allWords
					),
					\array_map(
						function ( $word ) {
							$trimmed = \rtrim( $word, 's' );
							return $trimmed === $word ? null : $trimmed;
						},
						$allWords
					)
				),
				function ( $word ) {
					return !empty( $word ) && \strlen( $word ) > 2;
				}
			) )
		);
	}
}