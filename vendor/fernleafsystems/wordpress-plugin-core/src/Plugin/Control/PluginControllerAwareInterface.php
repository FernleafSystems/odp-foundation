<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Control;

interface PluginControllerAwareInterface {

	public function con() :Controller;

	/**
	 * @return static|mixed
	 */
	public function setCon( Controller $con );
}