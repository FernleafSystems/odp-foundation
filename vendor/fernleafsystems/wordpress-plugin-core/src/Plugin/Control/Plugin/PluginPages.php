<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\Plugin;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Exceptions\PluginPageHandlerNotProvidedException;
use FernleafSystems\Wordpress\Services\Services;

/**
 * How to Add/Create a new plugin page:
 * 1. Add a new entry to the `top_navs` array in the plugin.json "pages" array.
 * 2. Add a new entry to the `sub_navs` array in the plugin.json "pages" array. Each sub_nav should have a "top_nav" key that points to its parent top_nav.
 * 3. Override the CfgPageHandlersDef and create a new entry for each top_nav & sub_nav combination. The array idx is the top_nav, and each array element should correspond to the sub_navs.
 * 4. It's possible to provide a "default" key here for any sub_navs that don't have a specific handler.
 * 5. Create the corresponding new class that extends PluginPageHandlerBase and implement the abstract methods.
 */
class PluginPages extends PluginControllerConsumerBase {

	public const FIELD_NAV = 'nav';
	public const FIELD_SUBNAV = 'nav_sub';
	public const NAV_DASHBOARD = 'dashboard';
	public const NAV_OPTIONS_CONFIG = 'config';
	public const SUBNAV_INDEX = 'index'; /* special case used only to indicate pick first in subnav list, for now */

	private $pages = null;

	public static function GetNav() :string {
		return (string)Services::Request()->query( self::FIELD_NAV );
	}

	public static function GetSubNav() :string {
		return (string)Services::Request()->query( static::FIELD_SUBNAV );
	}

	public static function IsNavs( string $nav, string $subNav ) :bool {
		return static::GetNav() === $nav && static::GetSubNav() === $subNav;
	}

	public function exists( string $nav, string $subNav = '' ) :bool {
		try {
			$page = $this->getPageNav( $nav );
			$exists = empty( $subNav ) || isset( $page[ 'sub_navs' ][ $subNav ] );
		}
		catch ( \Exception $e ) {
			$exists = false;
		}
		return $exists;
	}

	/**
	 * @throws \Exception
	 */
	public function getNavHierarchy() :array {
		return \array_map(
			function ( array $nav ) {
				if ( !isset( $nav[ 'parents' ] ) ) {
					$nav[ 'parents' ] = [];
				}
//				if ( !\in_array( self::NAV_DASHBOARD, $nav[ 'parents' ] ) ) {
//					$nav[ 'parents' ][] = self::NAV_DASHBOARD;
//				}
				return $nav;
			},
			$this->getPages()
		);
	}

	/**
	 * @throws \Exception
	 */
	public function getPages() :array {
		if ( $this->pages === null ) {
			$this->pages = [];

			$primaryFound = false;
			$maybePrimaryID = null;

			foreach ( $this->getPagesSpec() as $page ) {
				$page[ 'name' ] = __( $page[ 'name' ], 'wp-simple-firewall' );
				$page[ 'subtitle' ] = __( $page[ 'subtitle' ] ?? '', 'wp-simple-firewall' );

				// Only allow 1 home page.
				$page[ 'is_primary' ] = $page[ 'is_primary' ] ?? false;
				if ( $page[ 'is_primary' ] ) {
					if ( $primaryFound ) {
						$page[ 'is_primary' ] = false;
					}
					else {
						$primaryFound = true;
					}
				}

				$subNavsDefs = $page[ 'sub_navs' ] ?? [];

				$subNavs = [];
				foreach ( $subNavsDefs as $subNav ) {
					try {
						$subNav[ 'name' ] = isset( $subNav[ 'name' ] ) ? __( $subNav[ 'name' ], 'wp-simple-firewall' ) : $page[ 'name' ];

						$subNav[ 'admin_menu' ] = \array_merge( [
							'show' => false,
							'name' => $subNav[ 'name' ],
						], $subNav[ 'admin_menu' ] ?? [] );

						$subNav[ 'plugin_menu' ] = \array_merge( [
							'show'         => false,
							'dynamic_load' => false,
							'name'         => $subNav[ 'name' ],
						], $subNav[ 'plugin_menu' ] ?? [] );

						$subNavs[ $subNav[ 'id' ] ] = $subNav;
					}
					catch ( \Exception $e ) {
					}
				}

				if ( empty( $subNavs ) ) {
					$subNavs[ static::SUBNAV_INDEX ] = [
						'id'          => static::SUBNAV_INDEX,
						'name'        => __( $page[ 'name' ], 'wp-simple-firewall' ),
						'admin_menu'  => [
							'show' => false,
						],
						'plugin_menu' => [
							'show' => false,
						],
					];
				}

				$page[ 'sub_navs' ] = $subNavs;
				// Configuration of the top-level plugin nav menu. If `show`==false, no sub-items will be shown.
				$page[ 'plugin_menu' ] = \array_merge( [
					'show'         => false,
					'dynamic_load' => false,
					'name'         => $page[ 'name' ],
					'img'          => 'sliders',
				], $page[ 'plugin_menu' ] ?? [] );

				$this->pages[ $page[ 'id' ] ] = $page;

				if ( empty( $maybePrimaryID ) ) {
					$maybePrimaryID = $page[ 'id' ];
				}
			}

			if ( !$primaryFound && !empty( $maybePrimaryID ) ) {
				$this->pages[ $maybePrimaryID ][ 'is_primary' ] = true;
			}
		}

		return $this->pages;
	}

	protected function getPagesSpec() :array {
		$defaultPagesSpec = $this->ctr()->get( 'cfg.default_pages_spec' );
		$pluginPagesSpec = $this->con()->cfg->pages;

		$pagesSpec = [];
		foreach ( $defaultPagesSpec[ 'top_navs' ] ?? [] as $topNavSpec ) {
			$pagesSpec[ $topNavSpec[ 'id' ] ] = $topNavSpec;
		}

		// Merge in the top navs from the plugin configuration with the default (i.e. config)
		foreach ( $pluginPagesSpec[ 'top_navs' ] ?? [] as $topNavSpec ) {
			$pagesSpec[ $topNavSpec[ 'id' ] ] = Services::DataManipulation()->mergeArraysRecursive(
				$pagesSpec[ $topNavSpec[ 'id' ] ] ?? [],
				$topNavSpec
			);
		}

		// Now populate all the subnavs for each top nav
		foreach ( \array_keys( $pagesSpec ) as $navID ) {

			$subNavs = [];

			// add the default sub_navs for this top nav.
			foreach ( $defaultPagesSpec[ 'sub_navs' ] ?? [] as $defaultSubNavSpec ) {
				if ( $defaultSubNavSpec[ 'top_nav' ] === $navID ) {
					$subNavs[ $defaultSubNavSpec[ 'id' ] ] = $defaultSubNavSpec;
				}
			}

			// merge the plugin specific sub_navs.
			foreach ( $pluginPagesSpec[ 'sub_navs' ] ?? [] as $subNavSpec ) {
				if ( $subNavSpec[ 'top_nav' ] === $navID ) {
					$subNavID = $subNavSpec[ 'id' ];
					$subNavs[ $subNavID ] = Services::DataManipulation()->mergeArraysRecursive(
						$subNavs[ $subNavID ] ?? [],
						$subNavSpec
					);
				}
			}

			$pagesSpec[ $navID ][ 'sub_navs' ] = $subNavs;
		}

		/**
		 * If there are no modules with configurable options at all, then you'll have no config pages and so you'll run
		 * into a redirect loop on &nav=config&nav_sub=index which will never be able to find a suitable sub_nav.
		 *
		 * This section will also automatically make any modules with configurable options have a config page.
		 */
		if ( isset( $pagesSpec[ static::NAV_OPTIONS_CONFIG ] ) ) {

			foreach ( $this->ctr()->modules as $module ) {

				$pagesSpec[ static::NAV_OPTIONS_CONFIG ][ 'sub_navs' ][ $module->cfg->slug ] =
					Services::DataManipulation()->mergeArraysRecursive(
						[
							'id'          => $module->cfg->slug,
							'admin_menu'  => $module->cfg->properties[ 'admin_menu' ] ??
											 [
												 'show' => false,
												 'name' => 'Config: '.$module->cfg->properties[ 'name' ],
											 ],
							'plugin_menu' => $module->cfg->properties[ 'plugin_menu' ] ??
											 [
												 'show'         => true,
												 'dynamic_load' => true,
												 'name'         => $module->cfg->properties[ 'name' ],
											 ],
						],
						$pagesSpec[ static::NAV_OPTIONS_CONFIG ][ 'sub_navs' ][ $module->cfg->slug ] ?? []
					);

				// If there are no no-hidden options, then don't provide a config page.
				$hasNonHidden = \count( \array_filter( $module->cfg->options, function ( array $option ) {
						return $option[ 'section' ] !== 'section_hidden';
					} ) ) > 0;
				if ( !$hasNonHidden ) {
					$pagesSpec[ static::NAV_OPTIONS_CONFIG ][ 'sub_navs' ][ $module->cfg->slug ][ 'plugin_menu' ][ 'show' ] = false;
				}
			}
		}

		return $pagesSpec;
	}

	/**
	 * @throws PluginPageHandlerNotProvidedException|\Exception
	 */
	public function getPageHandlerFor( string $nav, string $subNav = '' ) :string {
		if ( !$this->exists( $nav ) ) {
			throw new \Exception( 'Invalid nav: '.$nav );
		}
		if ( empty( $subNav ) ) {
			$subNav = $this->getDefaultSubNavForNav( $nav );
		}

		/** @var array[] $pageHandlers */
		$pageHandlers = $this->ctr()->get( 'cfg.page_handlers' );
		if ( isset( $pageHandlers[ $nav ] ) ) {
			$handler = $pageHandlers[ $nav ][ $subNav ] ?? $pageHandlers[ $nav ][ 'default' ];
		}
		if ( empty( $handler ) ) {
			throw new PluginPageHandlerNotProvidedException( sprintf( 'no handler for %s-%s', $nav, $subNav ) );
		}
		return $handler;
	}

	/**
	 * @throws \Exception
	 */
	public function getPrimary() :array {
		$nav = '';
		$subNav = '';
		foreach ( $this->getPages() as $nav => $page ) {
			if ( $page[ 'is_primary' ] ) {
				$nav = $page[ 'id' ];
				$subNav = $this->getDefaultSubNavForNav( $nav );
				break;
			}
		}
		return [ $nav, $subNav ];
	}

	public function getDefaultSubNavForNav( string $nav ) :?string {
		try {
			$subNav = \key( $this->getPageNav( $nav )[ 'sub_navs' ] );
		}
		catch ( \Exception $e ) {
			$subNav = null;
		}
		return $subNav;
	}

	/**
	 * @throws \Exception
	 */
	protected function getPageNav( string $nav ) :array {
		if ( !isset( $this->getPages()[ $nav ] ) ) {
			throw new \Exception( 'Invalid nav: '.$nav );
		}
		return $this->getPages()[ $nav ];
	}

	public function isPageDynamic( string $nav, string $subNav = '' ) :bool {
		try {
			if ( empty( $subNav ) ) {
				$subNav = $this->getDefaultSubNavForNav( $nav );
			}
			$dynamic = $this->exists( $nav, $subNav )
					   && !empty( $this->getPageNav( $nav )[ 'sub_navs' ][ $subNav ][ 'plugin_menu' ][ 'dynamic_load' ] );
		}
		catch ( \Exception $e ) {
			$dynamic = false;
		}
		return $dynamic;
	}
}