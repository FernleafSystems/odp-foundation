<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\Plugin;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Modules\Base\ModCon;
use FernleafSystems\Wordpress\Services\Services;
use FernleafSystems\Wordpress\Services\Utilities\URL;

class PluginURLs extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	public function rootAdminPage() :string {
		return Services::WpGeneral()->getUrl_AdminPage(
			$this->rootAdminPageSlug(), (bool)$this->con()->cfg->properties[ 'wpms_network_admin_only' ] );
	}

	public function rootAdminPageSlug() :string {
		return $this->ctr()->prefix->pfx( 'plugin' );
	}

	public function adminHome() :string {
		[ $nav, $sub ] = $this->ctr()->plugin_pages->getPrimary();
		return $this->adminTopNav( $nav, $sub );
	}

	public function adminTopNav( string $nav, string $subNav = '' ) :string {
		if ( empty( $subNav ) ) {
			$subNav = $this->ctr()->plugin_pages->getDefaultSubNavForNav( $nav );
		}
		return URL::Build( $this->rootAdminPage(), [
			PluginPages::FIELD_NAV    => sanitize_key( $nav ),
			PluginPages::FIELD_SUBNAV => sanitize_key( $subNav ),
		] );
	}

	/**
	 * @param ModCon|string|mixed $mod
	 */
	public function modCfg( $mod ) :string {
		return $this->adminTopNav( $this->ctr()->plugin_pages::NAV_OPTIONS_CONFIG, \is_string( $mod ) ? $mod : $mod->cfg->slug );
	}

	public function modCfgOption( string $optKey ) :string {
		return $this->modCfgSection(
			$this->con()->cfg->configuration->modFromOpt( $optKey ),
			$this->ctr()->opts->optDef( $optKey )[ 'section' ]
		);
	}

	/**
	 * @param ModCon|string|mixed $mod
	 */
	public function modCfgSection( $mod, string $optSection ) :string {
		return $this->modCfg( $mod ).'#tab-'.$optSection;
	}

	/**
	 * Builds a URL with a nonce + any other auxiliary data for executing a Shield Plugin action
	 */
	public function noncedPluginAction( string $action, ?string $url = null, array $aux = [] ) :string {
		return URL::Build(
			empty( $url ) ? Services::WpGeneral()->getHomeUrl() : $url,
			$this->ctr()->actions->dataBuilder()->build( $action, false, $aux )
		);
	}

	public function fileDownload( string $downloadCategory, array $params = [] ) :string {
		return $this->noncedPluginAction(
			FileDownload::class,
			Services::WpGeneral()->getAdminUrl(),
			\array_merge( $params, [ 'download_category' => $downloadCategory ] )
		);
	}

	public function fileDownloadAsStream( string $downloadCategory, array $params = [] ) :string {
		return $this->noncedPluginAction(
			FileDownloadAsStream::class,
			Services::WpGeneral()->getAdminUrl(),
			\array_merge( $params, [ 'download_category' => $downloadCategory ] )
		);
	}
}