<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\Plugin;

class PluginReset extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	public function run() {
		$this->con()->plugin_reset = true;
		$this->ctr()->opts->resetToDefaults();
		$this->con()->deletePlugin();
	}
}