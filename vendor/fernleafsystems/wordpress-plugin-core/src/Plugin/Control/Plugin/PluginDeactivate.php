<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\Plugin;

use FernleafSystems\Wordpress\Services\Services;

class PluginDeactivate extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	public function run() {
		$con = $this->con();

		if ( $con->isPluginAdmin() ) {

			$con->plugin_deactivating = true;

			do_action( $con->cntnr->prefix->hook( 'pre_deactivate_plugin' ) );

			if ( apply_filters( $con->cntnr->prefix->hook( 'delete_on_deactivate' ), false ) ) {
				$con->deletePlugin();
			}

			$this->deleteCrons();

			do_action( $this->ctr()->prefix->hook( 'post_deactivate_plugin' ) );
		}
	}

	protected function deleteCrons() :void {
		$cfg = $this->con()->cfg;
		$pattern = sprintf( '#^(%s|%s)#', $cfg->properties[ 'slug_parent' ], $cfg->properties[ 'slug_plugin' ] );
		foreach ( Services::WpCron()->getCrons() as $cron ) {
			foreach ( $cron as $key => $cronEntry ) {
				if ( \is_string( $key ) && \preg_match( $pattern, $key ) ) {
					Services::WpCron()->deleteCronJob( $key );
				}
			}
		}
	}
}