<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\Plugin;

use FernleafSystems\Wordpress\Plugin\Core\Databases\Base\Handler;
use FernleafSystems\Wordpress\Services\Services;

class PluginDelete extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	public function run() {
		$this->preDelete();
		$this->delete();
		$this->postDelete();
	}

	protected function preDelete() {
		$this->con()->plugin_deleting = true;
		do_action( $this->ctr()->prefix->hook( 'pre_delete_plugin' ) );
	}

	protected function delete() {
		$this->deleteDatabases();
		$this->deleteTmpDir();
		$this->deleteOptions();
	}

	protected function postDelete() {
		do_action( $this->ctr()->prefix->hook( 'post_delete_plugin' ) );
	}

	protected function deleteOptions() {
		$this->ctr()->opts->delete();
	}

	protected function deleteTmpDir() {
		$path = $this->ctr()->cache_dir_handler->dir();
		if ( !empty( $path ) ) {
			Services::WpFs()->deleteDir( $path );
		}
	}

	protected function deleteDatabases() {
		$dbCon = $this->ctr()->db_con;

		$builtInTablesToDelete = \array_unique( \array_map(
			function ( $dbh ) {
				/** @var $dbh Handler */
				$dbh::GetTableReadyCache()->setReady( $dbh->getTableSchema(), false );
				return $dbh->getTableSchema()->table;
			},
			\array_filter( \array_map(
				function ( array $handlerDefs ) {
					return $handlerDefs[ 'handler' ] ?? null;
				},
				$dbCon->loadAll()
			) )
		) );

		/**
		 * Always signal to the DB Service that any data we may have retained about DB table readiness is purged.
		 */
		if ( !empty( $builtInTablesToDelete ) ) {
			Services::WpDb()->doDropTable( \implode( '`,`', $builtInTablesToDelete ) );
		}
		$dbCon->reset();
	}
}