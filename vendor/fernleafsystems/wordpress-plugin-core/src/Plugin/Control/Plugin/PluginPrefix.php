<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\Plugin;

class PluginPrefix extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	public function _( string $item = '' ) :string {
		return $this->pfx( $item, '_' );
	}

	/**
	 * @alias $this->slash()
	 */
	public function hook( string $item = '' ) :string {
		return $this->slash( $item );
	}

	public function slash( string $item = '' ) :string {
		return $this->pfx( $item, '/' );
	}

	public function pfx( string $item = '', string $glue = '-' ) :string {
		$prefix = $this->getPrefix( $glue );

		if ( $item == $prefix || \strpos( $item, $prefix.$glue ) === 0 ) { //it already has the full prefix
			return $item;
		}

		return sprintf( '%s%s%s', $prefix, empty( $item ) ? '' : $glue, empty( $item ) ? '' : $item );
	}

	public function getPrefix( string $glue = '-' ) :string {
		return sprintf( '%s%s%s', $this->con()->cfg->properties[ 'slug_parent' ], $glue, $this->con()->cfg->properties[ 'slug_plugin' ] );
	}
}