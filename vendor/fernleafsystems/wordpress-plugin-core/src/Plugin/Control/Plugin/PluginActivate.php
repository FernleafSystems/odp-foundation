<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\Plugin;

use FernleafSystems\Wordpress\Services\Services;

class PluginActivate extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	public function run() {
		$this->ctr()->opts->optSet( 'activated_at', Services::Request()->ts() );
		$this->con()->is_activating = true;
		do_action( $this->ctr()->prefix->hook( 'plugin_activated' ) );
	}
}