<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Control;

use FernleafSystems\Utilities\Data\Adapter\DynPropertiesClass;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\{
	Checks,
	Updates
};
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Actions\{
	Actions,
	Exceptions\ActionException
};
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Config\{
	ConfigVO,
	LabelsVO,
	Modules,
	Ops\LoadConfig
};
use FernleafSystems\Wordpress\Plugin\Core\Plugin\I18n\LoadTextDomain;
use FernleafSystems\Wordpress\Plugin\Core\Plugin\Services\{
	ServiceProvider,
	ServiceProviderDef
};
use FernleafSystems\Wordpress\Services\Services;
use FernleafSystems\Wordpress\Services\Utilities\Options\Transient;
use League\Container\Argument\Literal\ObjectArgument;

/**
 * @property string                  $base_file
 * @property string                  $root_file
 * @property Container\AptoContainer $cntnr
 * @property ConfigVO                $cfg
 * @property LabelsVO                $labels
 * @property array                   $prechecks
 * @property array                   $flags
 * @property bool                    $is_activating
 * @property bool                    $is_mode_debug
 * @property bool                    $is_mode_staging
 * @property bool                    $is_mode_live
 * @property bool                    $is_my_upgrade
 * @property bool                    $is_rest_enabled
 * @property bool                    $modules_loaded
 * @property bool                    $plugin_deactivating
 * @property bool                    $plugin_deleting
 * @property bool                    $plugin_reset
 * @property bool                    $user_can_base_permissions
 * @property string[]                $reqs_not_met
 */
class Controller extends DynPropertiesClass {

	/**
	 * @var Controller
	 */
	public static $Instance;

	/**
	 * @throws \Exception
	 */
	public static function GetInstance( ?string $rootFile = null ) :Controller {
		if ( !isset( static::$Instance ) ) {
			if ( empty( $rootFile ) ) {
				throw new \Exception( 'Empty root file provided for instantiation' );
			}
			static::$Instance = new static( $rootFile );
		}
		return static::$Instance;
	}

	/**
	 * @throws \Exception
	 */
	protected function __construct( string $rootFile ) {
		$this->root_file = $rootFile;
		$this->base_file = plugin_basename( $this->getRootFile() );
	}

	/**
	 * @throws \Exception
	 */
	public function __get( string $key ) {
		$val = parent::__get( $key );

		switch ( $key ) {

			case 'labels':
				$val = $this->cntnr->plugin_labels->getLabels();
				break;

			case 'flags':
				if ( !\is_array( $val ) ) {
					$this->flags = $val = [];
				}
				break;

			case 'is_rest_enabled':
				if ( $val === null ) {
					$restReqs = $this->cfg->reqs_rest;
					$val = Services::WpGeneral()->getWordpressIsAtLeastVersion( $restReqs[ 'wp' ] )
						   && Services::Data()->getPhpVersionIsAtLeast( $restReqs[ 'php' ] );
					$this->is_rest_enabled = $val;
				}
				break;

			case 'is_activating':
			case 'is_my_upgrade':
			case 'modules_loaded':
			case 'plugin_deactivating':
			case 'plugin_deleting':
			case 'user_can_base_permissions':
				$val = (bool)$val;
				break;

			case 'plugin_reset':
				if ( $val === null ) {
					$this->plugin_reset = $val = Services::WpFs()
														 ->isAccessibleFile( $this->cntnr->paths->forFlag( 'reset' ) );
				}
				break;

			case 'cache_dir_handler':
				if ( empty( $val ) ) {
					throw new \Exception( 'Accessing Cache Dir Handler too early.' );
				}
				break;

			default:
				break;
		}

		return $val;
	}

	/**
	 * This is where everything happens that runs the plugin.
	 * 1) Modules are all loaded.
	 * 2) Upgrades are run.
	 * 3) Rules Engine is initiated
	 * 4) If Rules Engine is ready, they're executed and then processors are kicked off.
	 * @throws \Exception
	 */
	public function boot() {
		$this->preBoot();
		$this->loadConfig();
		$this->initContainer();
		$this->saveCurrentPluginControllerCfg();
		$this->checkMinimumRequirements();

		/** @var LoadTextDomain $textDomainLoader */
		$textDomainLoader = $this->cntnr->get( 'handler.text_domain' );
		$textDomainLoader->run();

		// Register the Controller hooks
		$this->doRegisterHooks();

		$this->loadModules();

		$this->cntnr->plugin_labels->execute();

		if ( $this->plugin_reset ) {
			$this->resetPlugin();
		}

		/** @var Checks\PreModulesBootCheck $preChecker */
		$preChecker = $this->cntnr->get( 'handler.pre_modules_boot_check' );
		$this->prechecks = $preChecker->run();

		$this->cntnr->db_con->execute();

		/** @var Updates\HandleUpgrades $upgrader */
		$upgrader = $this->cntnr->get( 'handler.upgrades' );
		$upgrader->execute();
	}

	protected function preBoot() :void {
	}

	/**
	 * Note that we don't/can't use $this->cntnr->paths-> to get the path to the plugin.json file as we haven't
	 * initiated the ServiceProvider Container, yet.  We prefer to init the container after the cfg is loaded so we can
	 * use it where necessary to build services/configurations.
	 * @throws \Exception
	 */
	protected function loadConfig() {
		$this->cfg = ( new LoadConfig( path_join( $this->getRootDir(), 'plugin.json' ), $this->getConfigStoreKey() ) )
			->setCon( $this )
			->run();
	}

	/**
	 * This should follow loadConfig()
	 */
	protected function initContainer() :void {
		$this->cntnr = new Container\AptoContainer();
		$this->cntnr->setCon( $this );
		$this->cntnr->add( 'con', new ObjectArgument( $this ) );
		$this->cntnr->inflector( PluginControllerAwareInterface::class )
					->invokeMethod( 'setCon', [ 'con' ] );
		$this->cntnr->addServiceProvider(
			( new ServiceProvider( $this->getServiceProviderDef() ) )->setCon( $this )
		);
	}

	protected function getServiceProviderDef() :ServiceProviderDef {
		return new ServiceProviderDef();
	}

	/**
	 * @throws \Exception
	 */
	protected function checkMinimumRequirements() {
		$FS = Services::WpFs();

		$flag = $this->cntnr->paths->forFlag( 'reqs_met.flag' );
		if ( !$FS->isAccessibleFile( $flag )
			 || Services::Request()->carbon()->subHour()->timestamp > $FS->getModifiedTime( $flag ) ) {
			$reqsMsg = [];

			$minPHP = $this->cfg->requirements[ 'php' ];
			if ( !empty( $minPHP ) && \version_compare( Services::Data()->getPhpVersion(), $minPHP, '<' ) ) {
				$reqsMsg[] = sprintf( 'PHP does not meet minimum version. Your version: %s.  Required Version: %s.', \PHP_VERSION, $minPHP );
			}

			$wp = $this->cfg->requirements[ 'wordpress' ];
			if ( !empty( $wp ) && \version_compare( Services::WpGeneral()->getVersion( true ), $wp, '<' ) ) {
				$reqsMsg[] = sprintf( 'WordPress does not meet minimum version. Required Version: %s.', $wp );
			}

			$mysql = $this->cfg->requirements[ 'mysql' ];
			if ( !empty( $mysql ) && !( new Checks\Requirements() )->isMysqlVersionSupported( $mysql ) ) {
				$reqsMsg[] = sprintf( "Your MySQL database server doesn't support IPv6 addresses. Your Version: %s; Required MySQL Version: %s;",
					Services::WpDb()->getMysqlServerInfo(), $mysql );
			}

			if ( !empty( $reqsMsg ) ) {
				$this->reqs_not_met = $reqsMsg;
				add_action( 'admin_notices', [ $this, 'adminNoticeDoesNotMeetRequirements' ] );
				add_action( 'network_admin_notices', [ $this, 'adminNoticeDoesNotMeetRequirements' ] );
				throw new \Exception( 'Plugin does not meet minimum requirements' );
			}

			$FS->touch( $this->cntnr->paths->forFlag( 'reqs_met.flag' ) );
		}
	}

	public function adminNoticeDoesNotMeetRequirements() {
		if ( !empty( $this->reqs_not_met ) ) {
			$this->cntnr
				->render
				->getRenderer()
				->setTemplate( '/notices/does-not-meet-requirements.twig' )
				->setTemplateEngineTwig()
				->setRenderVars( [
					'strings' => [
						'not_met'          => 'Shield Security Plugin - minimum site requirements are not met',
						'requirements'     => $this->reqs_not_met,
						'summary_title'    => "Your web hosting doesn't meet the minimum requirements for the Shield Security Plugin.",
						'recommend'        => "We highly recommend upgrading your web hosting components as they're probably quite out-of-date.",
						'more_information' => 'Click here for more information on requirements'
					],
					'hrefs'   => [
						'more_information' => 'https://shsec.io/shieldsystemrequirements'
					]
				] )
				->display();
		}
	}

	/**
	 * @throws \Exception
	 */
	protected function loadModules() :void {
		if ( !$this->modules_loaded ) {

			$this->modules_loaded = true;

			$configuration = $this->cfg->configuration;
			if ( empty( $configuration ) || $this->cfg->rebuilt ) {
				/** @var Modules\LoadModuleConfigs $loader */
				$loader = $this->cntnr->get( 'handler.modules_configs_loader' );
				$this->cfg->configuration = $loader->run();
			}

			do_action( $this->cntnr->prefix->hook( 'modules_configuration' ) ); // Extensions jump in here to augment options/sections

			/** @var Modules\ModulesBuildDefs $modBuilder */
			$modBuilder = $this->cntnr->get( 'handler.modules.build_defs' );
			foreach ( \array_keys( $this->cfg->configuration->modules ) as $slug ) {
				$modBuilder->buildDef( $slug );
			}

			// Boot all modules
			\array_map(
				function ( $mod ) {
					$mod->boot();
				},
				$this->cntnr->modules
			);
		}
	}

	public function onWpDeactivatePlugin() {
		$this->cntnr->plugin_deactivate->run();
	}

	public function resetPlugin() {
		$this->cntnr->plugin_reset->run();
	}

	public function deletePlugin() {
		$this->cntnr->plugin_delete->run();
	}

	/**
	 * @throws \TypeError - Potentially. Not sure how the plugin hasn't initiated by that stage.
	 */
	public function onWpActivatePlugin() {
		$this->cntnr->plugin_activate->run();
	}

	protected function doRegisterHooks() {
		register_deactivation_hook( $this->getRootFile(), [ $this, 'onWpDeactivatePlugin' ] );

		add_action( 'init', [ $this, 'onWpInit' ], 0 );
		add_action( 'wp_loaded', [ $this, 'onWpLoaded' ], 5 );
		add_action( 'admin_init', [ $this, 'onWpAdminInit' ] );
		add_action( 'shutdown', [ $this, 'onWpShutdown' ], \PHP_INT_MIN );

		/**
		 * Support for WP-CLI and it marks the cli as plugin admin
		 */
		add_filter( $this->cntnr->prefix->hook( 'bypass_is_plugin_admin' ), function ( $byPass ) {
			if ( Services::WpGeneral()->isWpCli() ) {
				$byPass = true;
			}
			return $byPass;
		}, \PHP_INT_MAX );
	}

	public function onWpAdminInit() {
	}

	public function onWpInit() {
		$this->getMeetsBasePermissions();

		try {
			$this->cntnr->actions->execute();
			$this->cntnr->actions->action( Actions\PluginAdmin\PluginAdminPageHandler::class, \array_merge(
				Services::Request()->query,
				Services::Request()->post
			) );
		}
		catch ( ActionException $e ) {
		}

		$this->cntnr->enqueue->execute();
	}

	public function onWpLoaded() {
		$this->cntnr->admin_notices->execute();
		$this->cntnr->crons->execute();
	}

	public function onWpShutdown() {
		do_action( $this->cntnr->prefix->pfx( 'pre_plugin_shutdown' ) );
		$this->cntnr->opts->store();
		do_action( $this->cntnr->prefix->pfx( 'plugin_shutdown' ) );
		$this->saveCurrentPluginControllerCfg();
		$this->deleteFlags();
	}

	protected function deleteFlags() {
		$FS = Services::WpFs();
		foreach ( [ 'rebuild', 'reset' ] as $flag ) {
			if ( $FS->exists( $this->cntnr->paths->forFlag( $flag ) ) ) {
				$FS->deleteFile( $this->cntnr->paths->forFlag( $flag ) );
			}
		}
	}

	/**
	 * All our module page names are prefixed
	 * @see PluginAdminPageHandler - All Plugin admin pages go through the plugin modules, see:
	 */
	public function isPluginAdminPageRequest() :bool {
		return Services::Request()->query( 'page' ) === $this->cntnr->plugin_urls->rootAdminPageSlug();
	}

	public function isPremiumActive() :bool {
		if ( !empty( $this->cfg->properties[ 'enable_premium' ] ) ) {
			die( 'Enable Premium is set to be on, but there is no premium handling.' );
		}
		return true;
	}

	public function isValidAdminArea( bool $checkUserPerms = false ) :bool {
		if ( $checkUserPerms && did_action( 'init' ) && !$this->isPluginAdmin() ) {
			return false;
		}

		$WP = Services::WpGeneral();
		if ( !$WP->isMultisite() && is_admin() ) {
			return true;
		}
		elseif ( $WP->isMultisite() && $this->cfg->properties[ 'wpms_network_admin_only' ] && ( is_network_admin() || $WP->isAjax() ) ) {
			return true;
		}
		return false;
	}

	/**
	 * only ever consider after WP INIT (when a logged-in user is recognised)
	 */
	public function isPluginAdmin() :bool {
		return apply_filters( $this->cntnr->prefix->pfx( 'bypass_is_plugin_admin' ), false )
			   || apply_filters( $this->cntnr->prefix->pfx( 'is_plugin_admin' ), $this->getMeetsBasePermissions() );
	}

	/**
	 * DO NOT CHANGE THIS IMPLEMENTATION.
	 * We call this as early as possible so that the
	 * current_user_can() never gets caught up in an infinite loop of permissions checking
	 */
	public function getMeetsBasePermissions() :bool {
		if ( did_action( 'init' ) && !isset( $this->user_can_base_permissions ) ) {
			$this->user_can_base_permissions = current_user_can( $this->cfg->properties[ 'base_permissions' ] );
		}
		return $this->user_can_base_permissions;
	}

	public function getPluginPrefix( string $glue = '-' ) :string {
		return sprintf( '%s%s%s', $this->cfg->properties[ 'slug_parent' ], $glue, $this->cfg->properties[ 'slug_plugin' ] );
	}

	public function getIsPage_PluginAdmin() :bool {
		return \strpos( Services::WpGeneral()->getCurrentWpAdminPage(), $this->getPluginPrefix() ) === 0;
	}

	public function getRootDir() :string {
		return trailingslashit( \dirname( $this->getRootFile() ) );
	}

	public function getRootFile() :string {
		if ( empty( $this->root_file ) ) {
			$VO = ( new \FernleafSystems\Wordpress\Services\Utilities\WpOrg\Plugin\Files() )->findPluginFromFile( __FILE__ );
			if ( $VO instanceof \FernleafSystems\Wordpress\Services\Core\VOs\Assets\WpPluginVo ) {
				$this->root_file = path_join( WP_PLUGIN_DIR, $VO->file );
			}
			else {
				$this->root_file = __FILE__;
			}
		}
		return $this->root_file;
	}

	public function getTextDomain() :string {
		return $this->cfg->properties[ 'text_domain' ];
	}

	protected function saveCurrentPluginControllerCfg() {
		add_filter( $this->cntnr->prefix->hook( 'bypass_is_plugin_admin' ), '__return_true' );

		if ( $this->plugin_deleting ) {
			Services::WpGeneral()->deleteOption( $this->getConfigStoreKey() );
			Transient::Delete( $this->getConfigStoreKey() );
		}
		elseif ( isset( $this->cfg ) ) {
			Services::WpGeneral()->updateOption( $this->getConfigStoreKey(), $this->cfg->getRawData() );
		}
		remove_filter( $this->cntnr->prefix->hook( 'bypass_is_plugin_admin' ), '__return_true' );
	}

	private function getConfigStoreKey() :string {
		return 'aptoweb_controller_'.\substr( \md5( \get_class( $this ) ), 0, 6 );
	}

	public function setFlag( string $flag, $value ) {
		$flags = $this->flags;
		$flags[ $flag ] = $value;
		$this->flags = $flags;
	}
}