<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Control;

use FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\Container\AptoContainer;

trait PluginControllerConsumer {

	private $con;

	public function con() :Controller {
		return $this->con;
	}

	/**
	 * @return static|mixed
	 */
	public function setCon( Controller $con ) :self {
		$this->con = $con;
		return $this;
	}

	protected function ctr() :AptoContainer {
		return $this->con()->cntnr;
	}
}