<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\Checks;

/**
 * All Plugin Modules have been created at this stage.
 * We now run some pre-checks to ensure we're ok to do full modules boot.
 */
class PreModulesBootCheck extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	public function run() :array {
		$dbCon = $this->ctr()->db_con;

		$checks = [
			'dbs' => [
			]
		];

		foreach ( $dbCon->getHandlers() as $dbKey => $handlerDef ) {
			try {
				if ( $handlerDef[ 'def' ][ 'is_core' ] ) {
					$dbh = $dbCon->loadDbH( $dbKey );
					$checks[ 'dbs' ][ $dbh->getTableSchema()->slug ] = $dbh->isReady();
				}
			}
			catch ( \Exception $e ) {
			}
		}

		return $checks;
	}
}
