<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Core\Plugin\I18n;

class GetAllAvailableLocales extends \FernleafSystems\Wordpress\Plugin\Core\Plugin\Control\PluginControllerConsumerBase {

	public function run() :array {
		$locales = [ 'en_US' ];
		foreach ( new \DirectoryIterator( $this->ctr()->paths->forLanguages() ) as $file ) {
			if ( $file->isFile() && $file->getExtension() === 'mo' ) {
				$locales[] = \str_replace( $this->con()->getTextDomain().'-', '', $file->getBasename( '.mo' ) );
			}
		}
		\asort( $locales );
		return \array_unique( $locales );
	}
}