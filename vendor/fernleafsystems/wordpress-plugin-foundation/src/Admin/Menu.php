<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Admin;

use FernleafSystems\Wordpress\Plugin\Foundation;

class Menu {

	use Foundation\Configuration\PluginController\ConfigConsumer;

	/**
	 * @var Foundation\Utility\Prefix
	 */
	private $oPrefix;

	/**
	 * @var Foundation\Display\Labels
	 */
	private $oLabels;

	/**
	 * @var Foundation\Paths\Derived
	 */
	private $oPaths;

	/**
	 * @param Foundation\Display\Labels $oLabels
	 * @param Foundation\Paths\Derived  $oPaths
	 * @param Foundation\Utility\Prefix $oPrefix
	 */
	public function __construct( $oLabels, $oPaths, $oPrefix ) {
		$this->oPrefix = $oPrefix;
		$this->oPaths = $oPaths;
		$this->oLabels = $oLabels;
		add_action( 'admin_menu', [ $this, 'onWpAdminMenu' ] );
		add_action( 'network_admin_menu', [ $this, 'onWpAdminMenu' ] );
	}

	public function onWpAdminMenu() {
		$this->createPluginMenu();
	}

	/**
	 * @return bool
	 */
	protected function createPluginMenu() {
		$oConfig = $this->getConfig();

		$bHideMenu = apply_filters( $this->oPrefix->prefix( 'filter_hidePluginMenu' ), !$oConfig->getMenuSpec( 'show' ) );
		if ( $bHideMenu ) {
			return true;
		}

		if ( $oConfig->getMenuSpec( 'top_level' ) ) {

			$sIconUrl = $this->oLabels->getIconUrl16();
			$sIconUrl = empty( $sIconUrl ) ? $oConfig->getMenuSpec( 'icon_image' ) : $sIconUrl;

			$sMenuTitle = $oConfig->getMenuSpec( 'title' );
			if ( is_null( $sMenuTitle ) ) {
				$sMenuTitle = $this->oLabels->getName(); // TODO: Human Name?
			}

			// Because only plugin-relative paths, or absolute URLs are accepted
			if ( !preg_match( '#^(http(s)?:)?//#', $sIconUrl ) ) {
				$sIconUrl = $this->oPaths->getPluginUrl_Image( $sIconUrl );
			}

			$sFullParentMenuId = $this->oPrefix->getPluginPrefix();
			add_menu_page(
				$this->oLabels->getHumanName(),
				$sMenuTitle,
				$this->getConfig()->getBasePermissions(),
				$sFullParentMenuId,
				[ $this, $oConfig->getMenuSpec( 'callback' ) ],
				$sIconUrl
			);

			if ( $oConfig->getMenuSpec( 'has_submenu' ) ) {

				$aPluginMenuItems = apply_filters( $this->oPrefix->prefix( 'filter_plugin_submenu_items' ), [] );
				if ( !empty( $aPluginMenuItems ) ) {
					foreach ( $aPluginMenuItems as $sMenuTitle => $aMenu ) {
						list( $sMenuItemText, $sMenuItemId, $aMenuCallBack ) = $aMenu;
						add_submenu_page(
							$sFullParentMenuId,
							$sMenuTitle,
							$sMenuItemText,
							$this->getConfig()->getBasePermissions(),
							$sMenuItemId,
							$aMenuCallBack
						);
					}
				}
			}

			if ( $oConfig->getMenuSpec( 'do_submenu_fix' ) ) {
				$this->fixSubmenu();
			}
		}
		return true;
	}

	/**
	 * TODO: Remove this.
	 */
	public function onDisplayTopMenu() {
	}

	protected function fixSubmenu() {
		global $submenu;
		$sFullParentMenuId = $this->oPrefix->getPluginPrefix();
		if ( isset( $submenu[ $sFullParentMenuId ] ) ) {
			unset( $submenu[ $sFullParentMenuId ][ 0 ] );
		}
	}
}
