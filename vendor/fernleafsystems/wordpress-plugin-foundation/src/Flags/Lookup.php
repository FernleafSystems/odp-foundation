<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Flags;

use FernleafSystems\Wordpress\Plugin\Foundation\Paths\Derived as DerivedPaths;
use FernleafSystems\Wordpress\Services\Services;

class Lookup {

	/**
	 * @var array
	 */
	protected $aFlagFiles;

	/**
	 * @param DerivedPaths $oDerivedPaths
	 */
	public function __construct( $oDerivedPaths ) {
		$this->loadFlagsInDir( $oDerivedPaths->getPath_Flags() );
		$this->deleteTransientFlags( $oDerivedPaths );
	}

	public function getHasFlags() :bool {
		return !empty( $this->aFlagFiles );
	}

	public function getHasFlagForceOff() :bool {
		return $this->getHasFlag( 'forceoff' );
	}

	public function getHasFlagReset() :bool {
		return $this->getHasFlag( 'reset' );
	}

	/**
	 * Flags are Case-insensitive
	 * @param string $sFlag
	 */
	public function getHasFlag( $sFlag ) :bool {
		return \in_array( \strtolower( $sFlag ), $this->aFlagFiles );
	}

	/**
	 * @param string $sDir
	 */
	protected function loadFlagsInDir( $sDir ) {
		$aFlags = Services::WpFs()->getAllFilesInDir( $sDir );
		if ( is_array( $aFlags ) ) {
			foreach ( $aFlags as $nKey => $sFileName ) {
				if ( preg_match( '#[^a-z0-9]#i', $sFileName ) ) {
					unset( $aFlags[ $nKey ] );
				}
			}
			$this->aFlagFiles = array_map( 'strtolower', $aFlags );
		}
		else {
			$this->aFlagFiles = [];
		}
	}

	/**
	 * @param $oDerivedPaths DerivedPaths
	 */
	protected function deleteTransientFlags( $oDerivedPaths ) {
		$aToDelete = array_intersect( $this->getTransientFlags(), $this->aFlagFiles );
		if ( !empty( $aToDelete ) ) {
			$oFS = Services::WpFs();
			foreach ( $aToDelete as $sFlagName ) {
				$oFS->deleteFile( $oDerivedPaths->getPath_Flags( $sFlagName ) );
			}
		}
	}

	/**
	 * @return array
	 */
	protected function getTransientFlags() {
		return [ 'rebuild', 'reset' ];
	}
}