<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Database\Vo;

use FernleafSystems\Wordpress\Services\Services;

/**
 * @deprecated
 */
class DbVo {

	/**
	 * @var array
	 */
	protected $aTableColumnsByDefinition;

	/**
	 * @var string
	 */
	protected $sFullTableName;

	/**
	 * @param string $sTableName
	 * @param array  $aTableColumnsDefinition
	 */
	public function __construct( $sTableName, $aTableColumnsDefinition ) {
		$this->setTableName( $sTableName );
		$this->setTableColumnsByDefinition( $aTableColumnsDefinition );
	}

	/**
	 * @return string
	 */
	public function getTableName() {
		return $this->sFullTableName;
	}

	/**
	 * @return array
	 */
	public function getTableColumnsByDefinition() {
		return ( isset( $this->aTableColumnsByDefinition ) && is_array( $this->aTableColumnsByDefinition ) ) ? $this->aTableColumnsByDefinition : [];
	}

	/**
	 * @param string $sTableName
	 * @return $this
	 * @throws \Exception
	 */
	private function setTableName( $sTableName = null ) {
		if ( empty( $sTableName ) || !is_string( $sTableName ) ) {
			throw new \Exception( 'Database Table Name is EMPTY' );
		}
		$this->sFullTableName = str_replace( '-', '_', Services::WpDb()->getPrefix().esc_sql( $sTableName ) );
		return $this;
	}

	/**
	 * @param array $aDefinition
	 * @return $this
	 */
	public function setTableColumnsByDefinition( $aDefinition ) {
		$this->aTableColumnsByDefinition = $aDefinition;
		return $this;
	}
}