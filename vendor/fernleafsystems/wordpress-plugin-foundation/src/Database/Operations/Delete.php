<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Database\Operations;

use FernleafSystems\Wordpress\Services\Services;

class Delete extends Base {

	/**
	 * @return bool|int
	 */
	public function drop() {
		return Services::WpDb()->doDropTable( $this->getDbVo()->getTableName() );
	}

	/**
	 * @param array $aWhere
	 * @return false|int
	 */
	public function deleteWhere( $aWhere ) {
		return Services::WpDb()->deleteRowsFromTableWhere( $this->getDbVo()->getTableName(), $aWhere );
	}

	/**
	 * @param int    $nTimeStamp
	 * @param string $sColumnToCompare
	 * @return bool|int
	 * @throws \Exception
	 */
	public function deleteRowsOlderThan( $nTimeStamp, $sColumnToCompare = 'created_at' ) {
		$oVerify = new Verify( $this->getDbVo() );
		if ( !$oVerify->hasColumn( $sColumnToCompare ) ) {
			throw new \Exception( sprintf( 'Trying to use column that does not exist: "%s"', $sColumnToCompare ) );
		}
		return $this->deleteWhere( [ $sColumnToCompare => $nTimeStamp ] );
	}
}