<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Database\Operations;

use FernleafSystems\Wordpress\Plugin\Foundation\Database\Vo\DbVo;

class Base {

	/**
	 * @var DbVo
	 */
	protected $oDbVo;

	/**
	 * @param DbVo $DbVo
	 */
	public function __construct( $DbVo ) {
		$this->oDbVo = $DbVo;
	}

	/**
	 * @return DbVo
	 */
	protected function getDbVo() {
		return $this->oDbVo;
	}
}