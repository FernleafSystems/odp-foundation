<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Database\Operations;

class Cron extends Base {

	/**
	 * @param string   $sFullHookName
	 * @param callable $aCallableHook
	 * @return $this
	 */
	public function create( $sFullHookName, $aCallableHook ) {
		if ( !wp_next_scheduled( $sFullHookName ) && !defined( 'WP_INSTALLING' ) ) {
			$nNextRun = strtotime( 'tomorrow 6am' ) - get_option( 'gmt_offset' )*HOUR_IN_SECONDS; // TODO: Services::WpGeneral()->getOption
			wp_schedule_event( $nNextRun, 'daily', $sFullHookName );
		}
		add_action( $sFullHookName, $aCallableHook );
		return $this;
	}

	/**
	 * @return $this
	 */
	public function delete( $sFullHookName ) {
		wp_clear_scheduled_hook( $sFullHookName );
		return $this;
	}
}