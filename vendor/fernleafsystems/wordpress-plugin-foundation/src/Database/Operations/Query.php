<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Database\Operations;

use FernleafSystems\Wordpress\Services\Services;

class Query extends Base {

	/**
	 * @param array $aData
	 * @return int|false number of rows inserted
	 */
	public function insertData( $aData ) {
		return Services::WpDb()->insertDataIntoTable( $this->getDbVo()->getTableName(), $aData );
	}

	/**
	 * @return array
	 */
	public function selectAll() {
		return $this->selectColumns();
	}

	/**
	 * @param array $aColumnsToSelect Leave empty to select all (*) columns
	 * @param bool  $bExcludeDeletedRows
	 * @return array
	 */
	public function selectColumns( $aColumnsToSelect = [], $bExcludeDeletedRows = true ) {

		// Try to get the database entry that corresponds to this set of data. If we get nothing, fail.
		$sQuery = "SELECT %s FROM `%s` %s";

		$oVerify = new Verify( $this->getDbVo() );

		$aColumns = $oVerify->verifyColumnsSelection( $aColumnsToSelect );
		$sColumnsSelection = empty( $aColumns ) ? '*' : implode( ',', $aColumns );

		$sQuery = sprintf( $sQuery,
			$sColumnsSelection,
			$this->getDbVo()->getTableName(),
			( $bExcludeDeletedRows && $oVerify->hasColumn( 'deleted_at' ) ) ? "WHERE `deleted_at` = 0" : ''
		);
		$mResult = $this->selectCustom( $sQuery );
		return ( is_array( $mResult ) && isset( $mResult[ 0 ] ) ) ? $mResult : [];
	}

	/**
	 * @param string $sQuery
	 * @param        $nFormat
	 * @return array|boolean
	 */
	public function selectCustom( $sQuery, $nFormat = ARRAY_A ) {
		return Services::WpDb()->selectCustom( $sQuery, $nFormat );
	}

	/**
	 * @param array $aData  - new insert data (associative array, column=>data)
	 * @param array $aWhere - insert where (associative array)
	 * @return int|bool (number of rows affected)
	 */
	public function updateRowsWhere( $aData, $aWhere ) {
		return Services::WpDb()->updateRowsFromTableWhere( $this->getDbVo()->getTableName(), $aData, $aWhere );
	}
}