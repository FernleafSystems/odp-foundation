<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Database\Operations;

use FernleafSystems\Wordpress\Services\Services;

class Clean extends Base {

	/**
	 * @param int $nAutoExpirePeriod in seconds
	 * @return bool|int
	 */
	public function cleanOld( $nAutoExpirePeriod ) {
		if ( empty( $nAutoExpirePeriod ) ) {
			return false;
		}
		$nTimeStamp = Services::Request()->ts() - $nAutoExpirePeriod;
		return ( new Delete( $this->getDbVo() ) )->deleteRowsOlderThan( $nTimeStamp );
	}
}