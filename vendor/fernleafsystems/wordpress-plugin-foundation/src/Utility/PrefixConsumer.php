<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Utility;

/**
 * Trait PrefixConsumer
 * @package FernleafSystems\Wordpress\Plugin\Foundation\Root
 */
trait PrefixConsumer {

	/**
	 * @var Prefix
	 */
	private $oPrefix;

	/**
	 * @return Prefix
	 */
	public function getPrefix() {
		return $this->oPrefix;
	}

	/**
	 * @param Prefix $oRootFile
	 * @return $this
	 */
	public function setPrefix( $oRootFile ) {
		$this->oPrefix = $oRootFile;
		return $this;
	}
}