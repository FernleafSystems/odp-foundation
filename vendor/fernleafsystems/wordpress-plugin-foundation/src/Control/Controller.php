<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Control;

use FernleafSystems\Utilities\Data\Adapter\DynPropertiesClass;
use FernleafSystems\Wordpress\Plugin\Foundation;
use FernleafSystems\Wordpress\Plugin\Foundation\Configuration;
use FernleafSystems\Wordpress\Plugin\Foundation\Display;
use FernleafSystems\Wordpress\Plugin\Foundation\Flags\Lookup;
use FernleafSystems\Wordpress\Plugin\Foundation\Locale\TextDomain;
use FernleafSystems\Wordpress\Plugin\Foundation\Module;
use FernleafSystems\Wordpress\Plugin\Foundation\Permissions\Permissions;
use FernleafSystems\Wordpress\Plugin\Foundation\Request\Handler;
use FernleafSystems\Wordpress\Plugin\Foundation\Request\Session\Visitor;
use FernleafSystems\Wordpress\Plugin\Foundation\Utility\Prefix;
use FernleafSystems\Wordpress\Services\Services;

/**
 * @property Module\Base\Controller[] $modules
 */
class Controller extends DynPropertiesClass {

	private bool $hasBooted = false;

	/**
	 * @var Controller
	 */
	private static $oFoundationController;

	private ?Configuration\PluginController\Config $cfg = null;

	protected ?Foundation\Root\File $rootFile = null;

	protected ?Foundation\Paths\Root $rootPaths = null;

	protected ?Permissions $permissions = null;

	protected ?Prefix $prefix = null;

	protected ?Display\Labels $labels = null;

	private ?Foundation\Paths\Derived $paths = null;

	protected ?Handler\Ajax $ajax = null;

	protected ?Lookup $flags = null;

	protected ?Handler\Form $formHandler = null;

	protected ?Handler\Query $queryHandler = null;

	protected ?Visitor $visitorSession = null;

	/**
	 * @var Foundation\Module\Core\Options
	 */
	protected $oCoreOptions;

	public function __construct( Foundation\Root\File $rootFile, bool $autoBoot = true ) {
		$this->rootFile = $rootFile;
		Services::GetInstance();
		if ( $autoBoot ) {
			$this->boot();
		}
	}

	public function boot() :void {
		if ( !$this->hasBooted ) {
			$this->hasBooted = true;
			try {
				$this->loadMainConfig();
				$this->start();
			}
			catch ( \Exception $e ) {
				trigger_error( 'Failed to Boot plugin due to caught Exception: '.$e->getMessage() );
			}
		}
	}

	/**
	 * @return Controller
	 */
	public function getFoundationController() {
		return self::$oFoundationController;
	}

	/**
	 * @param Controller $foundation
	 * @return $this
	 */
	public function setFoundationController( $foundation ) {
		self::$oFoundationController = $foundation;
		return $this;
	}

	protected function start() {
		$this->loadAllModules();
		Services::WpTrack(); // todo: remove

		if ( $this->getIsValidAdminArea() ) {
			( new TextDomain() )
				->setConfig( $this->config() )
				->loadTextDomain( $this->getPluginPaths() );
		}

		if ( Services::Request()->countQuery() > 0 ) {
			$this->getQueryHandler();
		}
		if ( Services::Request()->countPost() > 0 ) {
			$this->getFormHandler();
		}
		if ( Services::WpGeneral()->isAjax() ) {
			( new Foundation\Request\AjaxHandler() )->setPrefix( $this->getPrefix() );
			$this->getAjaxHandler();
		}
		$this->doRegisterHooks();
	}

	protected function doRegisterHooks() {
		$this->registerActivationHooks();
		add_action( 'init', [ $this, 'onWpInit' ] );
		add_action( 'wp_loaded', [ $this, 'onWpLoaded' ] );
		add_action( 'admin_init', [ $this, 'onWpAdminInit' ] );
		add_action( 'shutdown', [ $this, 'onWpShutdown' ], 0 );
	}

	public function onWpInit() {
		$this->getLabels(); // initiates the necessary.
		( new Foundation\Assets\Enqueue( $this->getPrefix(), $this->getPluginPaths() ) )
			->setConfig( $this->config() );
		$this->initUpgraders();
	}

	protected function initUpgraders() {
		( new Foundation\Updates\Upgraders\AptoWeb() )
			->setConfig( $this->config() )
			->setPrefix( $this->getPrefix() )
			->setRootFile( $this->getRootFile() )
			->execute();
	}

	public function onWpAdminInit() {
		if ( $this->getIsValidAdminArea() ) {
			// initiates the necessary.
			( new Foundation\Admin\Notices() )->setPrefix( $this->getPrefix() );
			( new Display\Hide() )
				->setPrefix( $this->getPrefix() )
				->setRootFile( $this->getRootFile() )
				->setConfig( $this->config() );
			( new Display\ActionLinks( $this->getRootFile() ) )
				->setConfig( $this->config() );
			( new Display\RowMeta() )
				->setRootFile( $this->getRootFile() )
				->setConfig( $this->config() );
			( new Display\UpdateMessage( $this->getLabels(), $this->getRootFile() ) )->setPrefix( $this->getPrefix() );
		}

//		if ( $oConfig->getProperty( 'show_dashboard_widget' ) === true ) {
//			add_action( 'wp_dashboard_setup', array( $this, 'onWpDashboardSetup' ) ); TODO
//		}
	}

	/**
	 * @return string - the unique, never-changing site install ID.
	 */
	public function getSiteInstallationId() {
		$sOptKey = $this->getPrefix()->prefixOption( 'install_id' );
		$sId = (string)Services::WpGeneral()->getOption( $sOptKey );

		$sUrl = base64_encode( Services::Data()->urlStripSchema( Services::WpGeneral()->getHomeUrl( '', true ) ) );
		if ( empty( $sId ) || !strpos( $sId, ':' ) || strpos( $sId, $sUrl ) !== 0 ) {
			$sId = $sUrl.':'.sha1( uniqid( Services::WpGeneral()->getHomeUrl( '', true ), true ) );
			Services::WpGeneral()->updateOption( $sOptKey, $sId );
		}
		return str_replace( $sUrl.':', '', $sId );
	}

	public function onWpLoaded() {
		( new Foundation\Updates\Automatic( $this->getRootFile() ) )->setConfig( $this->config() );
		$this->loadMenus(); // this must fire *before* admin_init

		if ( $this->config()->getProperty( 'use_plugin_cron' ) ) {
			( new Foundation\Cron\DailyCron() )
				->setPrefix( $this->getPrefix() )
				->init();
			( new Foundation\Cron\HourlyCron() )
				->setPrefix( $this->getPrefix() )
				->init();
		}
	}

	/**
	 * Hooked to 'shutdown'
	 */
	public function onWpShutdown() {
		$this->getSiteInstallationId();
		do_action( $this->getPrefix()->prefix( 'pre_plugin_shutdown' ) );
		do_action( $this->getPrefix()->prefix( 'plugin_shutdown' ) );
	}

	/**
	 * Registers the plugins activation, deactivate and uninstall hooks.
	 */
	protected function registerActivationHooks() {
		register_activation_hook( $this->getRootFile()->getFullPath(), [ $this, 'onWpActivatePlugin' ] );
		register_deactivation_hook( $this->getRootFile()->getFullPath(), [ $this, 'onWpDeactivatePlugin' ] );
		//	register_uninstall_hook( $this->oPluginVo->getRootFile(), array( $this, 'onWpUninstallPlugin' ) );
	}

	public function onWpDeactivatePlugin() {
		$oPrefix = $this->getPrefix();
		if ( $this->getPermissions()->getMeetsBasePermissions()
			 && apply_filters( $oPrefix->prefix( 'delete_on_deactivate' ), false ) ) {
			do_action( $oPrefix->prefix( 'delete_plugin' ) );
			$this->deleteControllerConfig();
		}
	}

	public function onWpActivatePlugin() {
		do_action( $this->getPrefix()->prefix( 'plugin_activate' ) );
	}

	/**
	 * @return Foundation\Service\Email
	 */
	public function getEmailService() {
		return new Foundation\Service\Email( $this );
	}

	/**
	 * @param bool $bCheckPerms
	 * @return bool
	 */
	public function getIsValidAdminArea( $bCheckPerms = true ) {
		if ( $bCheckPerms && did_action( 'init' ) && !$this->getPermissions()->getMeetsBasePermissions() ) {
			return false;
		}

		$WP = Services::WpGeneral();
		if ( !$WP->isMultisite() && is_admin() ) {
			return true;
		}
		elseif ( $WP->isMultisite() && is_network_admin() && $this->config()->getIsWpmsNetworkAdminOnly() ) {
			return true;
		}
		return false;
	}

	/**
	 * @deprecated 0.13
	 */
	public function getIsForceOff() :bool {
		return $this->getFlagsLookup()->getHasFlagForceOff();
	}

	public function loadAllModules() {
		foreach ( $this->config()->getPluginModules() as $slug => $properties ) {
			try {
				$this->loadModuleController( $slug );
			}
			catch ( \Exception $oE ) {
				Services::WpGeneral()->wpDie( $oE->getMessage() );
			}
		}

		do_action( $this->getPrefix()->prefix( 'run_processors' ) );
	}

	/**
	 * @param string $slug
	 * @return Module\Base\Controller|mixed
	 * @throws \Exception
	 */
	public function loadModuleController( $slug ) {
		if ( !is_array( $this->modules ) ) {
			$this->modules = [];
		}
		if ( isset( $this->modules[ $slug ] ) ) {
			return $this->modules[ $slug ];
		}

		// This all needs organised much better
		$sPathToConfig = $this->getPluginPaths()->getPath_Config( $slug );
		$sStorageKey = $this->getPrefix()->prefix( \md5( $sPathToConfig ) );
		$oModConfig = ( new Configuration\Build() )->build(
			$sPathToConfig,
			$sStorageKey,
			new Configuration\Module\Config()
		);

		$sNameSpaceTemplate = '\FernleafSystems\Wordpress\Plugin\%s\Module\%s';
		$sModuleNameSpace = sprintf(
			$sNameSpaceTemplate,
			$this->config()->getPluginNamespace(),
			str_replace( ' ', '', ucwords( str_replace( '_', ' ', $slug ) ) )
		);
		$sControllerClass = $sModuleNameSpace.'\\Controller';
		if ( !class_exists( $sControllerClass ) ) {
			throw new \Exception( sprintf( 'Class %s does not exist.', $sControllerClass ) );
		}
		/** @var Module\Base\Controller $mod */
		$mod = new $sControllerClass( $this, $oModConfig );

		$mods = $this->modules;
		$mods[ $slug ] = $mod;
		$this->modules = $mods;
		return $mod;
	}

	protected function deleteControllerConfig() :void {
		Services::WpGeneral()->deleteOption( $this->getPluginControllerOptionsKey() );
	}

	/**
	 * @param string $action
	 */
	public function getNonceActionData( $action = '' ) :array {
		return [
			'action'     => $this->getPrefix()->prefix(), //wp ajax doesn't work without this.
			'exec'       => $action,
			'exec_nonce' => wp_create_nonce( $action ),
		];
	}

	public function getAjaxHandler() :Handler\Ajax {
		return $this->ajax ??= new Handler\Ajax( $this->getPrefix(), $this->getPermissions() );
	}

	public function getFlagsLookup() :Lookup {
		return $this->flags ??= new Lookup( $this->getPluginPaths() );
	}

	public function getFormHandler() :Handler\Form {
		return $this->formHandler ??= new Handler\Form( $this->getPrefix(), $this->getPermissions() );
	}

	public function getQueryHandler() :Handler\Query {
		return $this->queryHandler ??= new Handler\Query( $this->getPrefix(), $this->getPermissions() );
	}

	public function getVisitorSession() :Visitor {
		return $this->visitorSession ??= new Visitor( $this->getPrefix() );
	}

	public function getLabels() :Display\Labels {
		return $this->labels ??= ( new Display\Labels( $this->getRootFile(), $this->getPrefix() ) )->setConfig( $this->config() );
	}

	public function loadMenus() {
		( new Foundation\Admin\Menu( $this->getLabels(), $this->getPluginPaths(), $this->getPrefix() ) )
			->setConfig( $this->config() );
	}

	public function getPermissions() :Permissions {
		return $this->permissions ??= ( new Permissions( $this->getPrefix() ) )->setConfig( $this->config() );
	}

	public function getPluginPaths() :Foundation\Paths\Derived {
		return $this->paths ??= new Foundation\Paths\Derived( $this->getRootPaths() );
	}

	public function getPrefix() :Prefix {
		return $this->prefix ??= ( new Prefix() )->setConfig( $this->config() );
	}

	public function getRootFile() :Foundation\Root\File {
		return $this->rootFile ??= new Foundation\Root\File( __FILE__ );
	}

	public function getRootPaths() :Foundation\Paths\Root {
		return $this->rootPaths ??= new Foundation\Paths\Root( $this->getRootFile() );
	}

	/**
	 * @throws \Exception
	 */
	protected function loadMainConfig() :Configuration\PluginController\Config {
		return $this->config();
	}

	/**
	 * @return Configuration\PluginController\Config
	 */
	public function config() :Configuration\PluginController\Config {
		if ( empty( $this->cfg ) || !$this->cfg->hasDefinition() ) {
			$this->cfg = ( new Configuration\PluginController\Config() )->applyFromArray(
				( new Configuration\Build() )->build(
					$this->getPluginPaths()->getPath_Config( 'plugin' ),
					$this->getPluginControllerOptionsKey(),
				)->getRawDataAsArray()
			);
		}
		return $this->cfg;
	}

	private function getPluginControllerOptionsKey() :string {
		return 'icwppluginconfig'.substr( md5( $this->getRootFile()->getFullPath() ), 0, 10 );
	}
}