<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Control;

use FernleafSystems\Wordpress\Plugin\Foundation\Root\File;

class Bootstrap {

	/**
	 * @param string $rootFilePath
	 */
	public static function Boot( $rootFilePath ) :Controller {
		return new Controller( new File( $rootFilePath ) );
	}
}