<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Control;

trait PluginControllerConsumer {

	/**
	 * @var Controller
	 */
	private $oPluginController;

	public function getCon() {
		return $this->oPluginController;
	}

	public function con() :Controller {
		return $this->oPluginController;
	}

	/**
	 * @param Controller $con
	 * @return $this
	 */
	public function setCon( $con ) {
		$this->oPluginController = $con;
		return $this;
	}

	/**
	 * @return Controller
	 * @deprecated 0.1.2
	 */
	public function getPluginController() {
		return $this->getCon();
	}

	/**
	 * @param Controller $oController
	 * @return $this
	 * @deprecated 0.1.2
	 */
	public function setPluginController( $oController ) {
		return $this->setCon( $oController );
	}
}