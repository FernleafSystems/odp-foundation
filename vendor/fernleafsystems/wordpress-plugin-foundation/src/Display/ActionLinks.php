<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Display;

use FernleafSystems\Wordpress\Plugin\Foundation;

/**
 * Class ActionLinks
 * @package FernleafSystems\Wordpress\Plugin\Foundation\Display
 */
class ActionLinks {

	use Foundation\Configuration\PluginController\ConfigConsumer;

	/**
	 * @param Foundation\Root\File $oRoot
	 */
	public function __construct( $oRoot ) {
		add_filter( 'plugin_action_links_'.$oRoot->getPluginBaseFile(), [ $this, 'oActionLinks' ], 50 );
	}

	/**
	 * @param array $actionLinks
	 * @return array
	 */
	public function oActionLinks( $actionLinks ) {

		$linksToAdd = $this->getConfig()->getActionLinks( 'add' );

		if ( !empty( $linksToAdd ) && is_array( $linksToAdd ) ) {

			$linkTemplate = '<a href="%s" target="%s">%s</a>';
			foreach ( $linksToAdd as $aLink ) {
				if ( empty( $aLink[ 'name' ] ) || ( empty( $aLink[ 'url_method_name' ] ) && empty( $aLink[ 'href' ] ) ) ) {
					continue;
				}

				if ( !empty( $aLink[ 'url_method_name' ] ) ) {
					$method = $aLink[ 'url_method_name' ];
					if ( method_exists( $this, $method ) ) {
						$settingsLink = sprintf( $linkTemplate, $this->{$method}(), "_top", $aLink[ 'name' ] );
						array_unshift( $actionLinks, $settingsLink );
					}
				}
				elseif ( !empty( $aLink[ 'href' ] ) ) {
					$settingsLink = sprintf( $linkTemplate, $aLink[ 'href' ], "_blank", $aLink[ 'name' ] );
					array_unshift( $actionLinks, $settingsLink );
				}
			}
		}

		return $actionLinks;
	}
}