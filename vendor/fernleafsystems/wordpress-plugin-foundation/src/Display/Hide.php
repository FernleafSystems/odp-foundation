<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Display;

use FernleafSystems\Wordpress\Plugin\Foundation;

class Hide {

	use Foundation\Configuration\PluginController\ConfigConsumer,
		Foundation\Root\RootFileConsumer,
		Foundation\Utility\PrefixConsumer;

	public function __construct() {
		add_filter( 'all_plugins', [ $this, 'hidePluginFromTableList' ] );
	}

	/**
	 * Added to a WordPress filter ('all_plugins') which will remove this particular plugin from the
	 * list of all plugins based on the "plugin file" name.
	 * @param array $aPlugins
	 * @return array
	 */
	public function hidePluginFromTableList( $aPlugins ) {
		$bHide = apply_filters( $this->getPrefix()->prefix( 'hide_plugin' ), $this->getConfig()
																				  ->getProperty( 'hide' ) );
		if ( $bHide ) {
			$sPluginBaseFileName = $this->getRootFile()->getPluginBaseFile();
			if ( isset( $aPlugins[ $sPluginBaseFileName ] ) ) {
				unset( $aPlugins[ $sPluginBaseFileName ] );
			}
		}
		return $aPlugins;
	}
}