<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Display;

use FernleafSystems\Wordpress\Plugin\Foundation;

/**
 * Class Labels
 * @package FernleafSystems\Wordpress\Plugin\Foundation\Display
 */
class Labels {

	use Foundation\Configuration\PluginController\ConfigConsumer;

	/**
	 * @var Foundation\Utility\Prefix
	 */
	private $oPrefix;

	/**
	 * @var Foundation\Root\File
	 */
	private $oRootFile;

	/**
	 * @var array
	 */
	protected $aFinalLabels;

	/**
	 * @param Foundation\Root\File      $oRootFile
	 * @param Foundation\Utility\Prefix $oPrefix
	 */
	public function __construct( $oRootFile, $oPrefix ) {
		$this->oPrefix = $oPrefix;
		$this->oRootFile = $oRootFile;
	}

	protected function init() {
		add_filter( 'all_plugins', [ $this, 'doPluginLabels' ] );
	}

	/**
	 * @param array $aPlugins
	 * @return array
	 */
	public function doPluginLabels( $aPlugins ) {
		$aLabelData = $this->all();
		if ( empty( $aLabelData ) ) {
			return $aPlugins;
		}

		$sPluginFile = $this->oRootFile->getPluginBaseFile();
		// For this plugin, overwrite any specified settings
		if ( array_key_exists( $sPluginFile, $aPlugins ) ) {
			foreach ( $aLabelData as $sLabelKey => $sLabel ) {
				$aPlugins[ $sPluginFile ][ $sLabelKey ] = $sLabel;
			}
		}
		return $aPlugins;
	}

	/**
	 * @return string
	 */
	public function getHumanName() {
		$labels = $this->all();
		return empty( $labels[ 'Name' ] ) ? $this->getConfig()->getProperty( 'human_name' ) : $labels[ 'Name' ];
	}

	/**
	 * @return string
	 */
	public function getIconUrl16() {
		return $this->getLabel( 'icon_url_16x16' );
	}

	/**
	 * @return string
	 */
	public function getIconUrl32() {
		return $this->getLabel( 'icon_url_32x32' );
	}

	/**
	 * @return string
	 */
	public function getName() {
		return $this->getLabel( 'Name' );
	}

	/**
	 * @param string $sKey
	 * @return string
	 */
	public function getLabel( $sKey = '' ) {
		$aLabels = $this->all();
		return ( !empty( $sKey ) && isset( $aLabels[ $sKey ] ) ) ? $aLabels[ $sKey ] : '';
	}

	/**
	 * @return array
	 */
	public function all() {
		if ( !isset( $this->aFinalLabels ) || !is_array( $this->aFinalLabels ) ) {
			$this->aFinalLabels = apply_filters( $this->oPrefix->prefix( 'plugin_labels' ), $this->getConfig()
																								 ->getLabels() );
		}
		return $this->aFinalLabels;
	}
}
