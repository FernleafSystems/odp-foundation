<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Display;

use FernleafSystems\Wordpress\Plugin\Foundation;

class RowMeta {

	use Foundation\Configuration\PluginController\ConfigConsumer,
		Foundation\Root\RootFileConsumer;

	public function __construct() {
		add_filter( 'plugin_row_meta', [ $this, 'onPluginRowMeta' ], 50, 2 );
	}

	/**
	 * @param array  $pluginMeta
	 * @param string $sPluginFile
	 * @return array
	 */
	public function onPluginRowMeta( $pluginMeta, $sPluginFile ) {

		if ( $sPluginFile == $this->getRootFile()->getPluginBaseFile() ) {
			$aMeta = $this->getConfig()->getPluginMeta();

			$sLinkTemplate = '<strong><a href="%s" target="%s">%s</a></strong>';
			foreach ( $aMeta as $aMetaLink ) {
				$settingsLink = sprintf( $sLinkTemplate, $aMetaLink[ 'href' ], "_blank", $aMetaLink[ 'name' ] );
				$pluginMeta[] = $settingsLink;
			}
		}
		return $pluginMeta;
	}
}