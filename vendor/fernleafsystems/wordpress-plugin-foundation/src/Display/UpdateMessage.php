<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Display;

use FernleafSystems\Wordpress\Plugin\Foundation;

class UpdateMessage {

	use Foundation\Utility\PrefixConsumer;

	/**
	 * @var Labels
	 */
	private $oLabels;

	/**
	 * UpdateMessage constructor.
	 * @param Labels               $oLabels
	 * @param Foundation\Root\File $oRoot
	 */
	public function __construct( $oLabels, $oRoot ) {
		$this->oLabels = $oLabels;
		add_action( 'in_plugin_update_message-'.$oRoot->getPluginBaseFile(), [
			$this,
			'onWpPluginUpdateMessage'
		] );
	}

	/**
	 * Displays a message in the plugins listing when a plugin has an update available.
	 */
	public function onWpPluginUpdateMessage() {
		$oPfx = $this->getPrefix();
		$sDefault = sprintf( 'Upgrade Now To Get The Latest Available %s Features.', $this->oLabels->getHumanName() );
		$sMessage = apply_filters( $oPfx->prefix( 'plugin_update_message' ), $sDefault );
		if ( empty( $sMessage ) ) {
			$sMessage = '';
		}
		else {
			$sMessage = sprintf(
				'<div class="%s plugin_update_message">%s</div>',
				$oPfx->getPluginPrefix(),
				$sMessage
			);
		}
		echo $sMessage;
	}
}