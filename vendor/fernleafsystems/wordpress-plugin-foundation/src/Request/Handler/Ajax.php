<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Request\Handler;

use FernleafSystems\Wordpress\Plugin\Foundation\Permissions\Permissions;
use FernleafSystems\Wordpress\Plugin\Foundation\Utility\Prefix;
use FernleafSystems\Wordpress\Services\Services;

class Ajax extends Base {

	/**
	 * @param Prefix      $oPrefix
	 * @param Permissions $oPermissions
	 */
	public function __construct( $oPrefix, $oPermissions ) {
		parent::__construct( $oPrefix, $oPermissions );
		if ( $this->getIsPluginAjaxCall() ) {
			add_action( 'init', [ $this, 'handleRequest' ] );
		}
	}

	public function handleRequest() {
		if ( $this->getNoncer()->verifyNonce() ) {
			do_action( $this->oPrefix->prefix( 'ajax_request' ) );
			if ( $this->oPermissions->getHasPermissionToManage() ) {
				do_action( $this->oPrefix->prefix( 'authenticated_ajax_request' ) );
			}
		}
		else {
			Services::WpGeneral()->wpDie( 'Failed Plugin Ajax Nonce' );
		}
	}

	/**
	 * @return string
	 */
	public function getAjaxCallFlag() {
		return $this->oPrefix->prefix( 'ajax' );
	}

	/**
	 * @return bool
	 */
	protected function getIsPluginAjaxCall() {
		$oReq = Services::Request();
		$bSubmit = false;
		if ( ( $oReq->countPost() > 0 ) || ( $oReq->countQuery() > 0 ) ) {
			$sKey = $this->getAjaxCallFlag();
			$bSubmit = ( $oReq->request( $sKey, false, 0 ) == 1 );
		}
		return $bSubmit;
	}
}