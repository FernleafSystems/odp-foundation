<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Request\Handler;

use FernleafSystems\Wordpress\Plugin\Foundation\Permissions\Permissions;
use FernleafSystems\Wordpress\Plugin\Foundation\Utility\Prefix;
use FernleafSystems\Wordpress\Services\Services;

class Form extends Base {

	/**
	 * @param Prefix      $oPrefix
	 * @param Permissions $oPermissions
	 */
	public function __construct( $oPrefix, $oPermissions ) {
		parent::__construct( $oPrefix, $oPermissions );
		add_action( 'init', [ $this, 'handleRequest' ] );
	}

	public function handleRequest() {
		if ( $this->getIsPluginFormSubmit() ) {
			if ( $this->getNoncer()->verifyNonce() ) {
				do_action( $this->oPrefix->prefix( 'form_submit' ) );
				if ( $this->oPermissions->getHasPermissionToManage() ) {
					do_action( $this->oPrefix->prefix( 'authenticated_form_submit' ) );
				}
				Services::Response()->redirect( Services::WpGeneral()->getUrl_CurrentAdminPage() );
			}
			else {
				$this->reject( 'Ruh Roh: Failed Form Nonce Verification - perhaps you should be logged in.' );
			}
		}
	}

	/**
	 * TODO: handle request 'icwp_link_action'
	 * @return bool
	 */
	protected function getIsPluginFormSubmit() :bool {
		$req = Services::Request();
		return !Services::WpGeneral()->isAjax()
			   && $req->isPost()
			   && $this->isPluginRequest()
			   && !is_null( $req->post( 'plugin_form_submit' ) );
	}
}