<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Request\Handler;

use FernleafSystems\Wordpress\Plugin\Foundation\Utility\Prefix;
use FernleafSystems\Wordpress\Services\Services;

class Nonce {

	const NONCE_KEY = 'nonce';
	const DEFAULT_NONCE_ACTION = 'gen';

	/**
	 * @var Prefix
	 */
	protected $oPrefix;

	/**
	 * @param Prefix $oPrefix
	 */
	public function __construct( $oPrefix ) {
		$this->oPrefix = $oPrefix;
	}

	/**
	 * @param array  $aData
	 * @param string $sAction
	 * @return array
	 */
	public function addNonceToArray( $aData, $sAction = 'gen' ) {
		return array_merge( $aData, $this->getNonceAsArray( $sAction ) );
	}

	/**
	 * @param string $sUrl
	 * @param string $sAction
	 * @return string
	 */
	public function addNonceToUrl( $sUrl, $sAction = 'gen' ) {
		return add_query_arg( $this->getNonceAsArray( $sAction ), $sUrl );
	}

	/**
	 * @param string $sAction
	 * @return array
	 */
	public function getNonceAsArray( $sAction = 'gen' ) {
		return [
			$this->oPrefix->prefix( 'action' ) => $sAction,
			$this->getNonceKey()               => $this->createNonce( $sAction ),
			$this->getNonceActionKey()         => $this->oPrefix->prefix( $sAction )
		];
	}

	/**
	 * @param string $sAction
	 * @return string
	 */
	public function createNonce( $sAction = '' ) {
		if ( empty( $sAction ) ) {
			$sAction = static::DEFAULT_NONCE_ACTION;
		}
		return wp_create_nonce( $this->oPrefix->prefix( $sAction ) );
	}

	/**
	 * @param string $sAction
	 * @return string
	 */
	public function createNonceFormField( $sAction = '' ) {
		if ( empty( $sAction ) ) {
			$sAction = static::DEFAULT_NONCE_ACTION;
		}
		return wp_nonce_field( $this->oPrefix->prefix( $sAction ), $this->getNonceKey() );
	}

	/**
	 * @return string
	 */
	public function getNonceAction() {
		$sAction = Services::Request()->request( $this->getNonceActionKey() );
		if ( empty( $sAction ) ) {
			$sAction = $this->oPrefix->prefix( static::DEFAULT_NONCE_ACTION );
		}
		return $sAction;
	}

	/**
	 * @return string
	 */
	public function getNonceActionKey() {
		return $this->oPrefix->prefix( 'nonaction' );
	}

	/**
	 * @param string $sExtra - optional but not used yet
	 * @return string
	 */
	public function getNonceKey( $sExtra = '' ) {
		$sExtra = preg_replace( '#[^a-z0-9]#i', '', $sExtra );
		return $this->oPrefix->prefix( static::NONCE_KEY.$sExtra );
	}

	/**
	 * @param string $sExtra
	 * @return string
	 */
	public function getNonceValue( $sExtra = '' ) {
		return Services::Request()->request( $this->getNonceKey( $sExtra ), false, '' );
	}

	/**
	 * @param bool $bStricter - if true, only valid within 0-12hrs (otherwise 0-24hrs)
	 * @return bool
	 */
	public function verifyNonce( $bStricter = false ) {
		$mVerification = wp_verify_nonce( $this->getNonceValue(), $this->getNonceAction() );
		return $bStricter ? ( $mVerification === 1 ) : ( $mVerification !== false );
	}
}