<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Request\Handler;

use FernleafSystems\Wordpress\Plugin\Foundation;
use FernleafSystems\Wordpress\Services\Services;

abstract class Base {

	const NONCE_KEY = 'nonce';

	/**
	 * @var Foundation\Utility\Prefix
	 */
	protected $oPrefix;

	/**
	 * @var Foundation\Permissions\Permissions
	 */
	protected $oPermissions;

	/**
	 * @param Foundation\Utility\Prefix          $oPrefix
	 * @param Foundation\Permissions\Permissions $oPermissions
	 */
	public function __construct( $oPrefix, $oPermissions ) {
		$this->oPrefix = $oPrefix;
		$this->oPermissions = $oPermissions;
	}

	/**
	 * This should be hooked to the appropriate place for each implementation
	 */
	abstract public function handleRequest();

	/**
	 * @return Nonce
	 */
	protected function getNoncer() {
		return new Nonce( $this->oPrefix );
	}

	/**
	 * @return string
	 */
	protected function getPluginRequestAction() {
		return (string)Services::Request()->request( $this->oPrefix->prefix( 'action' ) );
	}

	/**
	 * @return bool
	 */
	protected function isPluginRequest() {
		return ( strlen( $this->getPluginRequestAction() ) != 0 );
	}

	/**
	 * @param string $sMessage
	 */
	protected function reject( $sMessage ) {
		Services::WpGeneral()->wpDie( $sMessage );
	}
}