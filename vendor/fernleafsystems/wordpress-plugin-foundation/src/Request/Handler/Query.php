<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Request\Handler;

use FernleafSystems\Wordpress\Plugin\Foundation\Permissions\Permissions;
use FernleafSystems\Wordpress\Plugin\Foundation\Utility\Prefix;
use FernleafSystems\Wordpress\Services\Services;

class Query extends Base {

	/**
	 * @param Prefix      $oPrefix
	 * @param Permissions $oPermissions
	 */
	public function __construct( $oPrefix, $oPermissions ) {
		parent::__construct( $oPrefix, $oPermissions );
		add_action( 'init', [ $this, 'handleRequest' ] );
	}

	public function handleRequest() {
		if ( $this->isPluginRequest() ) {
			do_action( $this->oPrefix->prefix( 'no_nonce_query_action' ), $this->getPluginRequestAction() );
			if ( $this->getNoncer()->verifyNonce() ) {
				do_action( $this->oPrefix->prefix( 'query_action' ), $this->getPluginRequestAction() );
				if ( $this->oPermissions->getHasPermissionToManage() ) {
					do_action( $this->oPrefix->prefix( 'authenticated_query_action' ), $this->getPluginRequestAction() );
				}
			}
			else {
				$this->reject( 'Ruh Roh: Failed Query Nonce Verification - perhaps you should be logged in.' );
			}
		}
	}
}