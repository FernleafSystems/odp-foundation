<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Request;

use FernleafSystems\Wordpress\Plugin\Foundation;
use FernleafSystems\Wordpress\Services\Services;

class AjaxHandler {

	use Foundation\Utility\PrefixConsumer;

	public function __construct() {
		if ( Services::WpGeneral()->isAjax() ) {
			add_action( 'wp_loaded', [ $this, 'onWpLoaded' ] );
		}
	}

	public function onWpLoaded() {
		add_action( 'wp_ajax_'.$this->getPrefix()->prefix(), [ $this, 'ajaxAction' ] );
		add_action( 'wp_ajax_nopriv_'.$this->getPrefix()->prefix(), [ $this, 'ajaxAction' ] );
	}

	public function ajaxAction() {
		$sNonceAction = Services::Request()->request( 'exec' );
		check_ajax_referer( $sNonceAction, 'exec_nonce' );

		$sAction = Services::WpUsers()->isUserLoggedIn() ? 'ajaxAuthAction' : 'ajaxNonAuthAction';
		ob_start();
		$aResponseData = apply_filters( $this->getPrefix()->prefix( $sAction ), [] );
		if ( empty( $aResponseData ) ) {
			$aResponseData = apply_filters( $this->getPrefix()->prefix( 'ajaxAction' ), $aResponseData );
		}
		$sNoise = ob_get_clean();

		if ( is_array( $aResponseData ) && isset( $aResponseData[ 'success' ] ) ) {
			$bSuccess = $aResponseData[ 'success' ];
		}
		else {
			$bSuccess = false;
			$aResponseData = [];
		}

		wp_send_json(
			[
				'success' => $bSuccess,
				'data'    => $aResponseData,
				'noise'   => $sNoise
			]
		);
	}
}
