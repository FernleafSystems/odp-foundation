<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Request\Session;

use FernleafSystems\Wordpress\Plugin\Foundation\Utility\Prefix;
use FernleafSystems\Wordpress\Services\Services;

class Visitor {

	/**
	 * @var Prefix
	 */
	protected $oPrefix;

	/**
	 * @var string
	 */
	protected $sSessionId;

	/**
	 * @var string
	 */
	protected $sRequestId;

	public function __construct( $oPrefix ) {
		$this->oPrefix = $oPrefix;
	}

	/**
	 * @return $this
	 */
	public function close() {
		Services::Response()->cookieDelete( $this->oPrefix->getPluginPrefix() );
		$this->sSessionId = null;
		return $this;
	}

	/**
	 * @return $this
	 */
	public function open() {
		$WP = Services::WpGeneral();
		Services::Response()->cookieSet(
			$this->oPrefix->getPluginPrefix(),
			$this->getId(),
			Services::Request()->ts() + DAY_IN_SECONDS*30,
			$WP->getCookiePath(),
			$WP->getCookieDomain()
		);
		return $this;
	}

	/**
	 * @return string
	 */
	public function getId() {
		if ( empty( $this->sSessionId ) ) {
			$oReq = Services::Request();
			$this->sSessionId = $oReq->cookie( $this->oPrefix->getPluginPrefix() );
			if ( !is_string( $this->sSessionId ) || ( strlen( $this->sSessionId ) != 8 ) ) {
				$this->sSessionId = substr(
					sha1( $this->oPrefix->prefix( Services::Request()->ip().$oReq->ts() ) ),
					0, 8
				);
				$this->open();
			}
		}
		return $this->sSessionId;
	}

	/**
	 * @return string
	 */
	public function getRequestId() {
		if ( empty( $this->sRequestId ) ) {
			$this->sRequestId = md5( $this->getId().Services::Request()->ip().Services::Request()->ts() );
		}
		return $this->sRequestId;
	}
}