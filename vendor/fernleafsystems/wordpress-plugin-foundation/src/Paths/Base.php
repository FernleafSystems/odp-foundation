<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Paths;

/**
 * Class Base
 * @package FernleafSystems\Wordpress\Plugin\Foundation\Paths
 */
abstract class Base {

	/**
	 * @return string
	 */
	abstract protected function getRoot();

	/**
	 * @param string $sPath
	 * @return string
	 */
	public function getFullPath( $sPath ) {
		return $this->pathJoin( $this->getRoot(), $sPath );
	}

	/**
	 * @param string $sAsset
	 * @return string
	 */
	public function getPath_Assets( $sAsset = '' ) {
		return $this->getFullPath( $this->pathJoin( 'assets', $sAsset ) );
	}

	/**
	 * @param string $sAsset
	 * @return string
	 */
	public function getPath_AssetCss( $sAsset = '' ) {
		return $this->getPath_Assets( $this->pathJoin( 'css', $sAsset ) );
	}

	/**
	 * @param string $sAsset
	 * @return string
	 */
	public function getPath_AssetJs( $sAsset = '' ) {
		return $this->getPath_Assets( $this->pathJoin( 'js', $sAsset ) );
	}

	/**
	 * @param string $sAsset
	 * @return string
	 */
	public function getPath_AssetImage( $sAsset = '' ) {
		return $this->getPath_Assets( $this->pathJoin( 'images', $sAsset ) );
	}

	/**
	 * @param string $sFile
	 * @return string
	 */
	public function getPath_Source( $sFile ) {
		return $this->getFullPath( $this->pathJoin( 'src', $sFile ) );
	}

	/**
	 * @param string $sFile
	 * @return string
	 */
	public function getPath_Temp( $sFile = '' ) {
		return $this->getFullPath( $this->pathJoin( 'temp', $sFile ) );
	}

	/**
	 * @param string $sFile
	 * @return string
	 */
	public function getPath_Templates( $sFile = '' ) {
		return $this->getFullPath( $this->pathJoin( 'templates', $sFile ) );
	}

	/**
	 * @param string $sBase
	 * @param string $sPath
	 * @return string
	 */
	protected function pathJoin( $sBase, $sPath ) {
		return rtrim( $sBase, DIRECTORY_SEPARATOR ).DIRECTORY_SEPARATOR.ltrim( $sPath, DIRECTORY_SEPARATOR );
	}

	/**
	 * @param string $sBase
	 * @param string $sPath
	 * @return string
	 */
	protected function urlJoin( $sBase, $sPath ) {
		return rtrim( $sBase, '/' ).'/'.ltrim( $sPath, '/' );
	}
}