<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Paths;

/**
 * Class Foundation
 * @package FernleafSystems\Wordpress\Plugin\Foundation\Paths
 */
class Foundation extends Base {

	/**
	 * @var string
	 */
	private $sFoundationRoot;

	/**
	 * Foundation constructor.
	 * @param string $sFoundationPath
	 */
	public function __construct( $sFoundationPath ) {
		$this->sFoundationRoot = $sFoundationPath;
	}

	/**
	 * @return string
	 */
	protected function getRoot() {
		return $this->sFoundationRoot;
	}
}