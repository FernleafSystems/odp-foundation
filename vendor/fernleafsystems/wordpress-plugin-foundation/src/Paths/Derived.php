<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Paths;

use FernleafSystems\Wordpress\Plugin\Foundation\Paths\Root as RootPath;
use FernleafSystems\Wordpress\Services\Services;

/**
 * Class Derived
 *
 * Assumes:
 * assets - store JS, CSS, Images in sub-dir to 'assets' folder
 *
 * @package FernleafSystems\Wordpress\Plugin\Paths
 */
class Derived extends Base {

	/**
	 * @var RootPath
	 */
	private $oRootPath;

	/**
	 * Derived constructor.
	 *
	 * @param RootPath $oRootPaths
	 */
	public function __construct( $oRootPaths ) {
		$this->oRootPath = $oRootPaths;
	}

	/**
	 * @return string
	 */
	protected function getRoot() {
		return $this->oRootPath->getRootDir();
	}

	/**
	 * @param string $sModuleSlug
	 * @return string
	 */
	public function getPath_Config( string $sModuleSlug ) :string {
		return $this->getFullPath( 'config/'.$sModuleSlug );
	}

	/**
	 * @param string $sFlag
	 * @return string
	 */
	public function getPath_Flags( $sFlag = '' ) {
		return $this->getFullPath( $this->pathJoin( 'flags', $sFlag ) );
	}

	/**
	 * @return string
	 */
	public function getPath_Languages() {
		return $this->getFullPath( 'languages' );
	}

	/**
	 * @param string $sAsset
	 * @return string
	 */
	public function getPluginUrl_Asset( $sAsset ) {
		$sUrl = '';
		$sAssetPath = $this->getPath_Assets( $sAsset );
		if ( Services::WpFs()->exists( $sAssetPath ) ) {
			$sUrl = Services::Includes()->addIncludeModifiedParam(
				$this->oRootPath->getPluginUrl( $this->urlJoin( 'assets', $sAsset ) ),
				$sAssetPath
			);
		}
		return $sUrl;
	}

	/**
	 * @param string $sAsset
	 * @return string
	 */
	public function getPluginUrl_Css( $sAsset ) {
		return $this->getPluginUrl_Asset( $this->urlJoin( 'css', $sAsset ) );
	}

	/**
	 * @param string $sAsset
	 * @return string
	 */
	public function getPluginUrl_Image( $sAsset ) {
		return $this->getPluginUrl_Asset( $this->urlJoin( 'images', $sAsset ) );
	}

	/**
	 * @param string $sAsset
	 * @return string
	 */
	public function getPluginUrl_Js( $sAsset ) {
		return $this->getPluginUrl_Asset( $this->urlJoin( 'js', $sAsset ) );
	}
}