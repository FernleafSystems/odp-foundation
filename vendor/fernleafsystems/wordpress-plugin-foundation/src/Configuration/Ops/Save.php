<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Configuration\Ops;

use FernleafSystems\Wordpress\Plugin\Foundation\Configuration\Module\Config;
use FernleafSystems\Wordpress\Services\Utilities\Options\Transient;

class Save {

	/**
	 * @param Config $oConfig
	 * @param string $sOptionKey
	 * @return bool
	 */
	static public function ToWp( Config $oConfig, string $sOptionKey ) :bool {
		return empty( $oConfig->needs_save )
			   || Transient::Set( $sOptionKey, $oConfig->getRawDataAsArray() );
	}
}