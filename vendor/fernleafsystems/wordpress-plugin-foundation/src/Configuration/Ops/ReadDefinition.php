<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Configuration\Ops;

use FernleafSystems\Wordpress\Services\Utilities\Options\Transient;

class ReadDefinition {

	/**
	 * @var string
	 */
	private $path;

	/**
	 * @var string
	 */
	private $cache_key;

	public function __construct( string $sPath, ?string $sKey = null ) {
		$this->path = $sPath;
		$this->cache_key = $sKey;
	}

	/**
	 * @return array
	 * @throws \Exception
	 */
	public function read() :array {
		$aDef = $this->fromWp();
		if ( empty( $aDef ) || !is_array( $aDef ) ) {
			$aDef = $this->fromFile();
		}
		return $aDef;
	}

	/**
	 * @return array
	 * @throws \Exception
	 */
	public function fromFile() :array {
		return Read::FromFile( $this->path );
	}

	/**
	 * @return array|null
	 */
	public function fromWp() {
		return Transient::Get( $this->cache_key );
	}
}