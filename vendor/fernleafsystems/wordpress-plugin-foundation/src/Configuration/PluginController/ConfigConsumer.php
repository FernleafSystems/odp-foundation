<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Configuration\PluginController;

trait ConfigConsumer {

	/**
	 * @var Config
	 */
	private $oPluginConfig;

	public function getConfig() :Config {
		return $this->oPluginConfig;
	}

	/**
	 * @return $this
	 */
	public function setConfig( Config $oConfig ) {
		$this->oPluginConfig = $oConfig;
		return $this;
	}
}