<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Options\Operations;

use FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Traits\ModConsumer;
use FernleafSystems\Wordpress\Services\Services;

class BuildForUI {

	use ModConsumer;
	/**
	 * @var string
	 */
	const CollateSeparator = '--SEP--';

	/**
	 * @return string
	 */
	public function collateAllFormInputsForAllOptions() {
		$aOptions = $this->build();

		$aToJoin = [];
		foreach ( $aOptions as $aOptionsSection ) {

			if ( empty( $aOptionsSection ) ) {
				continue;
			}
			foreach ( $aOptionsSection[ 'options' ] as $aOption ) {
				$aToJoin[] = $aOption[ 'type' ].':'.$aOption[ 'key' ];
			}
		}
		return implode( self::CollateSeparator, $aToJoin );
	}

	/**
	 * Will initiate the plugin options structure for use by the UI builder.
	 * It doesn't set any values, just populates the array created in buildOptions()
	 * with values stored.
	 * It has to handle the conversion of stored values to data to be displayed to the user.
	 * @return array
	 */
	public function build() {
		$oOpts = $this->getOptions();
		$oConfig = $oOpts->config();
		$strings = $this->getMod()->getStrings();

		$aOptions = $oOpts->getLegacyOptionsConfigData();
		foreach ( $aOptions as $nSectionKey => $aOptionsSection ) {

			if ( empty( $aOptionsSection ) || !isset( $aOptionsSection[ 'options' ] ) ) {
				continue;
			}

			// Ensure certain keys are always set for the UI.
			$aOptionsSection = Services::DataManipulation()->mergeArraysRecursive(
				[
					'help_video' => []
				],
				$aOptionsSection
			);

			foreach ( $aOptionsSection[ 'options' ] as $nKey => $aOption ) {

				$sOptionKey = $aOption[ 'key' ];
				$sOptionType = $aOption[ 'type' ];
				$aOption[ 'is_value_default' ] = !isset( $aOption[ 'value' ] ) || ( $aOption[ 'value' ] === $aOption[ 'default' ] );

				if ( $oOpts->getOpt( $sOptionKey ) === false ) {
					$oOpts->resetOptToDefault( $sOptionKey );
				}
				$mCurrent = $oOpts->getOpt( $sOptionKey );

				if ( $sOptionType == 'password' && !empty( $mCurrent ) ) {
					$mCurrent = '';
				}
				else if ( $sOptionType == 'array' ) {

					if ( empty( $mCurrent ) || !is_array( $mCurrent ) ) {
						$mCurrent = '';
					}
					else {
						$mCurrent = implode( "\n", $mCurrent );
					}
					$aOption[ 'rows' ] = substr_count( $mCurrent, "\n" ) + 2;
				}
				else if ( $sOptionType == 'yubikey_unique_keys' ) {

					if ( empty( $mCurrent ) ) {
						$mCurrent = '';
					}
					else {
						$aDisplay = [];
						foreach ( $mCurrent as $aParts ) {
							$aDisplay[] = key( $aParts ).', '.reset( $aParts );
						}
						$mCurrent = implode( "\n", $aDisplay );
					}
					$aOption[ 'rows' ] = substr_count( $mCurrent, "\n" ) + 1;
				}
				else if ( $sOptionType == 'comma_separated_lists' ) {

					if ( empty( $mCurrent ) ) {
						$mCurrent = '';
					}
					else {
						$aNewValues = [];
						foreach ( $mCurrent as $sPage => $aParams ) {
							$aNewValues[] = $sPage.', '.implode( ", ", $aParams );
						}
						$mCurrent = implode( "\n", $aNewValues );
					}
					$aOption[ 'rows' ] = substr_count( $mCurrent, "\n" ) + 1;
				}
				else if ( $sOptionType == 'multiple_select' ) {
					if ( !is_array( $mCurrent ) ) {
						$mCurrent = [];
					}
				}

				if ( $sOptionType == 'text' ) {
					$mCurrent = stripslashes( $mCurrent );
				}

				$aOption[ 'value' ] = is_scalar( $mCurrent ) ? esc_attr( $mCurrent ) : $mCurrent;
				$aOption[ 'disabled' ] = false;// TODO !$this->isPremium() && ( isset( $aOptParams[ 'premium' ] ) && $aOptParams[ 'premium' ] );
				$aOption[ 'enabled' ] = !$aOption[ 'disabled' ];

				// Build strings and add
				$aOptionsSection[ 'options' ][ $nKey ] = array_merge( $aOption, $strings->getOptionsDef( $aOption[ 'key' ] ) );
			}

			$aWarnings = [];
			if ( !$oConfig->isSectionReqsMet( $aOptionsSection[ 'slug' ] ) ) {
				$aWarnings[] = 'Unfortunately your PHP version is too low to support this feature.';
			}
			$aOptionsSection[ 'warnings' ] = $aWarnings;

			$aOptions[ $nSectionKey ] = array_merge( $aOptionsSection, $strings->getSectionDef( $aOptionsSection[ 'slug' ] ) );
		}
		return $aOptions;
	}
}