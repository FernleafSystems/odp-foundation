<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Options\Operations;

use FernleafSystems\Wordpress\Services\Services;

class Load {

	/**
	 * @param string $sStorageKey
	 * @return array
	 */
	public function valuesFromWp( $sStorageKey ) {
		$aOptionsValues = Services::WpGeneral()->getOption( $sStorageKey, [] );
		if ( !is_array( $aOptionsValues ) ) {
			$aOptionsValues = [];
		}
		return $aOptionsValues;
	}
}