<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Options\Operations;

use FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Options;

class Export {

	/**
	 * @param Options $oOptions
	 * @return array
	 */
	public function toArray( $oOptions ) {
		return $oOptions->getTransferableOptions();
	}
}