<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Options\Operations;

use FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Options;
use FernleafSystems\Wordpress\Services\Services;

class Delete {

	/**
	 * @param Options $oOptions
	 * @param string  $sStorageKey
	 * @return bool
	 */
	public function execute( $oOptions, $sStorageKey ) {
		$oOptions->config()->cleanTransientStorage();
		return Services::WpGeneral()->deleteOption( $sStorageKey );
	}
}