<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Options\Operations;

use FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Options;
use FernleafSystems\Wordpress\Services\Services;

class Save {

	/**
	 * @var boolean
	 */
	protected $bDeletePreSave;

	/**
	 * @param Options $oOptions
	 * @param string  $sStorageKey
	 * @return bool
	 */
	public function execute( $oOptions, $sStorageKey ) {
		$bSuccess = true;
		$oOptions->cleanOptions();
		if ( $oOptions->getNeedSave() ) {
			$oOptions->setNeedSave( false );
			if ( $this->getDeletePreSave() ) {
				Services::WpGeneral()->deleteOption( $sStorageKey );
			}
			$bSuccess = Services::WpGeneral()->updateOption( $sStorageKey, $oOptions->getAllOptionsValues() );
		}
		return $bSuccess;
	}

	/**
	 * @param bool $bDeletePreSave
	 * @return $this
	 */
	public function setDeletePreSave( $bDeletePreSave = true ) {
		$this->bDeletePreSave = (bool)$bDeletePreSave;
		return $this;
	}

	/**
	 * @return bool
	 */
	public function getDeletePreSave() {
		return (bool)$this->bDeletePreSave;
	}
}