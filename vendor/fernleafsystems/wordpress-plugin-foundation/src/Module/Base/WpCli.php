<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base;

use FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\WpCli\ModuleStandard;
use FernleafSystems\Wordpress\Services\Services;

class WpCli extends Common\ExecOnceModConsumer {

	protected function canRun() :bool {
		return Services::WpGeneral()->isWpCli()
			   && $this->getCfg()[ 'enabled' ]
			   && !empty( $this->getAllCmdHandlers() );
	}

	protected function run() {
		try {
			array_map(
				fn( $handlerClass ) => ( new $handlerClass() )
					->setMod( $this->getMod() )
					->execute(),
				$this->getAllCmdHandlers()
			);
		}
		catch ( \Exception $e ) {
		}
	}

	/**
	 * @return string[]
	 */
	protected function getAllCmdHandlers() :array {
		$handlers = $this->enumCmdHandlers();
		if ( $this->getCfg()[ 'inc_mod_standard' ] ) {
			$handlers[] = ModuleStandard::class;
		}
		return $handlers;
	}

	/**
	 * @return string[] - FQ class names
	 */
	protected function enumCmdHandlers() :array {
		return [];
	}

	public function getCfg() :array {
		$modCfg = $this->getMod()->getModuleConfig();
		return array_merge(
			[
				'enabled'          => false,
				'cmd_root'         => $this->getCon()->getPrefix()->getPluginPrefix(),
				'cmd_base'         => $modCfg->getModuleSlug(),
				'inc_mod_standard' => false,
			],
			$modCfg->getRawData_FullFeatureConfig()[ 'wpcli' ] ?? []
		);
	}
}