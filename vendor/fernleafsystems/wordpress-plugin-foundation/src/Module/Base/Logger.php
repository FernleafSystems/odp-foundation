<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base;

use FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Logging\LogFileDirCreate;
use Monolog\Formatter\JsonFormatter;
use Monolog\Handler\RotatingFileHandler;

class Logger {

	use Traits\ModConsumer;

	protected ?\Monolog\Logger $logger;

	public function getLogger() :\Monolog\Logger {
		if ( empty( $this->logger ) ) {
			$this->logger = new \Monolog\Logger( $this->getCon()->getPrefix()->getPluginPrefix() );

			foreach ( $this->getHandlers() as $handler ) {
				$this->logger->pushHandler( $handler );
			}
			foreach ( $this->getProcessors() as $processor ) {
				$this->logger->pushProcessor( $processor );
			}
		}
		return $this->logger;
	}

	protected function canLogToDefaultFile() :bool {
		$can = true;
		try {
			$this->getLogFilePath();
		}
		catch ( \Exception $e ) {
			$can = false;
			error_log( $e->getMessage() );
		}
		return $can;
	}

	protected function getDefaultFileHandler() :?RotatingFileHandler {
		try {
			$fileHandler = new RotatingFileHandler( $this->getLogFilePath() );
			if ( $this->isJson() ) {
				$fileHandler->setFormatter( new JsonFormatter() );
			}
		}
		catch ( \Exception $e ) {
			$fileHandler = null;
		}
		return $fileHandler;
	}

	protected function getHandlers() :array {
		$handlers = [];
		if ( $this->canLogToDefaultFile() ) {
			$handler = $this->getDefaultFileHandler();
			if ( !empty( $handler ) ) {
				$handlers[] = $handler;
			}
		}
		return $handlers;
	}

	protected function getProcessors() :array {
		return [];
	}

	/**
	 * @throws \Exception
	 */
	protected function getLogFilePath() :string {
		$dir = ( new LogFileDirCreate() )
			->setMod( $this->getMod() )
			->run();
		return path_join( $dir, sprintf( '%s.log', $this->getMod()->getModSlug( false ) ) );
	}

	protected function isJson() :bool {
		return true;
	}
}