<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Common;

use FernleafSystems\Utilities\Logic\ExecOnce;
use FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Traits\ModConsumer;

class ExecOnceModConsumer {

	use ModConsumer;
	use ExecOnce;
}