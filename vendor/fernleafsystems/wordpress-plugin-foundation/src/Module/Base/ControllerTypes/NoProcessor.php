<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\ControllerTypes;

use FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Controller;

class NoProcessor extends Controller {

	/**
	 * @return null
	 */
	public function getProcessor() {
		return null;
	}

	/**
	 * Do Nothing - It's Processor-less
	 */
	public function doExecuteProcessor() {
	}
}