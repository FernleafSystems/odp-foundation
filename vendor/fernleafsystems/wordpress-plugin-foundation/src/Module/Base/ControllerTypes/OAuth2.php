<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\ControllerTypes;

use FernleafSystems\Wordpress\Services\Services;

/**
 * Modules that use this controller must have the following options specified in the config:
 * oauth2_client_id
 * oauth2_client_secret
 * oauth2_authenticated_at
 * oauth2_authorization_code
 * oauth2_authorization_code_redirect
 * oauth2_authorization_code_stage_started_at
 * oauth2_accesstoken
 * oauth2_accesstoken_expires_at
 * oauth2_refresh_token
 * Class OAuth2
 * @package FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\ControllerTypes
 */
trait OAuth2 {

	protected function initOauth() {
		add_action( 'wp_loaded', [ $this, 'processOAuth2Stages' ] );
	}

	protected function handleOauth2OptionsSubmit() {
		// This will automatically flag to redirect to OAuth2 Authorization
		if ( $this->getIsMainFeatureEnabled() && $this->getOAuth2Stage() == 'preauth' ) {
			$this->setOAuth2AuthorizationCodeRedirect();
		}
	}

	/**
	 * Fires all the jump off points for the OAuth2 Auth, Tokens etc.
	 */
	public function processOAuth2Stages() {

		if ( $this->getOAuth2Stage() == 'preclient' ) {
		}
		if ( $this->getOAuth2Stage() == 'preauth' ) {
			$this->stagePreAuth();
		}
		if ( $this->getOAuth2Stage() == 'pretoken' ) {
			$this->stagePreToken();
		}
		if ( $this->getOAuth2Stage() == 'expiredtoken' ) {
			$this->stageExpiredToken();
		}
		if ( $this->getOAuth2Stage() == 'ready' ) {
			// further init
		}
	}

	/**
	 * Stage: Pre-Auth.
	 * No successful authentication or authorization has been made with the API. We must send
	 * an Authorization Code request, or if we have already, look out for the redirect with the code.
	 */
	protected function stagePreAuth() {
		$this->setOAuth2Token(); // we need to clear this since we're expecting to create a whole new one at this stage.

		if ( $this->isOAuth2AuthorizationCodeRedirect() ) {
			$this->doOAuthRedirect();
		}
		elseif ( !$this->isOAuth2StageAuthCodeExpired() ) {
			$this->captureOAuthCode();
		}
		else {
			//TODO: Notice to say it has expired.
		}
	}

	/**
	 * Stage: Pre-Token.
	 * We have attained a valid authorization code, and now we need to request an
	 * Access Token with which we will be able to perform authenticated API requests.
	 */
	protected function stagePreToken() {

		try {
			$aReqData = [
				'code'          => $this->getOAuth2AuthorizationCode(),
				'client_id'     => $this->getOAuth2ClientId(),
				'client_secret' => $this->getOAuth2ClientSecret(),
				'redirect_uri'  => $this->getFeatureAdminPageUrl()
			];

			$oAccessToken = $this->getOAuth2Provider()
								 ->getAccessToken( 'authorization_code', $aReqData );
			$this->storeAccessToken( $oAccessToken );
		}
		catch ( \Exception $oe ) { //IdentityProviderException
		}
	}

	/**
	 * Stage: Expired-Token.
	 * We're already authenticated with the API provider but our temporary Access Token has
	 * expired. We must send a request for an updated token and store the results.
	 */
	protected function stageExpiredToken() {

		try {
			$aReqData = [
				'client_id'     => $this->getOAuth2ClientId(),
				'client_secret' => $this->getOAuth2ClientSecret(),
				'refresh_token' => $this->getOAuth2RefreshToken()
			];
			$oAccessToken = $this->getOAuth2Provider()
								 ->getAccessToken( 'refresh_token', $aReqData );
			$this->storeAccessToken( $oAccessToken );
		}
		catch ( \Exception $oe ) { //IdentityProviderException
		}
	}

	/**
	 * @param \League\OAuth2\Client\Token\AccessToken $oAccessToken
	 * @return $this
	 */
	protected function storeAccessToken( $oAccessToken ) {
		$this->setOAuth2Token( $oAccessToken->getToken() )
			 ->setOAuth2TokenExpiresAt( $oAccessToken->getExpires() );
		$sRefresh = $oAccessToken->getRefreshToken();
		if ( !empty( $sRefresh ) ) {
			$this->setOAuth2RefreshToken( $oAccessToken->getRefreshToken() );
		}
		return $this;
	}

	protected function doOAuthRedirect() {
		$this->setOAuth2AuthorizationCodeRedirect( false )
			 ->setOAuth2AuthorizationCodeStartedAt( Services::Request()->ts() );
		Services::Response()->redirect( $this->getOAuth2UrlRedirect(), [], false, false );
	}

	protected function captureOAuthCode() {
		$code = Services::Request()->query( 'code' );
		if ( preg_match( '#^[-_a-z0-9]{41}$#i', $code ) ) {
			$this->setOAuth2AuthorizationCodeStartedAt()
				 ->setOAuth2AuthorizationCode( $code );
		}
	}

	/**
	 * \League\OAuth2\Client\Provider\AbstractProvider
	 * @return mixed
	 */
	abstract protected function getOAuth2Provider();

	/**
	 * @return string
	 */
	protected function getOAuth2UrlRedirect() {
		$sAuthUrl = $this->getOAuth2Provider()
						 ->getBaseAuthorizationUrl();

		return add_query_arg(
			[
				'redirect_uri'  => $this->getFeatureAdminPageUrl(),
				'client_id'     => $this->getOAuth2ClientId(),
				'response_type' => 'code'
			],
			$sAuthUrl
		);
	}

	protected function resetOauth2() {
		$this->setOAuth2AuthorizationCode( '' )
			 ->setOAuth2AuthorizationCodeStartedAt()
			 ->setOAuth2Token()
			 ->setOAuth2TokenExpiresAt( 0 )
			 ->setOAuth2RefreshToken();
	}

	/**
	 * @return string
	 */
	public function getOAuth2ClientId() {
		return $this->getOpt( 'oauth2_client_id' );
	}

	/**
	 * @return string
	 */
	public function getOAuth2ClientSecret() {
		return $this->getOpt( 'oauth2_client_secret' );
	}

	/**
	 * @return int >= 0
	 */
	public function getOAuth2AuthenticatedAt() {
		return $this->getOpt( 'oauth2_authenticated_at' );
	}

	/**
	 * @return string
	 */
	public function getOAuth2AuthorizationCode() {
		return $this->getOpt( 'oauth2_authorization_code' );
	}

	/**
	 * @return string
	 */
	public function getOAuth2RefreshToken() {
		return $this->getOpt( 'oauth2_refresh_token' );
	}

	/**
	 * @return string
	 */
	public function getOAuth2Stage() {

		if ( !Services::WpGeneral()->isAjax() ) {

			if ( !$this->isOAuth2ClientReady() ) {
				return 'preclient';
			}
			elseif ( !$this->hasOAuth2AuthorizationCode() ) {
				return 'preauth';
			}
			elseif ( !$this->hasOAuth2AccessToken() ) {
				return 'pretoken';
			}
			elseif ( !$this->isOAuth2TokenValid() ) {
				if ( $this->hasOAuth2RefreshToken() ) {
					return 'expiredtoken';
				}
				else {
					return 'preauth'; // this should ideally never happen.
				}
			}
		}
		return 'ready';
	}

	/**
	 * @return string
	 */
	public function getOAuth2AccessToken() {
		return $this->getOpt( 'oauth2_accesstoken', '' );
	}

	/**
	 * @return int
	 */
	public function getOAuth2TokenExpiresAt() {
		return (int)$this->getOpt( 'oauth2_accesstoken_expires_at', 0 );
	}

	/**
	 * @return bool
	 */
	public function hasClientId() {
		return $this->verifyString( $this->getOAuth2ClientId() );
	}

	/**
	 * @return bool
	 */
	public function hasClientSecret() {
		return $this->verifyString( $this->getOAuth2ClientSecret() );
	}

	/**
	 * @return bool
	 */
	public function hasOAuth2AuthorizationCode() {
		return $this->verifyString( $this->getOAuth2AuthorizationCode() );
	}

	/**
	 * @return bool
	 */
	public function hasOAuth2RefreshToken() {
		return $this->verifyString( $this->getOAuth2RefreshToken() );
	}

	/**
	 * @return bool
	 */
	public function hasOAuth2AccessToken() {
		return $this->verifyString( $this->getOAuth2AccessToken() );
	}

	/**
	 * @return bool
	 */
	public function isOAuth2Authenticated() {
		return $this->getOAuth2AuthenticatedAt() > 0;
	}

	/**
	 * @return bool
	 */
	public function isOAuth2TokenExpired() {
		return $this->getOAuth2TokenExpiresAt() < Services::Request()->ts();
	}

	/**
	 * @return bool
	 */
	public function isOAuth2TokenValid() {
		return $this->hasOAuth2AccessToken() && !$this->isOAuth2TokenExpired();
	}

	/**
	 * @return bool
	 */
	public function isOAuth2StageAuthCodeExpired() {
		$nStartedAt = (int)$this->getOpt( 'oauth2_authorization_code_stage_started_at' );
		return ( Services::Request()->ts() - $nStartedAt ) > MINUTE_IN_SECONDS;
	}

	/**
	 * @return bool
	 */
	public function isOAuth2ClientReady() {
		return $this->hasClientId() && $this->hasClientSecret();
	}

	/**
	 * @return bool
	 */
	public function isOAuth2AuthorizationCodeRedirect() {
		return $this->getOptIs( 'oauth2_authorization_code_redirect', true, true );
	}

	/**
	 * @param string $sCode
	 * @return $this
	 */
	public function setOAuth2AuthorizationCode( $sCode ) {
		$this->setOpt( 'oauth2_authorization_code', $sCode );
		return $this;
	}

	/**
	 * @param bool $bSet
	 * @return $this
	 */
	public function setOAuth2AuthorizationCodeRedirect( $bSet = true ) {
		$this->setOpt( 'oauth2_authorization_code_redirect', (bool)$bSet );
		return $this;
	}

	/**
	 * @param string $sToken
	 * @return $this
	 */
	public function setOAuth2RefreshToken( $sToken = '' ) {
		$this->setOpt( 'oauth2_refresh_token', $sToken );
		return $this;
	}

	/**
	 * @param string $sToken
	 * @return $this
	 */
	public function setOAuth2Token( $sToken = '' ) {
		$this->setOpt( 'oauth2_accesstoken', $sToken );
		return $this;
	}

	/**
	 * @param int $nExpiresAt
	 * @return $this
	 */
	public function setOAuth2TokenExpiresAt( $nExpiresAt ) {
		$this->setOpt( 'oauth2_accesstoken_expires_at', $nExpiresAt );
		return $this;
	}

	/**
	 * @param int $nStartedAt
	 * @return $this
	 */
	public function setOAuth2AuthorizationCodeStartedAt( $nStartedAt = 0 ) {
		$this->setOpt( 'oauth2_authorization_code_stage_started_at', (int)$nStartedAt );
		return $this;
	}

	/**
	 * @param string $sStringToVerify
	 * @return bool
	 */
	private function verifyString( $sStringToVerify ) {
		return is_string( $sStringToVerify ) && strlen( $sStringToVerify ) > 0;
	}
}