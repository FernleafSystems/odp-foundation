<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Rest\Request;

class RequestVO extends \FernleafSystems\Wordpress\Plugin\Core\Rest\Request\RequestVO {

	use \FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Traits\ModConsumer;
}