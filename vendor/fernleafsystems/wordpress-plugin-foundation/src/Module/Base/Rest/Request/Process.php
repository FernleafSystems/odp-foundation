<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Rest\Request;

use FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Traits\ModConsumer;

abstract class Process extends \FernleafSystems\Wordpress\Plugin\Core\Rest\Request\Process {

	use ModConsumer;

	/**
	 * @return RequestVO|mixed
	 */
	protected function getRequestVO() {
		/** @var RequestVO $req */
		$req = parent::getRequestVO();
		return $req->setMod( $this->getMod() );
	}

	/**
	 * @return RequestVO
	 */
	protected function newReqVO() {
		return new RequestVO();
	}
}
