<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Rest\Route;

use FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Rest\Request\Process;

abstract class RouteBase extends \FernleafSystems\Wordpress\Plugin\Core\Rest\Route\RouteBase {

	use \FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Traits\ModConsumer;

	protected function getNamespace() :string {
		return $this->getCon()->getPrefix()->getPluginPrefix();
	}

	protected function getRequestProcessor() {
		/** @var Process $proc */
		$proc = parent::getRequestProcessor();
		return $proc->setMod( $this->getMod() );
	}
}