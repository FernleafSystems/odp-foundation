<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Rest\Route;

class RouteCache extends \FernleafSystems\Wordpress\Plugin\Core\Rest\Route\RouteCache {

	use \FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Traits\ModConsumer;
}