<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\WpCli;

class ModuleStandard extends BaseWpCliCmd {

	/**
	 * @throws \Exception
	 */
	protected function addCmds() {
		\WP_CLI::add_command(
			$this->buildCmd( [ 'opt-list' ] ),
			[ $this, 'cmdOptList' ], $this->mergeCommonCmdArgs( [
			'shortdesc' => 'List the option keys and their names.',
			'longdesc'  => 'List the option keys and their names.',
			'synopsis'  => [
				[
					'type'        => 'assoc',
					'name'        => 'format',
					'optional'    => true,
					'options'     => [
						'table',
						'json',
						'yaml',
						'csv',
					],
					'default'     => 'table',
					'description' => 'Display all the option details.',
				],
				[
					'type'        => 'flag',
					'name'        => 'full',
					'optional'    => true,
					'description' => 'Display all the option details.',
				],
			],
		] ) );

		\WP_CLI::add_command(
			$this->buildCmd( [ 'opt-get' ] ),
			[ $this, 'cmdOptGet' ], $this->mergeCommonCmdArgs( [
			'shortdesc' => 'Get status of an option.',
			'longdesc'  => 'Get status of an option.',
			'synopsis'  => [
				[
					'type'        => 'assoc',
					'name'        => 'key',
					'optional'    => false,
					'options'     => $this->getOptions()->getOptionsForWpCli(),
					'description' => 'The option key to get.',
				],
			],
		] ) );

		\WP_CLI::add_command(
			$this->buildCmd( [ 'opt-set' ] ),
			[ $this, 'cmdOptSet' ], $this->mergeCommonCmdArgs( [
			'shortdesc' => 'Set status/value of an option.',
			'longdesc'  => 'Set status/value of an option.',
			'synopsis'  => [
				[
					'type'        => 'assoc',
					'name'        => 'key',
					'optional'    => false,
					'options'     => $this->getOptions()->getOptionsForWpCli(),
					'description' => 'The option key to updateModuleStandard.php
					.',
				],
				[
					'type'        => 'assoc',
					'name'        => 'value',
					'optional'    => false,
					'description' => "The option's new value.",
				],
			],
		] ) );

		\WP_CLI::add_command(
			$this->buildCmd( [ 'module' ] ),
			[ $this, 'cmdModAction' ], $this->mergeCommonCmdArgs( [
			'shortdesc' => 'Enable, disable, or query the status of a module.',
			'longdesc'  => 'Enable, disable, or query the status of a module.',
			'synopsis'  => [
				[
					'type'        => 'assoc',
					'name'        => 'action',
					'optional'    => false,
					'options'     => [
						'status',
						'enable',
						'disable',
					],
					'description' => 'The action to perform on the module.',
				],
			],
		] ) );
	}

	public function cmdModAction( $null, $args ) {
		$mod = $this->getMod();

		switch ( $args[ 'action' ] ) {

			case 'status':
				$mod->getIsMainFeatureEnabled() ?
					\WP_CLI::log( 'Module is currently enabled.' )
					: \WP_CLI::log( 'Module is currently disabled.' );
				break;

			case 'enable':
				$mod->setIsMainFeatureEnabled( true );
				$mod->savePluginOptions();
				\WP_CLI::success( 'Module enabled.' );
				break;

			case 'disable':
				$mod->setIsMainFeatureEnabled( false );
				$mod->savePluginOptions();
				\WP_CLI::success( 'Module disabled.' );
				break;
		}
	}

	public function cmdOptGet( array $null, array $args ) {
		$opts = $this->getOptions();

		$value = $opts->getOpt( $args[ 'key' ], $null );
		$optDef = $this->getMod()->getModuleConfig()->getOptDefinition( $args[ 'key' ] );
		if ( !is_numeric( $value ) && empty( $value ) ) {
			\WP_CLI::log( __( 'No value set.', 'wp-simple-firewall' ) );
		}
		else {
			$explain = '';

			if ( is_array( $value ) ) {
				$value = sprintf( '[ %s ]', implode( ', ', $value ) );
			}

			if ( $optDef[ 'type' ] === 'checkbox' ) {
				$explain = sprintf( 'Note: %s', __( '"Y" = Turned On; "N" = Turned Off' ) );
			}

			\WP_CLI::log( sprintf( __( 'Current value: %s', 'wp-simple-firewall' ), $value ) );
			if ( !empty( $explain ) ) {
				\WP_CLI::log( $explain );
			}
		}
	}

	public function cmdOptSet( array $null, array $args ) {
		$this->getOptions()->setOpt( $args[ 'key' ], $args[ 'value' ] );
		\WP_CLI::success( 'Option updated.' );
	}

	public function cmdOptList( array $null, array $args ) {
		$opts = $this->getOptions();
		$modCfg = $this->getMod()->getModuleConfig();
		$strings = $this->getMod()->getStrings();
		$optsList = [];
		foreach ( $opts->getOptionsForWpCli() as $key ) {
			try {
				$optsList[] = [
					'key'     => $key,
					'name'    => $strings->getOptionsDef( $key )[ 'name' ],
					'type'    => $modCfg->getOptionType( $key ),
					'current' => $opts->getOpt( $key ),
					'default' => $opts->getOptDefault( $key ),
				];
			}
			catch ( \Exception $e ) {
			}
		}

		if ( empty( $optsList ) ) {
			\WP_CLI::log( "This module doesn't have any configurable options." );
		}
		else {
			if ( !\WP_CLI\Utils\get_flag_value( $args, 'full', false ) ) {
				$keys = [
					'key',
					'name',
					'current'
				];
			}
			else {
				$keys = array_keys( $opts[ 0 ] );
			}

			\WP_CLI\Utils\format_items(
				$args[ 'format' ],
				$optsList,
				$keys
			);
		}
	}
}