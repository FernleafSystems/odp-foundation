<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base;

use FernleafSystems\Wordpress\Plugin\Core;
use FernleafSystems\Wordpress\Plugin\Foundation;
use FernleafSystems\Wordpress\Plugin\Foundation\Module\Base;
use FernleafSystems\Wordpress\Plugin\Foundation\Module\Options\Operations;
use FernleafSystems\Wordpress\Services\Services;

abstract class Controller {

	use Base\Traits\ModuleConfigConsumer;
	use Foundation\Control\PluginControllerConsumer;

	/**
	 * @var Logger
	 */
	private $logger;

	/**
	 * @var WpCli
	 */
	private $wpcli;

	/**
	 * @var Options
	 */
	private $oOptions;

	/**
	 * @var Notifications
	 */
	protected $oNotificationsHandler;

	/**
	 * @var Request
	 */
	protected $oRequestHandler;

	/**
	 * @var Processor
	 */
	protected $oProcessor;

	/**
	 * @var string
	 */
	protected static $sActivelyDisplayedModuleOptions = '';

	/**
	 * @var Core\Databases\Base\Handler[]
	 */
	private $dbHandlers = [];

	/**
	 * @param Foundation\Control\Controller          $oPluginController
	 * @param Foundation\Configuration\Module\Config $oModConfig
	 * @throws \Exception
	 */
	public function __construct( $oPluginController, $oModConfig ) {
		if ( empty( $oPluginController ) ) {
			throw new \Exception( 'Plugin Controller must be provided' );
		}
		$this->setCon( $oPluginController );
		$this->setModuleConfig( $oModConfig );
		$this->postConstructInit();
	}

	/**
	 * @return $this
	 */
	public function postConstructInit() {
		if ( $this->verifyModuleMeetRequirements() ) {
			$this->setupHooks();
			$this->doPostConstruction();
		}
		return $this;
	}

	protected function verifyModuleMeetRequirements() :bool {
		$meetReqs = true;

		$reqsPHP = $this->config()->getRequirement( 'php' );
		if ( !empty( $reqsPHP ) ) {

			if ( !empty( $reqsPHP[ 'version' ] ) ) {
				$meetReqs = $meetReqs && Services::Data()->getPhpVersionIsAtLeast( $reqsPHP[ 'version' ] );
			}
			if ( !empty( $reqsPHP[ 'functions' ] ) && is_array( $reqsPHP[ 'functions' ] ) ) {
				foreach ( $reqsPHP[ 'functions' ] as $fun ) {
					$meetReqs = $meetReqs && function_exists( $fun );
				}
			}
			if ( !empty( $reqsPHP[ 'constants' ] ) && is_array( $reqsPHP[ 'constants' ] ) ) {
				foreach ( $reqsPHP[ 'constants' ] as $const ) {
					$meetReqs = $meetReqs && defined( $const );
				}
			}
		}

		return $meetReqs;
	}

	protected function setupHooks() {

		$nLoadPriority = $this->config()->getLoadPriority();
		add_action( $this->prefix( 'run_processors' ), [ $this, 'onRunProcessors' ], $nLoadPriority );

		add_action( 'init', [ $this, 'onWpInit' ], 1 );
		add_action( 'wp_loaded', [ $this, 'onWpLoaded' ], 1 );
		add_action( $this->prefix( 'authenticated_form_submit' ), [ $this, 'handleFormSubmit' ] );
		add_action( $this->prefix( 'authenticated_ajax_request' ), [ $this, 'handleFormSubmitAjax' ] );
		add_action( $this->prefix( 'no_nonce_query_action' ), [ $this, 'handleNoNonceQueryAction' ] );
		add_action( $this->prefix( 'query_action' ), [ $this, 'handleQueryAction' ] );
		add_action( $this->prefix( 'authenticated_query_action' ), [ $this, 'handleQueryActionAuthenticated' ] );
		add_filter( $this->prefix( 'filter_plugin_submenu_items' ), [ $this, 'filter_addPluginSubMenuItem' ] );
		add_filter( $this->prefix( 'get_feature_summary_data' ), [ $this, 'filter_getFeatureSummaryData' ] );
		add_action( $this->prefix( 'plugin_shutdown' ), [ $this, 'onPluginShutdown' ] );
		add_action( $this->prefix( 'delete_plugin' ), [ $this, 'onPluginDelete' ] );
		add_filter( $this->prefix( 'aggregate_all_plugin_options' ), [ $this, 'aggregateOptionsValues' ] );
		add_filter( $this->prefix( 'register_admin_notices' ), [ $this, 'fRegisterAdminNotices' ] );
		add_filter( $this->prefix( 'gather_options_for_export' ), [ $this, 'exportTransferableOptions' ] );

		add_action( $this->prefix( 'daily_cron' ), [ $this, 'runDailyCron' ] );
		add_action( $this->prefix( 'hourly_cron' ), [ $this, 'runHourlyCron' ] );

		$this->loadAjaxHandler();

		$this->initialiseModule(); // maybe move this inside IF below?
		$this->getRequestHandler(); // init.

		$this->setupCustomHooks();
	}

	protected function setupCustomHooks() {
	}

	protected function doPostConstruction() {
	}

	/**
	 * @return bool
	 */
	public function isModuleRequest() {
		return ( $this->getModSlug() == Services::Request()->request( 'mod_slug' ) );
	}

	/**
	 * Added to WordPress 'plugins_loaded' hook
	 */
	public function onRunProcessors() {

		if ( $this->isUpgrading() ) {
			$this->updateHandler();
		}

		if ( $this->config()->getProperty( 'auto_load_processor' ) ) {
			$this->getProcessor();
		}

		if ( $this->getIsMainFeatureEnabled()
			 && !$this->getCon()->getFlagsLookup()->getHasFlagForceOff()
			 && $this->isReadyToExecute()
		) {
			$hook = $this->config()->getProcessorActionHook();
			if ( empty( $hook ) || $hook == 'plugins_loaded' ) {
				$this->doExecuteProcessor();
			}
			else {
				add_action( $hook, [ $this, 'doExecuteProcessor' ] );
			}
		}
	}

	/**
	 * Used to effect certain processing that is to do with options etc. but isn't related to processing
	 * functionality of the plugin.
	 * @return bool
	 */
	protected function isReadyToExecute() {
		try {
			$ready = $this->getProcessor() instanceof Processor;
		}
		catch ( \Exception $e ) {
			$ready = false;
		}
		return $ready;
	}

	/**
	 * @return bool
	 */
	public function isUpgrading() {
		return false; // TODO
	}

	/**
	 * Should be over-ridden by each new class to handle upgrades.
	 * Called upon construction and after plugin options are initialized.
	 */
	protected function updateHandler() {
	}

	/**
	 * @param array $aAdminNotices
	 * @return array
	 */
	public function fRegisterAdminNotices( $aAdminNotices ) {
		if ( !is_array( $aAdminNotices ) ) {
			$aAdminNotices = [];
		}
		return array_merge( $aAdminNotices, $this->getAdminNotices() );
	}

	/**
	 * @return false|string
	 */
	public function getWorkingDir() {
		return ( new Base\Lib\Utility\WorkingDir() )
			->setMod( $this )
			->retrieve();
	}

	/**
	 * Used to initialize anything required before the Processor itself runs.
	 * E.g. initialize the database handler
	 */
	protected function initialiseModule() {
	}

	public function doExecuteProcessor() {
		try {
			$this->getProcessor()->run();
		}
		catch ( \Exception $e ) {
			if ( Services::WpGeneral()->isDebug() ) {
				error_log( $e->getMessage() );
			}
		}
	}

	/**
	 * A action added to WordPress 'init' hook
	 */
	public function onWpInit() {
		$this->loadAjaxHandler();
		$this->loadDebug();

		$aNotices = $this->config()->getAdminNotices();
		if ( count( $aNotices ) > 0 ) {
			$this->initNotificationsHandler();
		}

		add_action( 'cli_init', function () {
			$wpcli = $this->getWpCli();
			if ( !empty( $wpcli ) ) {
				$wpcli->execute();
			}
		} );
	}

	public function onWpLoaded() {
		if ( $this->config()->getProperty( 'rest_api' ) ) {
			add_action( 'rest_api_init', function () {
				try {
					$this->getRestHandler()->init();

					/** @var Rest $rest */
					$rest = $this->loadClass( 'Rest', true, true );
					if ( !empty( $rest ) ) {
						$restCfg = $this->getModuleConfig()->getDef( 'rest_api' );
						$rest->applyFromArray( is_array( $restCfg ) ? $restCfg : [] );
						$rest->init();
					}
				}
				catch ( \Exception $e ) {
				}
			} );
		}
	}

	/**
	 * @return Options
	 */
	protected function getCoreOptions() {
		return $this->getOptions()->getCoreOptions();
	}

	/**
	 * @return array
	 */
	public function getAdminNotices() {
		return $this->config()->getAdminNotices();
	}

	public function onPluginShutdown() {
		if ( \rand( 1, 40 ) === 2 ) {
			// cleanup databases randomly just in-case cron doesn't run.
			$this->cleanupDatabases();
		}
		$this->savePluginOptions();
	}

	/**
	 * @return string
	 */
	protected function getOptionsStorageKey() {
		return $this->prefixOptionKey( $this->config()->getStorageKey() );
	}

	/**
	 * @param string $dbKey
	 * @return Core\Databases\Base\Handler|mixed|null
	 */
	protected function loadDbH( string $dbKey ) :Core\Databases\Base\Handler {
		$dbh = $this->dbHandlers[ $dbKey ] ?? null;

		if ( !$dbh instanceof Core\Databases\Base\Handler ) {
			$dbClasses = $this->getDbHandlerClasses();
			if ( isset( $dbClasses[ $dbKey ] ) ) {
				$dbSpec = $this->config()->getDef( 'db_table_'.$dbKey );
				if ( !empty( $dbSpec ) ) {
					$dbSpec[ 'table_prefix' ] = $this->getCon()->getPrefix()->getPluginPrefix( '_' );
					/** @var Core\Databases\Base\Handler $dbh */
					$dbh = new $dbClasses[ $dbKey ]( $dbSpec );
					$dbh->execute();
				}
				$this->dbHandlers[ $dbKey ] = $dbh;
			}
		}

		return $dbh;
	}

	/**
	 * @param bool $initAll
	 * @return Core\Databases\Base\Handler[]
	 */
	protected function loadDbHandlers( bool $initAll = false ) :array {
		if ( $initAll ) {
			foreach ( $this->getDbHandlerClasses() as $slug => $dbHandlerClass ) {
				$this->loadDbH( $slug );
			}
		}
		return is_array( $this->dbHandlers ) ? $this->dbHandlers : [];
	}

	/**
	 * @return string[]
	 * @deprecated 0.8.0
	 */
	protected function getAllDbClasses() :array {
		$c = $this->config()->getDef( 'db_classes' );
		return is_array( $c ) ? $c : [];
	}

	/**
	 * @return string[]
	 */
	protected function getDbHandlerClasses() :array {
		$c = $this->config()->getDef( 'db_handler_classes' );
		return is_array( $c ) ? $c : [];
	}

	protected function cleanupDatabases() {
		foreach ( $this->loadDbHandlers( true ) as $dbh ) {
			try {
				if ( $dbh->isReady() ) {
					$dbh->autoCleanDb();
				}
			}
			catch ( \Exception $e ) {
			}
		}
	}

	/**
	 * @return Request
	 * @throws \Exception
	 */
	public function getRequestHandler() {
		if ( !isset( $this->oRequestHandler ) ) {
			$this->oRequestHandler = $this->loadClass( 'Request', true );
		}
		return $this->oRequestHandler;
	}

	protected function loadAjaxHandler() {
		if ( $this->isModuleRequest() && Services::WpGeneral()->isAjax() ) {
			$this->loadClass( 'AjaxHandler', true );
		}
	}

	/**
	 * @return RestHandler
	 * @throws \Exception
	 */
	public function getRestHandler() {
		return $this->loadClass( 'RestHandler', true, true );
	}

	/**
	 * @return Strings
	 */
	public function getStrings() {
		return $this->loadClass( 'Strings', true );
	}

	/**
	 * @param string $action
	 * @return array
	 */
	public function getNonceActionData( $action = '' ) {
		$data = $this->getCon()->getNonceActionData( $action );
		$data[ 'mod_slug' ] = $this->getModSlug();
		return $data;
	}

	/**
	 * @param string $action
	 * @param bool   $bAsJsonEncodedObject
	 * @return array|string
	 */
	public function getAjaxActionData( $action = '', $bAsJsonEncodedObject = false ) {
		$aData = $this->getNonceActionData( $action );
		$aData[ 'ajaxurl' ] = admin_url( 'admin-ajax.php' );
		return $bAsJsonEncodedObject ? json_encode( (object)$aData ) : $aData;
	}

	protected function loadDebug() {
		$req = Services::Request();
		if ( ( $req->query( 'test' ) || $req->query( 'debug' ) ) && $req->query( 'mod' ) == $this->getModSlug() ) {
			/** @var Debug $debug */
			$debug = $this->loadClass( 'Debug', true );
			$debug->run();
		}
	}

	/**
	 * @param string $class
	 * @param bool   $injectModCon
	 * @param bool   $throwOnNoClass
	 * @return false|Traits\ModConsumer|mixed
	 * @throws \Exception
	 */
	private function loadClass( string $class, bool $injectModCon = false, bool $throwOnNoClass = false ) {
		$instance = false;
		try {
			$c = $this->findElementClass( $class );
			if ( @class_exists( $c ) && ( new \ReflectionClass( $c ) )->isInstantiable() ) {
				/** @var Base\Traits\ModConsumer $instance */
				$instance = new $c();
				if ( !empty( $instance ) && $injectModCon ) {
					$instance->setMod( $this );
				}
			}
		}
		catch ( \Exception $e ) {
		}
		if ( empty( $instance ) && $throwOnNoClass ) {
			throw  new \Exception( sprintf( 'Suitable class for "%s" could not be found.', $class ) );
		}
		return $instance;
	}

	/**
	 * @return string
	 */
	private function getNamespace() {
		try {
			$sNS = ( new \ReflectionClass( $this ) )->getNamespaceName();
		}
		catch ( \Exception $e ) {
			$sNS = __NAMESPACE__;
		}
		return $sNS;
	}

	/**
	 * @throws \Exception
	 */
	protected function findElementClass( string $element, bool $useFoundationBase = true, bool $throwException = true ) :?string {
		/**
		 * Build a list of potential namespaces where the Class may be found, in order of priority
		 */
		$modNamespace = $this->getNamespace();
		$NSs = [
			$modNamespace,
			substr( $modNamespace, 0, strrpos( $modNamespace, '\\' ) ).'\\Base'
		];
		if ( $useFoundationBase ) {
			$NSs[] = __NAMESPACE__;
		}

		$theClass = null;
		foreach ( $NSs as $NS ) {
			$maybeNS = $NS.'\\'.$element;
			if ( @class_exists( $maybeNS ) ) {
				$theClass = $maybeNS;
				break;
			}
		}

		if ( $throwException && is_null( $theClass ) ) {
			throw new \Exception( sprintf( 'Could not find class for element "%s".', $element ) );
		}
		return $theClass;
	}

	/**
	 * @return Processor
	 * @throws \Exception
	 */
	public function getProcessor() {
		if ( !isset( $this->oProcessor ) ) {
			$sClassName = $this->findElementClass( 'Processor' );
			$this->oProcessor = new $sClassName( $this );
		}
		return $this->oProcessor;
	}

	/**
	 * @return string
	 */
	public function getFeatureAdminPageUrl() {
		$url = sprintf( 'admin.php?page=%s', $this->getModSlug() );
		if ( $this->getCon()->config()->getIsWpmsNetworkAdminOnly() ) {
			$url = network_admin_url( $url );
		}
		else {
			$url = admin_url( $url );
		}
		return $url;
	}

	protected function getPremiumLicenseFilterName() :string {
		return $this->prefix( 'license_is_valid'.$this->getRequestId() );
	}

	protected function hasValidPremiumLicense() :bool {
		return (bool)apply_filters( $this->getPremiumLicenseFilterName(), false );
	}

	/**
	 * @return Notifications
	 * @throws \Exception
	 */
	public function initNotificationsHandler() {
		if ( !isset( $this->oNotificationsHandler ) ) {
			$this->oNotificationsHandler = $this->loadClass( 'Notifications', true );
		}
		return $this->oNotificationsHandler;
	}

	public function isPremium() :bool {
		return $this->hasValidPremiumLicense();
	}

	/**
	 * @param bool $enable
	 */
	public function setIsMainFeatureEnabled( $enable ) {
		return $this->getOptions()->setOpt( 'enable_'.$this->getModSlug( false ), $enable ? 'Y' : 'N' );
	}

	/**
	 * @return mixed
	 */
	public function getIsMainFeatureEnabled() {
		if ( apply_filters( $this->prefix( 'globally_disabled' ), false ) ) {
			return false;
		}
		return $this->getOptions()->getOptIs( 'enable_'.$this->getModSlug( false ), 'Y' )
			   || ( $this->config()->getProperty( 'auto_enabled' ) === true );
	}

	/**
	 * @return string
	 */
	public function getMainFeatureName() {
		return $this->config()->getProperty( 'name' );
	}

	public function getModSlug( bool $withPrefix = true ) :string {
		$slug = $this->config()->getModuleSlug();
		return $withPrefix ? $this->prefix( $slug ) : $slug;
	}

	public function getPluginInstallationTime() :int {
		return (int)$this->getOptions()->getOpt( 'installation_time', 0 );
	}

	/**
	 * @param string $slug
	 * @param string $baseURL
	 * @return string
	 */
	public function getUrl_Wizard( $slug, $baseURL = '' ) {
		return $this->getNoncer()
					->addNonceToUrl(
						add_query_arg(
							[
								$this->prefix( 'action' ) => 'wizard',
								'wizard'                  => $slug,
								'page'                    => $this->getModSlug(),
							],
							empty( $baseURL ) ? $this->getFeatureAdminPageUrl() : $baseURL
						),
						'wizard'
					);
	}

	/**
	 * @return string
	 */
	public function getUrl_WizardLanding() {
		return $this->getUrl_Wizard( 'landing' );
	}

	/**
	 * @param array $items
	 * @return array
	 */
	public function filter_addPluginSubMenuItem( $items ) {
		$sMenuTitleName = $this->config()->getProperty( 'menu_title' );
		if ( is_null( $sMenuTitleName ) ) {
			$sMenuTitleName = $this->getMainFeatureName();
		}
		if ( $this->getIfShowFeatureMenuItem() && !empty( $sMenuTitleName ) ) {

			$sHumanName = $this->getCon()->getLabels()->getHumanName();

			$bMenuHighlighted = $this->config()->getProperty( 'highlight_menu_item' );
			if ( $bMenuHighlighted ) {
				$sMenuTitleName = sprintf( '<span class="icwp_highlighted">%s</span>', $sMenuTitleName );
			}
			$sMenuPageTitle = $sMenuTitleName.' - '.$sHumanName;
			$items[ $sMenuPageTitle ] = [
				$sMenuTitleName,
				$this->getModSlug(),
				[ $this->getModuleRender(), 'displayModuleAdminPage' ]
			];

			$aAdditionalItems = $this->getOptions()->getAdditionalMenuItems();
			if ( !empty( $aAdditionalItems ) && is_array( $aAdditionalItems ) ) {

				foreach ( $aAdditionalItems as $aMenuItem ) {

					if ( empty( $aMenuItem[ 'callback' ] ) || !method_exists( $this, $aMenuItem[ 'callback' ] ) ) {
						continue;
					}

					$sMenuPageTitle = $sHumanName.' - '.$aMenuItem[ 'title' ];
					$items[ $sMenuPageTitle ] = [
						$aMenuItem[ 'title' ],
						$this->prefix( $aMenuItem[ 'slug' ] ),
						[ $this, $aMenuItem[ 'callback' ] ]
					];
				}
			}
		}
		return $items;
	}

	/**
	 * @return array
	 */
	protected function getAdditionalMenuItem() {
		return [];
	}

	/**
	 * @param array $aSummaryData
	 * @return array
	 */
	public function filter_getFeatureSummaryData( $aSummaryData ) {
		if ( !$this->getIfShowFeatureMenuItem() ) {
			return $aSummaryData;
		}

		$sMenuTitle = $this->config()->getProperty( 'menu_title' );
		$aSummaryData[] = [
			'enabled'    => $this->getIsMainFeatureEnabled(),
			'active'     => self::$sActivelyDisplayedModuleOptions == $this->getModSlug( false ),
			'slug'       => $this->getModSlug( false ),
			'name'       => $this->getMainFeatureName(),
			'menu_title' => empty( $sMenuTitle ) ? $this->getMainFeatureName() : $sMenuTitle,
			'href'       => $this->getFeatureAdminPageUrl()
		];

		return $aSummaryData;
	}

	public function getIfShowFeatureMenuItem() :bool {
		return (bool)$this->config()->getProperty( 'show_feature_menu_item' );
	}

	/**
	 * @param string $key
	 * @return mixed|null
	 * @alias
	 */
	public function getDef( $key ) {
		return $this->config()->getDef( $key );
	}

	/**
	 * Will send ajax error response immediately upon failure
	 * @return bool
	 */
	protected function checkAjaxNonce() {

		$sNonce = Services::Request()->request( '_ajax_nonce', '' );
		if ( empty( $sNonce ) ) {
			$sMessage = $this->getTranslatedString( 'nonce_failed_empty', 'Nonce security checking failed - the nonce value was empty.' );
		}
		elseif ( wp_verify_nonce( $sNonce, 'icwp_ajax' ) === false ) {
			$sMessage = $this->getTranslatedString( 'nonce_failed_supplied', 'Nonce security checking failed - the nonce supplied was "%s".' );
			$sMessage = sprintf( $sMessage, $sNonce );
		}
		else {
			return true; // At this stage we passed the nonce check
		}

		// At this stage we haven't returned after success so we failed the nonce check
		$this->sendAjaxResponse( false, [ 'message' => $sMessage ] );
		return false; //unreachable
	}

	/**
	 * @param string $sKey
	 * @param string $sDefault
	 * @return string
	 */
	protected function getTranslatedString( $sKey, $sDefault ) {
		return $sDefault;
	}

	public function sendAjaxResponse( bool $success, array $data = [] ) {
		$success ? wp_send_json_success( $data ) : wp_send_json_error( $data );
	}

	/**
	 * Saves the options to the WordPress Options store.
	 * It will also update the stored plugin options version.
	 */
	public function savePluginOptions() {
		$this->doPrePluginOptionsSave();
		( new Operations\Save() )
			->setDeletePreSave( $this->getCon()->getFlagsLookup()->getHasFlagReset() )
			->execute( $this->getOptions(), $this->getOptionsStorageKey() );
	}

	/**
	 * This is the point where you would want to do any options verification
	 */
	protected function doPrePluginOptionsSave() {
	}

	/**
	 * @param array $aAggregatedOptions
	 * @return array
	 */
	public function aggregateOptionsValues( $aAggregatedOptions ) {
		return array_merge( $aAggregatedOptions, $this->getOptions()->getAllOptionsValues() );
	}

	public function onPluginDelete() {
		if ( $this->getCon()->getPermissions()->getHasPermissionToManage() ) {
			foreach ( $this->loadDbHandlers( true ) as $dbh ) {
				if ( !empty( $dbh ) ) {
					$dbh->tableDelete();
				}
			}
			( new Operations\Delete() )->execute( $this->getOptions(), $this->getOptionsStorageKey() );
			remove_action( $this->prefix( 'plugin_shutdown' ), [ $this, 'onPluginShutdown' ] );
		}
	}

	/**
	 * @deprecated 0.2.3
	 */
	public function deletePluginOptions() {
	}

	public function handleFormSubmit() {
		$this->processFormSubmit();
	}

	public function handleFormSubmitAjax() {
		$this->processFormSubmit();
	}

	protected function processFormSubmit() {
		$oReq = Services::Request();

		if ( $oReq->post( 'mod_slug' ) == $this->getModSlug() ) {

			$sAllOptions = $oReq->post( 'all_options_input' );
			if ( !empty( $sAllOptions ) ) {
				( new Operations\ParseFromSubmit() )->parseAndStore( $this->getOptions(), $sAllOptions );
				$this->savePluginOptions();
			}
			$this->doExtraSubmitProcessing();
		}
	}

	protected function doExtraSubmitProcessing() {
	}

	public function getLogger() :\Monolog\Logger {
		return $this->loadModLogger()->getLogger();
	}

	protected function loadModLogger() :Logger {
		if ( !isset( $this->logger ) ) {
			$this->logger = $this->loadClass( 'Logger', true );
		}
		return $this->logger;
	}

	/**
	 * @return Foundation\Request\Handler\Nonce
	 */
	public function getNoncer() {
		return new Foundation\Request\Handler\Nonce( $this->getCon()->getPrefix() );
	}

	/**
	 * @return Options
	 */
	public function getOptions() {
		if ( !isset( $this->oOptions ) ) {
			$this->oOptions = $this->loadClass( 'Options', true );
		}
		return $this->oOptions;
	}

	/**
	 * @param Options $oModOpts
	 * @return $this
	 */
	public function setOptions( $oModOpts ) {
		$this->oOptions = $oModOpts;
		return $this;
	}

	/**
	 * @param string $sAction
	 */
	public function handleNoNonceQueryAction( $sAction ) {
	}

	/**
	 * @param string $sAction
	 */
	public function handleQueryAction( $sAction ) {
	}

	/**
	 * @param string $sAction
	 */
	public function handleQueryActionAuthenticated( $sAction ) {
	}

	/**
	 * @return bool
	 */
	protected function isPageThisController() {
		return ( Services::Request()->query( 'page' ) == $this->getModSlug() );
	}

	/**
	 * Prefixes an option key only if it's needed
	 * @param $sKey
	 * @return string
	 */
	public function prefixOptionKey( $sKey ) {
		return $this->prefix( $sKey, '_' );
	}

	/**
	 * Will prefix and return any string with the unique plugin prefix.
	 * @param string $sSuffix
	 * @param string $sGlue
	 * @return string
	 */
	public function prefix( $sSuffix = '', $sGlue = '-' ) {
		return $this->getCon()->getPrefix()->prefix( $sSuffix, $sGlue );
	}

	public function runDailyCron() {
		$this->cleanupDatabases();
	}

	public function runHourlyCron() {
	}

	/**
	 * quick wrapper
	 * @return string
	 */
	public function getRequestId() {
		return $this->getCon()->getVisitorSession()->getRequestId();
	}

	/**
	 * quick wrapper
	 * @return string
	 */
	public function getSessionId() {
		return $this->getCon()->getVisitorSession()->getId();
	}

	/**
	 * @return bool
	 */
	public function canRunWizards() {
		return Services::Data()->getPhpVersionIsAtLeast( '5.4.0' );
	}

	/**
	 * @return Render
	 */
	public function getModuleRender() {
		return $this->loadClass( 'Render', true );
	}

	public function getWpCli() :?WpCli {
		if ( !isset( $this->wpcli ) ) {
			try {
				$this->wpcli = $this->loadClass( 'WpCli', true );
			}
			catch ( \Exception $e ) {
				$this->wpcli = null;
			}
		}
		return $this->wpcli;
	}

	public function getBuilderForUI() :Operations\BuildForUI {
		return ( new Operations\BuildForUI() )->setMod( $this );
	}

	public function getIsShowMarketing() :bool {
		return (bool)apply_filters( $this->prefix( 'show_marketing' ), true );
	}

	/**
	 * @param array $data
	 * @throws \Exception
	 */
	public function renderAdminNotice( $data ) :string {
		if ( empty( $data[ 'notice_attributes' ] ) ) {
			throw new \Exception( 'notice_attributes is empty' );
		}

		if ( !isset( $data[ 'icwp_ajax_nonce' ] ) ) {
			$data[ 'icwp_ajax_nonce' ] = wp_create_nonce( 'icwp_ajax' );
		}
		if ( !isset( $data[ 'icwp_admin_notice_template' ] ) ) {
			$data[ 'icwp_admin_notice_template' ] = $data[ 'notice_attributes' ][ 'notice_id' ];
		}

		if ( !isset( $data[ 'notice_classes' ] ) ) {
			$data[ 'notice_classes' ] = [];
		}
		if ( is_array( $data[ 'notice_classes' ] ) ) {
			if ( empty( $data[ 'notice_classes' ] ) ) {
				$data[ 'notice_classes' ][] = 'updated';
			}
			$data[ 'notice_classes' ][] = $data[ 'notice_attributes' ][ 'type' ];
		}
		$data[ 'notice_classes' ] = implode( ' ', $data[ 'notice_classes' ] );

		return $this->getModuleRender()
					->renderTemplate( 'notices/admin-notice-template', $data );
	}

	/**
	 * TODO: Remove from controller
	 * @param array $aTransferableOptions
	 * @return array
	 */
	public function exportTransferableOptions( $aTransferableOptions ) {
		if ( !is_array( $aTransferableOptions ) ) {
			$aTransferableOptions = [];
		}
		$oExporter = new Operations\Export();
		$aTransferableOptions[ $this->getOptionsStorageKey() ] = $oExporter->toArray( $this->getOptions() );
		return $aTransferableOptions;
	}

	/**
	 * @param string $sOptionKey
	 * @param mixed  $mDefault
	 * @return mixed
	 * @deprecated 0.1.2
	 */
	public function getOpt( $sOptionKey, $mDefault = null ) {
		return $this->getOptions()->getOpt( $sOptionKey, $mDefault );
	}

	/**
	 * @param         $sKey
	 * @param mixed   $mValueToTest
	 * @param boolean $bStrict
	 * @return bool
	 * @deprecated 0.1.2
	 */
	public function getOptIs( $sKey, $mValueToTest, $bStrict = false ) {
		return $this->getOptions()->getOptIs( $sKey, $mValueToTest, $bStrict );
	}

	/**
	 * @return Options
	 * @deprecated 0.1.2
	 */
	public function getOptionsVo() {
		return $this->getOptions();
	}

	/**
	 * @param Options $oOptions
	 * @return $this
	 * @deprecated 0.1.2
	 */
	public function setOptionsVo( $oOptions ) {
		return $this->setOptions( $oOptions );
	}

	/**
	 * @param array $aOptions
	 * @return $this
	 * @deprecated 0.1.2
	 */
	public function setMultipleOptions( $aOptions ) {
		$oVO = $this->getOptions();
		foreach ( $aOptions as $sKey => $mValue ) {
			$oVO->setOpt( $sKey, $mValue );
		}
		return $this;
	}

	/**
	 * @param string $sOptionKey
	 * @param mixed  $mValue
	 * @return mixed
	 * @deprecated 0.1.2
	 */
	public function setOpt( $sOptionKey, $mValue ) {
		return $this->getOptions()->setOpt( $sOptionKey, $mValue );
	}

	/**
	 * TODO: Remove from controller
	 * @return array
	 */
	public function collectOptionsForTracking() {
		$modCfg = $this->config();
		$aOptionsData = $this->getOptions()->getOptionsMaskSensitive();
		foreach ( $aOptionsData as $sOption => $mValue ) {
			unset( $aOptionsData[ $sOption ] );
			// some cleaning to ensure we don't have disallowed characters
			$sOption = preg_replace( '#[^_a-z]#', '', strtolower( $sOption ) );
			$sType = $modCfg->getOptionType( $sOption );
			if ( $sType == 'checkbox' ) { // only want a boolean 1 or 0
				$aOptionsData[ $sOption ] = (int)( $mValue == 'Y' );
			}
			else {
				$aOptionsData[ $sOption ] = $mValue;
			}
		}
		return $aOptionsData;
	}

	/**
	 * @return Foundation\Control\Controller
	 * @deprecated
	 */
	public function getController() {
		return $this->getCon();
	}

	/**
	 * @param string $sDefinitionKey
	 * @return mixed|null
	 * @deprecated
	 */
	public function getDefinition( $sDefinitionKey ) {
		return $this->getDef( $sDefinitionKey );
	}

	/**
	 * @return string
	 * @deprecated
	 */
	public function getFeatureSlug() {
		return $this->getModSlug( false );
	}
}