<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base;

class Strings {

	use Traits\ModConsumer;

	public function __construct( ?Controller $mod = null ) {
		$this->setMod( $mod );
	}

	public function getSectionDef( string $sectionSlug ) :array {

		switch ( $sectionSlug ) {
			case 'section_non_ui' :
				$title = __( 'Non UI' );
				$titleShort = __( 'Non UI' );
				break;
			default:
				$section = $this->getOptions()->config()->getSection( $sectionSlug ) ?? [];
				$title = $section[ 'title' ] ?? 'Unknown Title';
				$titleShort = $section[ 'title_short' ] ?? 'Unknown Title';
				$summary = $section[ 'summary' ] ?? [];
				break;
		}

		return [
			'title'       => $title,
			'title_short' => $titleShort,
			'summary'     => ( isset( $summary ) && is_array( $summary ) ) ? $summary : [],
		];
	}

	public function getOptionsDef( string $optKey ) :array {

		switch ( $optKey ) {
			case 'enable_' :
				$name = __( 'Enable' );
				$summary = __( 'Enable' );
				$description = __( 'Enable' );
				break;

			default:
				$opt = $this->getOptions()->config()->getRawData_SingleOption( $optKey );
				$name = $opt[ 'name' ] ?? 'Unknown Option Name';
				$summary = $opt[ 'summary' ] ?? 'Unknown Option Summary';
				$description = $opt[ 'description' ] ?? 'Unknown Option Description';
		}

		return [
			'name'        => $name,
			'summary'     => $summary,
			'description' => $description,
		];
	}

	/**
	 * @param string $sOptKey
	 * @return array
	 * @throws \Exception
	 * @deprecated 0.7.4
	 */
	public function loadOptions( $sOptKey ) {
		return $this->getOptionsDef( $sOptKey );
	}

	/**
	 * @param string $sectionSlug
	 * @return array
	 * @throws \Exception
	 * @deprecated 0.7.4
	 */
	public function loadSectionTitles( $sectionSlug ) {
		return $this->getSectionDef( $sectionSlug );
	}
}