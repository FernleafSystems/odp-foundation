<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base;

use FernleafSystems\Wordpress\Plugin\Foundation\Configuration\Module\Config;
use FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Traits\ModConsumer;
use FernleafSystems\Wordpress\Plugin\Foundation\Module\Core\Options as CoreOptions;
use FernleafSystems\Wordpress\Plugin\Foundation\Module\Options\Operations\Load;

class Options {

	use ModConsumer;

	/**
	 * @var string
	 */
	protected $aOptionsKeys;

	/**
	 * @var array
	 */
	protected $aOptionsValues;

	/**
	 * @var boolean
	 */
	protected $bNeedSave;

	/**
	 * @var CoreOptions
	 */
	protected $oCoreOptions;

	/**
	 * @return Config
	 */
	public function config() {
		return $this->getMod()->getModuleConfig();
	}

	/**
	 * @return CoreOptions
	 */
	public function getCoreOptions() {
		return $this->oCoreOptions;
	}

	/**
	 * @return bool
	 */
	public function getHasCoreOptions() {
		return isset( $this->oCoreOptions );
	}

	/**
	 * @return array
	 */
	public function getAllOptionsValues() {
		if ( !isset( $this->aOptionsValues ) ) {
			$oPrfx = $this->getCon()->getPrefix();
			$this->aOptionsValues = ( new Load() )
				->valuesFromWp( $oPrfx->prefixOption( $this->config()->getStorageKey() ) );
		}
		return $this->aOptionsValues;
	}

	/**
	 * Returns an array of all the transferable options and their values
	 * @return array
	 */
	public function getTransferableOptions() {

		$aOptions = $this->getAllOptionsValues();
		$aTransferable = [];
		foreach ( $this->config()->options as $nKey => $aOptionData ) {
			if ( isset( $aOptionData[ 'transferable' ] ) && $aOptionData[ 'transferable' ] === true ) {
				$aTransferable[ $aOptionData[ 'key' ] ] = $aOptions[ $aOptionData[ 'key' ] ];
			}
		}
		return $aTransferable;
	}

	/**
	 * Returns an array of all the options with the values for "sensitive" options masked out.
	 * @return array
	 */
	public function getOptionsMaskSensitive() {

		$aValues = $this->getAllOptionsValues();
		foreach ( $this->getOptionsKeys() as $sKey ) {
			if ( !isset( $aValues[ $sKey ] ) ) {
				$aValues[ $sKey ] = $this->getOptDefault( $sKey );
			}
		}
		foreach ( $this->config()->options as $nKey => $aOptionData ) {
			if ( isset( $aOptionData[ 'sensitive' ] ) && $aOptionData[ 'sensitive' ] === true ) {
				unset( $aValues[ $aOptionData[ 'key' ] ] );
			}
		}
		return $aValues;
	}

	/**
	 * Determines whether the given option key is a valid option
	 * @param string
	 */
	public function getIsValidOptionKey( $key ) :bool {
		return in_array( $key, $this->getOptionsKeys() );
	}

	public function getHiddenOptions() :array {
		$optionsData = [];

		foreach ( $this->config()->getRawData_FullFeatureConfig()[ 'sections' ] as $section ) {

			// if hidden isn't specified we skip
			if ( !isset( $section[ 'hidden' ] ) || !$section[ 'hidden' ] ) {
				continue;
			}
			foreach ( $this->config()->options as $option ) {
				if ( $option[ 'section' ] != $section[ 'slug' ] ) {
					continue;
				}
				$optionsData[ $option[ 'key' ] ] = $this->getOpt( $option[ 'key' ] );
			}
		}
		return $optionsData;
	}

	public function getLegacyOptionsConfigData() :array {
		$legacyData = [];

		foreach ( $this->config()->getSections() as $section ) {

			$section = array_merge(
				[
					'primary' => false,
					'options' => []
				],
				$section
			);

			foreach ( $this->config()->options as $optDef ) {

				if ( $optDef[ 'section' ] != $section[ 'slug' ] ) {
					continue;
				}
				if ( isset( $optDef[ 'hidden' ] ) && $optDef[ 'hidden' ] ) {
					continue;
				}

				$optDef = array_merge(
					[
						'name'          => 'Unknown Name',
						'summary'       => 'Unknown Summary',
						'description'   => 'Unknown Description',
						'link_info'     => '',
						'link_blog'     => '',
						'help_video_id' => '',
						'value_options' => []
					],
					$optDef
				);

				if ( in_array( $optDef[ 'type' ], [ 'select', 'multiple_select' ] ) ) {
					$convertedOptions = [];
					foreach ( $optDef[ 'value_options' ] as $aValueOptions ) {
						$convertedOptions[ $aValueOptions[ 'value_key' ] ] = $aValueOptions[ 'text' ];
					}
					$optDef[ 'value_options' ] = $convertedOptions;
				}

				$section[ 'options' ][] = $optDef;
			}

			if ( count( $section[ 'options' ] ) > 0 ) {
				$legacyData[] = $section;
			}
		}
		return $legacyData;
	}

	/**
	 * @return array
	 */
	public function getAdditionalMenuItems() {
		return $this->config()->menu_items;
	}

	/**
	 * @return string
	 */
	public function getNeedSave() {
		return $this->bNeedSave;
	}

	/**
	 * @param string $key
	 * @param mixed  $mDefault
	 * @return mixed
	 */
	public function getOpt( $key, $mDefault = null ) {
		$aValues = $this->getAllOptionsValues();
		if ( !isset( $aValues[ $key ] ) && $this->getIsValidOptionKey( $key ) ) {
			$this->setOpt( $key, $this->getOptDefault( $key, $mDefault ) );
			$aValues = $this->getAllOptionsValues();
		}
		return $aValues[ $key ] ?? $this->getOpt_Core( $key, $mDefault );
	}

	/**
	 * @param string $key
	 * @param mixed  $mDefault
	 * @return mixed
	 */
	protected function getOpt_Core( $key, $mDefault = null ) {
		return $this->getHasCoreOptions() ? $this->getCoreOptions()->getOpt( $key ) : $mDefault;
	}

	/**
	 * @param string $key
	 * @param mixed  $default
	 * @return mixed|null
	 */
	public function getOptDefault( $key, $default = null ) {
		$option = $this->config()->getOptDefinition( $key );
		if ( isset( $option[ 'default' ] ) ) {
			$default = $option[ 'default' ];
		}
		elseif ( isset( $option[ 'value' ] ) ) {
			$default = $option[ 'value' ];
		}
		return $default;
	}

	/**
	 * @param         $sKey
	 * @param mixed   $mValueToTest
	 * @param bool    $strict
	 * @return bool
	 */
	public function getOptIs( $sKey, $mValueToTest, $strict = false ) {
		$mOptionValue = $this->getOpt( $sKey );
		return $strict ? $mOptionValue === $mValueToTest : $mOptionValue == $mValueToTest;
	}

	/**
	 * @return string[]
	 */
	public function getOptionsKeys() {
		if ( !isset( $this->aOptionsKeys ) ) {
			$this->aOptionsKeys = [];
			foreach ( $this->config()->options as $aOption ) {
				$this->aOptionsKeys[] = $aOption[ 'key' ];
			}
		}
		return $this->aOptionsKeys;
	}

	/**
	 * @return string[]
	 */
	public function getOptionsForWpCli() :array {
		return array_filter(
			$this->getOptionsKeys(),
			function ( $key ) {
				$opt = $this->config()->getRawData_SingleOption( $key );
				return !empty( $opt[ 'section' ] ) && $opt[ 'section' ] !== 'section_non_ui';
			}
		);
	}

	/**
	 * @param string $sOptionKey
	 * @return bool
	 */
	public function resetOptToDefault( $sOptionKey ) {
		return $this->setOpt( $sOptionKey, $this->getOptDefault( $sOptionKey ) );
	}

	/**
	 * @param boolean $bNeed
	 * @return $this
	 */
	public function setNeedSave( $bNeed ) {
		$this->bNeedSave = $bNeed;
		return $this;
	}

	/**
	 * @param array $aOptions
	 */
	public function setMultipleOptions( $aOptions ) {
		if ( is_array( $aOptions ) ) {
			foreach ( $aOptions as $sKey => $mValue ) {
				$this->setOpt( $sKey, $mValue );
			}
		}
	}

	/**
	 * @param CoreOptions $oCoreOptions
	 * @return $this
	 */
	public function setCoreOptions( $oCoreOptions ) {
		if ( is_object( $oCoreOptions ) && $oCoreOptions instanceof CoreOptions ) {
			$this->oCoreOptions = $oCoreOptions;
		}
		else {
			$this->oCoreOptions = null;
		}
		return $this;
	}

	/**
	 * @param string $sOptionKey
	 * @param mixed  $mValue
	 * @return mixed
	 */
	public function setOpt( $sOptionKey, $mValue ) {

		// We can't use getOpt() to find the current value since we'll create an infinite loop
		$aValues = $this->getAllOptionsValues();
		$mCurrent = isset( $aValues[ $sOptionKey ] ) ? $aValues[ $sOptionKey ] : null;

		if ( serialize( $mCurrent ) !== serialize( $mValue ) ) {
			$this->setNeedSave( true );

			//Load the config and do some pre-set verification where possible. This will slowly grow.
			$aOption = $this->config()->getRawData_SingleOption( $sOptionKey );
			if ( !empty( $aOption[ 'type' ] ) ) {
				if ( $aOption[ 'type' ] == 'boolean' && !is_bool( $mValue ) ) {
					return $this->resetOptToDefault( $sOptionKey );
				}
			}
			$aValues[ $sOptionKey ] = $mValue;
			$this->setOptionsValues( $aValues );
		}
		return true;
	}

	/**
	 * @param string $sOptionKey
	 * @return $this
	 */
	public function unsetOpt( $sOptionKey ) {
		$aValues = $this->getAllOptionsValues();
		unset( $aValues[ $sOptionKey ] );
		return $this->setOptionsValues( $aValues )
					->setNeedSave( true );
	}

	/**
	 * @return $this
	 */
	public function cleanOptions() {
		$aValues = $this->getAllOptionsValues();
		if ( !empty( $aValues ) && is_array( $aValues ) ) {

			foreach ( $aValues as $sKey => $mValue ) {
				if ( !$this->getIsValidOptionKey( $sKey ) ) {
					$this->unsetOpt( $sKey )
						 ->setNeedSave( true );
				}
			}
		}
		return $this;
	}

	/**
	 * @param array $aOptionsValues
	 * @return Options
	 */
	public function setOptionsValues( $aOptionsValues ) {
		$this->aOptionsValues = $aOptionsValues;
		return $this;
	}
}