<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Wizard;

use FernleafSystems\Utilities\Data\Adapter\DynPropertiesClass;

class WizardVo extends DynPropertiesClass {

	/**
	 * @return string
	 */
	public function getHookType() {
		return $this->hook_type;
	}

	/**
	 * @return array
	 */
	public function getWizardJsOptions() {
		return $this->wizard_js_options;
	}

	/**
	 * @return string
	 */
	public function getSlug() {
		return $this->slug;
	}

	/**
	 * @return array
	 */
	public function getSteps() {
		return $this->steps;
	}

	/**
	 * @return string
	 */
	public function getMinUserPermissions() {
		$sPerm = $this->min_user_permissions;
		if ( is_null( $sPerm ) ) {
			$sPerm = 'manage_options';
		}
		return $sPerm;
	}

	/**
	 * @return bool
	 */
	public function isDynamic() {
		return (bool)$this->is_dynamic;
	}

	/**
	 * @param string $sSlug
	 * @return $this
	 */
	public function setSlug( $sSlug ) {
		$this->slug = $sSlug;
		return $this;
	}
}