<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base;

use FernleafSystems\Wordpress\Plugin\Foundation\Database\Operations\Clean;
use FernleafSystems\Wordpress\Plugin\Foundation\Database\Operations\Create;
use FernleafSystems\Wordpress\Plugin\Foundation\Database\Operations\Cron;
use FernleafSystems\Wordpress\Plugin\Foundation\Database\Operations\Query;
use FernleafSystems\Wordpress\Plugin\Foundation\Database\Operations\Verify;
use FernleafSystems\Wordpress\Plugin\Foundation\Database\Vo\DbVo;

/**
 * Class Database
 * @package    FernleafSystems\Wordpress\Plugin\Foundation\Module\Base
 * @deprecated 0.3.0
 */
abstract class Database {

	/**
	 * @var DbVo
	 */
	protected $oDbVo;

	/**
	 * @var boolean
	 */
	protected $bTableExists;

	/**
	 * @var boolean
	 */
	protected $bTableReady;

	/**
	 * @var integer
	 */
	protected $nAutoExpirePeriod = null;

	/**
	 * @var string
	 */
	protected $sTableCreateSql;

	/**
	 * @var Query
	 */
	protected $oQuery;

	/**
	 * @param DbVo $oDbVo
	 */
	public function __construct( $oDbVo ) {
		$this->oDbVo = $oDbVo;
		$this->initializeTable();
		if ( $this->getTableReady() ) {
			$this->createCleanupCron();
		}
	}

	/**
	 * Typically executed in the cleanup Cron
	 */
	public function cleanupDatabase() {
		$oCleaner = new Clean( $this->oDbVo );
		$oCleaner->cleanOld( $this->getAutoExpirePeriod() );
	}

	protected function initializeTable() {
		$oVerify = new Verify( $this->oDbVo );
		if ( $oVerify->exists() ) {
			if ( $oVerify->valid() ) {
				$this->bTableReady = true;
			}
			else {
				$oCreate = new Create( $this->oDbVo );
				$oCreate->recreate();
				$this->bTableReady = ( $oVerify->exists() && $oVerify->valid() );
			}
		}
		else {
			$oCreate = new Create( $this->oDbVo );
			$oCreate->create( $this->getCreateTableSql() );
			$this->bTableReady = ( $oVerify->exists() && $oVerify->valid() );
		}
	}

	/**
	 * Will setup the cleanup cron to clean out old entries. This should be overridden per implementation.
	 */
	protected function createCleanupCron() {
		$oCron = new Cron( $this->oDbVo );
		return $oCron->create( $this->getDbCleanupHookName(), [ $this, 'cleanupDatabase' ] );
	}

	/**
	 * @return string
	 */
	abstract public function getCreateTableSql();

	/**
	 * Will setup the cleanup cron to clean out old entries. This should be overridden per implementation.
	 */
	protected function deleteCleanupCron() {
		wp_clear_scheduled_hook( $this->getDbCleanupHookName() );
	}

	/**
	 * @return int
	 */
	public function getAutoExpirePeriod() {
		return $this->nAutoExpirePeriod;
	}

	/**
	 * @return string
	 */
	public function getDbCleanupHookName() {
		return sprintf( '%s_db_cleanup', $this->oDbVo->getTableName() );
	}

	/**
	 * @return Query
	 */
	public function getQuery() {
		if ( !isset( $this->oQuery ) ) {
			$this->oQuery = new Query( $this->oDbVo );
		}
		return $this->oQuery;
	}

	/**
	 * @return bool
	 */
	public function getTableExists() {
		if ( $this->getTableReady() || $this->bTableExists === true ) { // only return true if this is true.
			return true;
		}
		$oVerify = new Verify( $this->oDbVo );
		$this->bTableExists = $oVerify->exists();
		return $this->bTableExists;
	}

	/**
	 * @return bool
	 */
	public function getTableReady() {
		return $this->bTableReady;
	}

	/**
	 * @param int $nTimePeriod
	 * @return $this
	 */
	public function setAutoExpirePeriod( $nTimePeriod ) {
		$this->nAutoExpirePeriod = $nTimePeriod;
		return $this;
	}
}