<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base;

class Request {

	use Traits\ModConsumer;

	public function __construct( ?Controller $mod = null ) {
		$this->setMod( $mod );
		// Needs fixed: -1 is a hack to get in before our plugin handles this.
		add_action( 'init', [ $this, 'onWpInit' ], -1 );
	}

	public function onWpInit() {
		$mod = $this->getMod();

		add_action( $mod->prefix( 'ajax_request' ), [ $this, 'addActions' ] );
		add_action( $mod->prefix( 'authenticated_ajax_request' ), [ $this, 'addActionsAuthenticated' ] );

		add_action( $mod->prefix( 'form_submit' ), fn() => $this->formSubmit() );
		add_action( $mod->prefix( 'authenticated_form_submit' ), fn() => $this->adminFormSubmit() );

		add_action( $mod->prefix( 'query_action' ), fn() => $this->queryAction() );
		add_action( $mod->prefix( 'authenticated_query_action' ), fn() => $this->adminQueryAction() );
	}

	protected function formSubmit() {
	}

	protected function adminFormSubmit() {
	}

	protected function queryAction() {
	}

	protected function adminQueryAction() {
	}

	/**
	 * Override this to add in Ajax actions that do not require user/administrative authentication
	 */
	public function addActions() {
	}

	/**
	 * Override this to add in Ajax actions that DO require user/administrative authentication
	 */
	public function addActionsAuthenticated() {
	}

	/**
	 * @param bool  $success
	 * @param array $data
	 */
	protected function sendAjaxResponse( $success, $data = [] ) {
		$success ? wp_send_json_success( $data ) : wp_send_json_error( $data );
	}

	protected function prefixWpAjax( string $suffix = '' ) :string {
		return sprintf( '%s%s', 'wp_ajax_', $this->getMod()->prefix( $suffix ) );
	}
}