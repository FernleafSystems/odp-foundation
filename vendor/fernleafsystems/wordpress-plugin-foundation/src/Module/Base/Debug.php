<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base;

class Debug {

	use \FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Traits\ModConsumer;

	public function __construct() {
		add_action( 'wp_loaded', [ $this, 'onWpLoaded' ] );
		add_action( 'shutdown', [ $this, 'onShutdown' ] );
	}

	public function run() {
	}

	public function onWpLoaded() {
	}

	public function onShutdown() {
	}
}