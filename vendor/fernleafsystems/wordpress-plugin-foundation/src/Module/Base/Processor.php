<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base;

use FernleafSystems\Wordpress\Services\Services;

class Processor {

	use Traits\ModConsumer;

	public function __construct( Controller $mod ) {
		$this->setMod( $mod );
		add_action( $mod->prefix( 'no_nonce_query_action' ), [ $this, 'handleNoNonceQueryAction' ] );
		add_action( $mod->prefix( 'query_action' ), [ $this, 'handleQueryAction' ] );
		add_action( $mod->prefix( 'authenticated_query_action' ), [
			$this,
			'handleQueryActionAuthenticated'
		] );
		add_action( $mod->prefix( 'plugin_shutdown' ), [ $this, 'action_doFeatureProcessorShutdown' ] );
		$this->init();
	}

	/**
	 * @return \FernleafSystems\Wordpress\Plugin\Foundation\Control\Controller
	 * @deprecated 0.10
	 */
	public function getController() {
		return $this->getCon();
	}

	/**
	 * @param string $action
	 */
	public function handleNoNonceQueryAction( $action ) {
	}

	/**
	 * @param string $sAction
	 */
	public function handleQueryAction( $sAction ) {
	}

	/**
	 * @param string $sAction
	 */
	public function handleQueryActionAuthenticated( $sAction ) {
	}

	public function action_doFeatureProcessorShutdown() {
	}

	/**
	 * Resets the object values to be re-used anew
	 */
	public function init() {
	}

	/**
	 * @return bool
	 */
	protected function readyToRun() {
		return true;
	}

	/**
	 * Override to set what this processor does when it's "run"
	 */
	public function run() {
	}

	/**
	 * We don't handle locale derivatives (yet)
	 * @return string
	 */
	protected function getGoogleRecaptchaLocale() {
		return explode( '_', Services::WpGeneral()->getLocale(), 2 )[ 0 ];
	}

	/**
	 * @return mixed
	 */
	public function getPluginDefaultRecipientAddress() {
		return apply_filters( $this->getMod()->prefix( 'report_email_address' ),
			Services::WpGeneral()->getSiteAdminEmail() );
	}
}