<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Traits;

use FernleafSystems\Wordpress\Plugin\Foundation;

trait ModuleConfigConsumer {

	/**
	 * @var Foundation\Configuration\Module\Config
	 */
	private $oModuleConfig;

	/**
	 * @return Foundation\Configuration\Module\Config
	 */
	protected function config() {
		return $this->oModuleConfig;
	}

	/**
	 * @return Foundation\Configuration\Module\Config
	 */
	public function getModuleConfig() {
		return $this->config();
	}

	/**
	 * @param Foundation\Configuration\Module\Config $oConfig
	 * @return $this
	 */
	public function setModuleConfig( $oConfig ) {
		$this->oModuleConfig = $oConfig;
		return $this;
	}
}