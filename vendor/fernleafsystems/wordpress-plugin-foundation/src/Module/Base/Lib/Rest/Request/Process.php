<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Lib\Rest\Request;

use FernleafSystems\Wordpress\Plugin\Foundation\Module\Base;
use FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Lib\Rest;
use FernleafSystems\Wordpress\Services\Services;
use FernleafSystems\Wordpress\Services\Utilities\File\Cache;

/**
 * Class Process
 * @package FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Lib\Rest\Request
 */
abstract class Process {

	use Base\Traits\ModConsumer;
	use Rest\Route\RestRouteConsumer;

	/**
	 * @var RequestVO
	 */
	private $oReqVO;

	/**
	 * @var \WP_REST_Request
	 */
	protected $oRequest;

	/**
	 * Process constructor.
	 * @param Rest\Route\RouteBase|mixed $oRoute
	 * @param \WP_REST_Request           $oRestRequest
	 */
	public function __construct( $oRoute, \WP_REST_Request $oRestRequest ) {
		$this->setRestRoute( $oRoute );
		$this->oRequest = $oRestRequest;
	}

	/**
	 * @return array
	 */
	public function run() :array {
		$oRoute = $this->getRestRoute();

		$aApiResponse = [
			'meta' => [
				'ts'          => Services::Request()->ts(),
				'api_version' => $this->getCon()->config()->getVersion(),
				'from_cache'  => false
			],
		];

		$bLocked = false;
		try {
			if ( $oRoute->bypass_lock !== true ) {
				$oLocker = ( new Base\Lib\Rest\Utility\RestLocker() )->setRestRoute( $oRoute );
				$bLocked = $oLocker->start();
			}

			// Ensure only valid parameters are supplied
			$aPermitted = array_keys( $this->oRequest->get_attributes()[ 'args' ] );
			if ( count( array_diff_key(
					$this->oRequest->get_params(),
					array_flip( $aPermitted )
				) ) > 0 ) {
				throw new \Exception(
					sprintf( 'Please only supply parameters that are permitted: %s',
						implode( ', ', $aPermitted ) ), 500 );
			}

			// Is the site ready?
			( new Base\Lib\Rest\Utility\PreChecks() )
				->setMod( $this->getMod() )
				->run();

			// Begin processing.

			$oCacher = $oRoute->getCacheHandler();
			$oCacher->request_file = $this->getCacheFileFragment();
			$oCacheDef = $oCacher->getCacheDefinition();
			if ( $oCacher->can_cache ) {
				( new Cache\LoadFromCache() )
					->setCacheDef( $oCacheDef )
					->load();
				if ( is_array( $oCacheDef->data ) ) {
					$aApiResponse[ 'meta' ][ 'from_cache' ] = true;
				}
			}

			if ( !is_array( $oCacheDef->data ) ) {
				$oCacheDef->data = $this->process();
				if ( $oCacher->can_cache ) {
					( new Cache\StoreToCache() )
						->setCacheDef( $oCacheDef )
						->store();
				}
			}

			$aApiResponse[ 'error' ] = false;
			$aApiResponse[ $this->getResponseResultsDataKey() ] = $oCacheDef->data;
		}
		catch ( \Exception $oE ) {
			$aApiResponse[ 'error' ] = true;
			$aApiResponse[ 'code' ] = $oE->getCode();
			$aApiResponse[ 'message' ] = $oE->getMessage();
			$aApiResponse[ $this->getResponseResultsDataKey() ] = [];
		}

		if ( $bLocked && !empty( $oLocker ) ) {
			$oLocker->end();
		}

		return $aApiResponse;
	}

	/**
	 * @return array
	 * @throws \Exception
	 */
	abstract protected function process() :array;

	/**
	 * @return string
	 */
	protected function getResponseResultsDataKey() :string {
		return $this->getMod()->getModSlug( false );
	}

	/**
	 * @return \WP_REST_Request
	 */
	protected function getRestRequest() :\WP_REST_Request {
		return $this->oRequest;
	}

	/**
	 * @return RequestVO|mixed
	 */
	protected function getRequestVO() {
		if ( !isset( $this->oReqVO ) ) {
			$this->oReqVO = $this->getRestRoute()
								 ->getNewReqVO()
								 ->applyFromArray( $this->oRequest->get_params() );
		}
		return $this->oReqVO;
	}

	/**
	 * @return string
	 */
	protected function getCacheFileFragment() :string {
		$aD = $this->getRestRequest()->get_params();
		ksort( $aD );
		return md5( serialize( $aD ) );
	}
}
