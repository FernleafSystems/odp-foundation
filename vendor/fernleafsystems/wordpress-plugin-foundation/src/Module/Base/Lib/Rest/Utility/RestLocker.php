<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Lib\Rest\Utility;

use FernleafSystems\Wordpress\Plugin\Foundation\Module;
use FernleafSystems\Wordpress\Services\Services;

class RestLocker {

	use Module\Base\Lib\Rest\Route\RestRouteConsumer;

	/**
	 * @return bool
	 * @throws \Exception
	 */
	public function start() {
		$nCount = 0;
		$nMax = 20;
		while ( $this->isLocked() ) {
			if ( $nCount > $nMax ) {
				throw new \Exception( 'Could not get a lock - there are too many requests processing. Please try again a bit later.', 403 );
			}
			$nCount++;
			usleep( 50000 );
		}
		return $this->writeLock();
	}

	/**
	 * @return bool
	 * @throws \Exception
	 */
	private function isLocked() {
		clearstatcache();
		$oFs = Services::WpFs();
		$sFile = $this->getLockFile();
		return !empty( $sFile ) &&
			   $oFs->exists( $sFile ) && ( Services::Request()->ts() - (int)$oFs->getModifiedTime( $sFile ) < 10 );
	}

	/**
	 * @return bool
	 */
	private function writeLock() {
		return Services::WpFs()->touch( $this->getLockFile() );
	}

	public function end() {
		$oFs = Services::WpFs();
		$sFile = $this->getLockFile();
		if ( $oFs->exists( $sFile ) ) {
			Services::WpFs()->deleteFile( $sFile );
		}
	}

	/**
	 * @return string
	 */
	private function getLockFile() {
		try {
			$sBase = $this->getRestRoute()->getWorkingDir();
			$sFile = path_join( $sBase, 'rest_process.lock' );
		}
		catch ( \Exception $oE ) {
			$sFile = false;
		}
		return $sFile;
	}
}