<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Lib\Rest;

use FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Lib\Rest\Request\RequestVO;

trait RequestVoConsumer {

	/**
	 * @var RequestVO
	 */
	private $oRequestVO;

	/**
	 * @return RequestVO|mixed
	 */
	public function getRequestVO() {
		return $this->oRequestVO;
	}

	/**
	 * @param RequestVO|mixed $oReqVO
	 * @return $this
	 */
	public function setRequestVO( $oReqVO ) {
		$this->oRequestVO = $oReqVO;
		return $this;
	}
}