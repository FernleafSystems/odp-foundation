<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Lib\Rest\Request;

use FernleafSystems\Utilities\Data\Adapter\StdClassAdapter;

/**
 * @property string $action
 * @property string $type
 */
class RequestVO {

	use StdClassAdapter;

	/**
	 * @return string
	 */
	public function getCacheFileSlug() {
		$aD = $this->getRawDataAsArray();
		ksort( $aD );
		return md5( serialize( $aD ) );
	}
}