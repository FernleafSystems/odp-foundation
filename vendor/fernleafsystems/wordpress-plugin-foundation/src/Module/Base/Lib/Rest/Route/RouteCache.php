<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Module\Base\Lib\Rest\Route;

use FernleafSystems\Utilities\Data\Adapter\StdClassAdapter;
use FernleafSystems\Wordpress\Services\Services;
use FernleafSystems\Wordpress\Services\Utilities\File\Cache\CacheDefVO;
use FernleafSystems\Wordpress\Services\Utilities\File\Paths;

/**
 * @property bool            $can_cache
 * @property string          $request_file
 * @property bool            $is_touch
 * @property int             $expiration
 * @property RouteBase|mixed $oRoute
 */
class RouteCache {

	use StdClassAdapter;

	/**
	 * RouteCache constructor.
	 * @param RouteBase|mixed $oRoute
	 */
	public function __construct( $oRoute ) {
		$this->oRoute = $oRoute;
	}

	/**
	 * @return CacheDefVO
	 */
	public function getCacheDefinition() {
		$def = new CacheDefVO();
		try {
			$def->dir = path_join( $this->oRoute->getWorkingDir(), 'cache' );
			$def->expiration = (int)$this->expiration;
			$def->touch_on_load = (bool)$this->is_touch;
			if ( !empty( $this->request_file ) ) {
				$def->file_fragment = Paths::AddExt( $this->request_file, 'json' );
			}
		}
		catch ( \Exception $e ) {
		}
		return $def;
	}
}