<?php declare( strict_types=1 );

namespace FernleafSystems\Wordpress\Plugin\Foundation\Updates\Upgraders;

use FernleafSystems\Utilities\Logic\ExecOnce;
use FernleafSystems\Wordpress\Plugin\Foundation\{
	Configuration,
	Root,
	Utility
};
use FernleafSystems\Wordpress\Services\Utilities\URL;
use YahnisElsts\PluginUpdateChecker\v5\PucFactory;

class AptoWeb {

	use Configuration\PluginController\ConfigConsumer;
	use Root\RootFileConsumer;
	use Utility\PrefixConsumer;
	use ExecOnce;

	public const BASE_UPDATES_URL = 'https://wpupdates.fernleafsystems.com';

	protected function canRun() :bool {
		return !empty( $this->getConfig()->getUpgradeSettings()[ 'aptoweb' ] );
	}

	protected function run() {
		PucFactory::buildUpdateChecker(
			URL::Build( self::BASE_UPDATES_URL, [
				'action' => 'get_metadata',
				'slug'   => $this->getPrefix()->getPluginPrefix(),
			] ),
			$this->getRootFile()->getFullPath(),
			$this->getPrefix()->getPluginPrefix()
		);
	}
}