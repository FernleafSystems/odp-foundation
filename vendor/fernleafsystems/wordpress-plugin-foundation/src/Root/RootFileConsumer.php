<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Root;

/**
 * Class RootFileConsumer
 * @package FernleafSystems\Wordpress\Plugin\Foundation\Root
 */
trait RootFileConsumer {

	/**
	 * @var File
	 */
	private $oRootFile;

	/**
	 * @return File
	 */
	public function getRootFile() {
		return $this->oRootFile;
	}

	/**
	 * @param File $oRootFile
	 * @return $this
	 */
	public function setRootFile( $oRootFile ) {
		$this->oRootFile = $oRootFile;
		return $this;
	}
}