<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Databases\Base;

/**
 * Trait EntryConsumer
 * @package FernleafSystems\Wordpress\Plugin\Foundation\Databases\Base
 */
trait EntryConsumer {

	/**
	 * @var EntryVO|mixed
	 */
	private $oEntry;

	/**
	 * @return EntryVO|mixed
	 */
	public function getEntryVO() {
		return $this->oEntry;
	}

	/**
	 * @param EntryVO $oE
	 * @return $this
	 */
	public function setEntryVO( $oE ) {
		$this->oEntry = $oE;
		return $this;
	}
}