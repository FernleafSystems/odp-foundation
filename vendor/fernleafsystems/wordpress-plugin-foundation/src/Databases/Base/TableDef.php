<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Databases\Base;

use FernleafSystems\Wordpress\Plugin\Foundation\Module\Base;

/**
 * Class TableDef
 * @package FernleafSystems\Wordpress\Plugin\Foundation\Databases\Base
 */
abstract class TableDef {

	use Base\Traits\ModConsumer;

	/**
	 * The defined table columns.
	 * @var array
	 */
	private $colsDef;

	/**
	 * @var string
	 */
	private $sqlCreate;

	/**
	 * @return string[]
	 */
	public function enumerateColumns() {
		return array_merge(
			$this->getColumn_ID(),
			$this->getColumnsAsArray(),
			$this->getColumns_Ats()
		);
	}

	/**
	 * key - column name; value: column definition
	 * @return string[]
	 */
	public function getColumnsDefinition() {
		return is_array( $this->colsDef ) ? $this->colsDef : $this->getDefaultColumnsDefinition();
	}

	/**
	 * @return string
	 */
	public function getSqlCreate() {
		return empty( $this->sqlCreate ) ? $this->getDefaultCreateTableSql() : $this->sqlCreate;
	}

	/**
	 * @return string
	 */
	abstract public function getTableSlug() :string;

	/**
	 * @return string[]
	 */
	protected function getColumnsAsArray() {
		return [];
	}

	/**
	 * @return string[]
	 */
	protected function getColumn_ID() {
		return [
			'id' => 'int(11) UNSIGNED NOT NULL AUTO_INCREMENT',
		];
	}

	/**
	 * @return string[]
	 */
	protected function getColumns_Ats() {
		return [
			'created_at' => "int(15) UNSIGNED NOT NULL DEFAULT 0",
			'deleted_at' => "int(15) UNSIGNED NOT NULL DEFAULT 0",
		];
	}

	/**
	 * @return strinG
	 */
	protected function getPrimaryKeySpec() {
		return 'PRIMARY KEY  (id)';
	}

	/**
	 * @return string[]
	 */
	protected function getDefaultColumnsDefinition() {
		return [];
	}

	/**
	 * @return string
	 */
	protected function getDefaultCreateTableSql() {
		$cols = [];
		foreach ( $this->enumerateColumns() as $col => $def ) {
			$cols[] = sprintf( '%s %s', $col, $def );
		}
		$cols[] = $this->getPrimaryKeySpec();

		return "CREATE TABLE %s (
			".implode( ", ", $cols )."
		) %s;";
	}

	/**
	 * @param string[] $cols
	 * @return $this
	 */
	public function setColumnsDefinition( array $cols ) {
		$this->colsDef = $cols;
		return $this;
	}

	/**
	 * @param string $sql
	 * @return $this
	 */
	public function setSqlCreate( string $sql ) {
		$this->sqlCreate = $sql;
		return $this;
	}
}