<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Databases\Base;

use FernleafSystems\Wordpress\Plugin\Foundation\Module\Base;
use FernleafSystems\Wordpress\Services\Services;

/**
 * Class Handler
 * @package FernleafSystems\Wordpress\Plugin\Foundation\Databases\Base
 */
class Handler {

	use Base\Traits\ModConsumer;

	/**
	 * @var TableDef|mixed
	 */
	private $def;

	/**
	 * The defined table columns.
	 * @var array
	 * @deprecated 0.8.0
	 */
	protected $aColDef;

	/**
	 * The actual table columns.
	 * @var array
	 */
	protected $aColActual;

	/**
	 * @var string
	 * @deprecated 0.8.0
	 */
	protected $sTable;

	/**
	 * @var bool
	 */
	private $bIsReady;

	/**
	 * @var string
	 * @deprecated 0.8.0
	 */
	private $sSqlCreate;

	public function __construct() {
	}

	public function autoCleanDb() {
	}

	/**
	 * @param int $nTimeStamp
	 * @return bool
	 */
	public function deleteRowsOlderThan( $nTimeStamp ) {
		$bSuccess = false;
		try {
			if ( $this->isReady() ) {
				$bSuccess = $this->getQueryDeleter()
								 ->addWhereOlderThan( $nTimeStamp )
								 ->query();
			}
		}
		catch ( \Exception $e ) {
		}
		return $bSuccess;
	}

	/**
	 * @return string[]
	 */
	public function getColumnsActual() {
		if ( empty( $this->aColActual ) ) {
			$this->aColActual = Services::WpDb()->getColumnsForTable( $this->getTable(), 'strtolower' );
		}
		return is_array( $this->aColActual ) ? $this->aColActual : [];
	}

	/**
	 * @return string[]
	 */
	public function getColumnsDefinition() {
		return $this->hasTableDef() ?
			$this->getTableDef()->getColumnsDefinition() : $this->getDefaultColumnsDefinition();
	}

	/**
	 * @return string
	 */
	public function getTable() {
		$slug = $this->hasTableDef() ?
			$this->getTableDef()->getTableSlug() : $this->getDefaultTableName();
		return Services::WpDb()->getPrefix().esc_sql( $slug );
	}

	/**
	 * @return string
	 * @deprecated 0.8.0
	 */
	protected function getTableSlug() {
		return $this->hasTableDef() ?
			$this->getTableDef()->getTableSlug() : $this->getDefaultTableName();
	}

	/**
	 * @return TableDef|mixed
	 */
	public function getTableDef() {
		$class = $this->getNamespace().'TableDef';
		if ( !isset( $this->def ) && @class_exists( $class ) ) {
			$this->def = new $class();
			$this->def->setMod( $this->getMod() );
		}
		return $this->def;
	}

	public function hasTableDef() :bool {
		return $this->getTableDef() instanceof TableDef;
	}

	/**
	 * @return Insert
	 */
	public function getQueryInserter() {
		$class = $this->getNamespace().'Insert';
		/** @var Insert $o */
		$o = new $class();
		return $o->setDbH( $this );
	}

	/**
	 * @return Delete
	 */
	public function getQueryDeleter() {
		$class = $this->getNamespace().'Delete';
		/** @var Delete $o */
		$o = new $class();
		return $o->setDbH( $this );
	}

	/**
	 * @return Iterator|mixed
	 */
	public function getIterator() {
		$class = $this->getNamespace().'Iterator';
		/** @var Iterator $o */
		$o = new $class();
		return $o->setDbHandler( $this );
	}

	/**
	 * @return Select
	 */
	public function getQuerySelector() {
		$class = $this->getNamespace().'Select';
		/** @var Select $o */
		$o = new $class();
		return $o->setDbH( $this )
				 ->setResultsAsVo( true );
	}

	/**
	 * @return Update
	 */
	public function getQueryUpdater() {
		$class = $this->getNamespace().'Update';
		/** @var Update $o */
		$o = new $class();
		return $o->setDbH( $this );
	}

	/**
	 * @return EntryVO
	 */
	public function getVo() {
		$class = $this->getNamespace().'EntryVO';
		return new $class();
	}

	/**
	 * @param bool $bReTest
	 * @return bool
	 */
	public function isReady( $bReTest = false ) {
		if ( $bReTest ) {
			$this->reset();
		}

		if ( !isset( $this->bIsReady ) ) {
			try {
				$this->bIsReady = $this->tableExists() && $this->verifyTableStructure();
			}
			catch ( \Exception $e ) {
				$this->bIsReady = false;
			}
		}

		return $this->bIsReady;
	}

	/**
	 * @param string[] $aColumns
	 * @return $this
	 * @deprecated 0.8.0
	 */
	public function setColumnsDefinition( $aColumns ) {
		$this->aColDef = $aColumns;
		return $this;
	}

	/**
	 * @param string $sSqlCreate
	 * @return $this
	 * @deprecated 0.8.0
	 */
	public function setSqlCreate( $sSqlCreate ) {
		$this->sSqlCreate = $sSqlCreate;
		return $this;
	}

	/**
	 * @param string $sTable
	 * @return $this
	 * @deprecated 0.8.0
	 */
	public function setTable( $sTable ) {
		$this->sTable = $sTable;
		return $this;
	}

	/**
	 * @return string[]
	 */
	protected function getDefaultColumnsDefinition() {
		return [];
	}

	/**
	 * @return string
	 * @deprecated 0.8.0
	 */
	protected function getDefaultCreateTableSql() {
		return $this->hasTableDef() ? $this->getTableDef()->getSqlCreate() : '';
	}

	/**
	 * @return $this
	 */
	private function reset() {
		unset( $this->bIsReady );
		unset( $this->aColActual );
		return $this;
	}

	/**
	 * @param string $col
	 * @return bool
	 */
	public function hasColumn( string $col ) {
		return in_array( strtolower( $col ), array_map( 'strtolower', $this->getColumnsActual() ) );
	}

	/**
	 * @param int $nAutoExpireDays
	 * @return $this
	 */
	public function tableCleanExpired( $nAutoExpireDays ) {
		$nAutoExpire = $nAutoExpireDays*DAY_IN_SECONDS;
		if ( $nAutoExpire > 0 ) {
			$this->deleteRowsOlderThan( Services::Request()->ts() - $nAutoExpire );
		}
		return $this;
	}

	/**
	 * @return $this
	 * @throws \Exception
	 */
	protected function tableCreate() {
		$sql = $this->hasTableDef() ? $this->getTableDef()->getSqlCreate() : $this->getDefaultCreateTableSql();
		if ( empty( $sql ) ) {
			throw new \Exception( 'Table Create SQL is empty' );
		}
		$sql = sprintf( $sql, $this->getTable(), Services::WpDb()->getCharCollate() );
		Services::WpDb()->dbDelta( $sql );
		return $this;
	}

	/**
	 * @param bool $bTruncate
	 * @return bool
	 */
	public function tableDelete( $bTruncate = false ) :bool {
		$table = $this->getTable();
		$DB = Services::WpDb();
		$mResult = !$this->tableExists() ||
				   ( $bTruncate ? $DB->doTruncateTable( $table ) : $DB->doDropTable( $table ) );
		$this->reset();
		return $mResult !== false;
	}

	public function tableExists() :bool {
		return Services::WpDb()->tableExists( $this->getTable() );
	}

	/**
	 * @return $this
	 * @throws \Exception
	 */
	public function tableInit() {
		if ( !$this->isReady() ) {

			$this->tableCreate();

			if ( !$this->isReady( true ) ) {
				$this->tableDelete();
				$this->tableCreate();
			}
		}
		return $this;
	}

	/**
	 * @param int $nRowsLimit
	 * @return $this
	 */
	public function tableTrimExcess( $nRowsLimit ) {
		try {
			$this->getQueryDeleter()
				 ->deleteExcess( $nRowsLimit );
		}
		catch ( \Exception $e ) {
		}
		return $this;
	}

	/**
	 * @return string
	 */
	protected function getDefaultTableName() {
		return '';
	}

	/**
	 * @return bool
	 * @throws \Exception
	 */
	protected function verifyTableStructure() {
		$aColDef = array_map( 'strtolower', $this->getColumnsDefinition() );
		if ( empty( $aColDef ) ) {
			throw new \Exception( 'Could not verify table structure as no columns definition provided' );
		}

		$aColActual = $this->getColumnsActual();
		return ( count( array_diff( $aColActual, $aColDef ) ) <= 0
				 && ( count( array_diff( $aColDef, $aColActual ) ) <= 0 ) );
	}

	/**
	 * @return string
	 */
	private function getNamespace() {
		try {
			$sNS = ( new \ReflectionClass( $this ) )->getNamespaceName();
		}
		catch ( \Exception $oE ) {
			$sNS = __NAMESPACE__;
		}
		return rtrim( $sNS, '\\' ).'\\';
	}

	/**
	 * @return bool
	 * @deprecated 0.8.0
	 */
	public function deleteTable() {
		return $this->tableDelete();
	}

	/**
	 * @return bool
	 * @deprecated 0.8.0
	 */
	public function isTable() {
		return $this->tableExists();
	}

	/**
	 * @param int $nAutoExpireDays
	 * @return $this
	 * @deprecated 0.8.0
	 */
	public function cleanDb( $nAutoExpireDays ) {
		return $this->tableCleanExpired( $nAutoExpireDays );
	}

	/**
	 * @param int $nRowsLimit
	 * @return $this
	 * @deprecated 0.8.0
	 */
	public function trimDb( $nRowsLimit ) {
		return $this->tableTrimExcess( $nRowsLimit );
	}

	/**
	 * @return string
	 * @deprecated 0.8.0
	 */
	public function getSqlCreate() :string {
		return $this->hasTableDef() ? $this->getTableDef()->getSqlCreate() : $this->getDefaultCreateTableSql();
	}
}