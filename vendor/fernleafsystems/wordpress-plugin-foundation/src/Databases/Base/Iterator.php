<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Databases\Base;

use Elliotchance\Iterator\AbstractPagedIterator;
use FernleafSystems\Wordpress\Plugin\Foundation\Databases;

class Iterator extends AbstractPagedIterator {

	const PAGE_LIMIT = 50;
	use HandlerConsumer;

	/**
	 * @var Select|mixed
	 */
	private $oSelector;

	/**
	 * @var int
	 */
	private $nTotalSize;

	/**
	 * @var array
	 */
	private $aCustomFilters;

	/**
	 * @return EntryVO|mixed|null
	 */
	public function current() {
		return parent::current();
	}

	/**
	 * @return array
	 */
	public function getCustomQueryFilters() :array {
		return is_array( $this->aCustomFilters ) ? $this->aCustomFilters : [];
	}

	/**
	 * @return array
	 */
	protected function getDefaultQueryFilters() :array {
		return [
			'orderby' => 'id',
			'order'   => 'ASC',
		];
	}

	/**
	 * @return array
	 */
	protected function getFinalQueryFilters() :array {
		return array_merge( $this->getDefaultQueryFilters(), $this->getCustomQueryFilters() );
	}

	/**
	 * @param int $nPage - always starts at 0
	 * @return array
	 */
	public function getPage( $nPage ) {
		$aParams = $this->getFinalQueryFilters();

		$this->getSelector()
			 ->setResultsAsVo( true )
			 ->setPage( $nPage + 1 ) // Pages start at 1, not zero.
			 ->setLimit( $this->getPageSize() )
			 ->setOrderBy( $aParams[ 'orderby' ], $aParams[ 'order' ] );

		return $this->runQuery();
	}

	/**
	 * @return int
	 */
	public function getPageSize() {
		return static::PAGE_LIMIT;
	}

	/**
	 * @return Select|mixed
	 */
	public function getSelector() {
		if ( empty( $this->oSelector ) ) {
			$this->oSelector = $this->getDbHandler()->getQuerySelector();
		}
		return $this->oSelector;
	}

	/**
	 * @return int
	 */
	public function getTotalSize() {
		if ( !isset( $this->nTotalSize ) ) {
			$this->nTotalSize = $this->runQueryCount();
		}
		return $this->nTotalSize;
	}

	/**
	 * @return EntryVO[]|mixed[]
	 */
	protected function runQuery() {
		return ( clone $this->getSelector() )->query();
	}

	/**
	 * @return int
	 */
	protected function runQueryCount() {
		return ( clone $this->getSelector() )->count();
	}

	/**
	 * @param Select|mixed $oSelector
	 * @return $this
	 */
	public function setSelector( $oSelector ) :self {
		$this->oSelector = $oSelector;
		return $this;
	}
}