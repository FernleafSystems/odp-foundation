<?php

namespace FernleafSystems\Wordpress\Plugin\Foundation\Databases\Base;

use FernleafSystems\Wordpress\Plugin\Foundation\Databases\Util\SetRecordDataTypes;
use FernleafSystems\Wordpress\Services\Services;

/**
 * Class Select
 * @package FernleafSystems\Wordpress\Plugin\Foundation\Databases\Base
 */
class Select extends BaseQuery {

	/**
	 * @var array
	 */
	protected $aColumnsToSelect;

	/**
	 * @var bool
	 */
	protected $bIsCount = false;

	/**
	 * @var bool
	 */
	protected $bIsSum = false;

	/**
	 * @var bool
	 */
	protected $bIsDistinct = false;

	/**
	 * @var bool
	 */
	protected $bResultsAsVo;

	/**
	 * @var string
	 */
	protected $sCustomSelect;

	/**
	 * @var string
	 */
	protected $sResultFormat;

	/**
	 * @param string $sCol
	 * @return $this
	 */
	public function addColumnToSelect( $sCol ) {
		$aCols = $this->getColumnsToSelect();
		$aCols[] = $sCol;
		return $this->setColumnsToSelect( $aCols );
	}

	/**
	 * @return array[]|int|string[]|EntryVO[]|mixed
	 */
	public function all() {
		return $this->reset()->query();
	}

	/**
	 * @param int $nId
	 * @return \stdClass
	 */
	public function byId( $nId ) {
		$aItems = $this->reset()
					   ->addWhereEquals( 'id', $nId )
					   ->query();
		return array_shift( $aItems );
	}

	public function buildQuery() :string {
		return sprintf( $this->getBaseQuery(),
			$this->buildSelect(),
			$this->getDbH()->getTable(),
			$this->buildWhere(),
			$this->buildExtras()
		);
	}

	/**
	 * @return string
	 */
	protected function buildSelect() {
		$aCols = $this->getColumnsToSelect();

		if ( $this->isCount() ) {
			$sSubstitute = 'COUNT(*)';
		}
		elseif ( $this->isSum() ) {
			$sSubstitute = sprintf( 'SUM(%s)', array_shift( $aCols ) );
		}
		elseif ( $this->isDistinct() && $this->hasColumnsToSelect() ) {
			$sSubstitute = sprintf( 'DISTINCT %s', implode( ',', $aCols ) );
		}
		elseif ( $this->hasColumnsToSelect() ) {
			$sSubstitute = implode( ',', $aCols );
		}
		elseif ( $this->isCustomSelect() ) {
			$sSubstitute = $this->sCustomSelect;
		}
		else {
			$sSubstitute = '*';
		}
		return $sSubstitute;
	}

	/**
	 * @return int
	 */
	public function count() {
		return (int)$this->setIsCount( true )->query();
	}

	/**
	 * @return int
	 */
	public function sum() {
		return $this->setIsSum( true )->query();
	}

	/**
	 * @return EntryVO|\stdClass|mixed|null
	 */
	public function first() {
		$aR = $this->setLimit( 1 )->query();
		return empty( $aR ) ? null : array_shift( $aR );
	}

	protected function getBaseQuery() :string {
		return "SELECT %s FROM `%s` WHERE %s %s";
	}

	public function getColumnsToSelect() :array {
		return is_array( $this->aColumnsToSelect ) ? $this->aColumnsToSelect : [];
	}

	/**
	 * @param string $sColumn
	 * @return array
	 */
	public function getDistinctForColumn( string $sColumn ) {
		return $this->reset()
					->addColumnToSelect( $sColumn )
					->setIsDistinct( true )
					->query();
	}

	/**
	 * @param string $sColumn
	 * @return array
	 */
	protected function getDistinct_FilterAndSort( string $sColumn ) {
		$a = array_filter( $this->getDistinctForColumn( $sColumn ) );
		natcasesort( $a );
		return $a;
	}

	protected function getSelectDataFormat() :string {
		if ( $this->isResultsAsVo() ) {
			$sForm = ARRAY_A;
		}
		else {
			$sForm = in_array( $this->sResultFormat, [ OBJECT_K, ARRAY_A ] ) ? $this->sResultFormat : OBJECT_K;
		}
		return $sForm;
	}

	protected function hasColumnsToSelect() :bool {
		return ( count( $this->getColumnsToSelect() ) > 0 );
	}

	public function isCount() :bool {
		return (bool)$this->bIsCount;
	}

	public function isSum() :bool {
		return (bool)$this->bIsSum;
	}

	public function isCustomSelect() :bool {
		return !empty( $this->sCustomSelect );
	}

	public function isDistinct() :bool {
		return (bool)$this->bIsDistinct;
	}

	public function isResultsAsVo() :bool {
		return $this->bResultsAsVo && !$this->isSum();
	}

	/**
	 * Handle COUNT, DISTINCT, & normal SELECT
	 * @return int|string[]|array[]|EntryVO[]|mixed
	 */
	public function query() {
		if ( $this->isCount() || $this->isSum() ) {
			$mData = $this->queryVar();
		}
		elseif ( $this->isDistinct() ) {
			$mData = $this->queryDistinct();
			if ( is_array( $mData ) ) {
				$mData = array_map( function ( $aRecord ) {
					return array_shift( $aRecord );
				}, $mData );
			}
			else {
				$mData = [];
			}
		}
		else {
			$mData = $this->querySelect();
			if ( $this->isResultsAsVo() ) {

				$mData = array_map( function ( $record ) {
					return $this->getDbH()->getVo()->applyFromArray( $record );
				}, $mData );

				if ( $this->getDbH()->hasTableDef() ) {
					( new SetRecordDataTypes() )
						->setForRecords(
							$mData,
							array_map( function ( $def ) {
								return strtolower( trim( substr( $def, 0, strpos( $def, '(' ) ) ) );
							}, $this->getDbH()->getTableDef()->enumerateColumns() )
						);
				}
			}
		}

		$this->reset();
		return $mData;
	}

	private function setDataTypes() {

	}

	/**
	 * @return array[]
	 */
	protected function querySelect() {
		return Services::WpDb()->selectCustom( $this->buildQuery(), $this->getSelectDataFormat() );
	}

	/**
	 * @return int
	 */
	protected function queryVar() {
		return Services::WpDb()->getVar( $this->buildQuery() );
	}

	/**
	 * @return array[]
	 */
	protected function queryDistinct() {
		return Services::WpDb()->selectCustom( $this->buildQuery() );
	}

	/**
	 * @return $this
	 */
	public function reset() :self {
		parent::reset();
		return $this->setIsCount( false )
					->setIsDistinct( false )
					->setGroupBy( '' )
					->setSelectResultsFormat( '' )
					->setCustomSelect( '' )
					->setColumnsToSelect( [] )
					->clearWheres();
	}

	/**
	 * Verifies the given columns are valid and unique
	 * @param string[] $aColumns
	 * @return $this
	 */
	public function setColumnsToSelect( $aColumns ) {
		if ( is_array( $aColumns ) ) {
			$this->aColumnsToSelect = array_intersect(
				$this->getDbH()->getColumnsActual(),
				array_map( 'strtolower', $aColumns )
			);
		}
		return $this;
	}

	/**
	 * @param string $sSelect
	 * @return $this
	 */
	public function setCustomSelect( $sSelect ) {
		$this->sCustomSelect = $sSelect;
		return $this;
	}

	/**
	 * @param bool $bIsCount
	 * @return $this
	 */
	public function setIsCount( $bIsCount ) {
		$this->bIsCount = $bIsCount;
		return $this;
	}

	/**
	 * @param bool $bSum
	 * @return $this
	 */
	public function setIsSum( $bSum ) {
		$this->bIsSum = $bSum;
		return $this;
	}

	/**
	 * @param bool $bIsDistinct
	 * @return $this
	 */
	public function setIsDistinct( $bIsDistinct ) {
		$this->bIsDistinct = $bIsDistinct;
		return $this;
	}

	/**
	 * @param bool $bResultsAsVo
	 * @return $this
	 */
	public function setResultsAsVo( $bResultsAsVo ) {
		$this->bResultsAsVo = $bResultsAsVo;
		return $this;
	}

	/**
	 * @param string $sFormat
	 * @return $this
	 */
	public function setSelectResultsFormat( $sFormat ) {
		$this->sResultFormat = $sFormat;
		return $this;
	}
}