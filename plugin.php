<?php declare( strict_types=1 );
/*
 * Plugin Name: [Apto] Foundation
 * Plugin URI: https://icwp.io/bv
 * Description: Apto Plugin's Foundation Classes
 * Version: 2.6.5
 * Text Domain: odp-foundation
 * Domain Path: /languages/
 * Author: AptoWeb
 * Author URI: https://icwp.io/bv
 * Requires at least: 6.2
 * Requires PHP: 8.1
 */

if ( \version_compare( \PHP_VERSION, '8.1', '>=' ) ) {
	\call_user_func( function () {
		$rootFile = __FILE__;
		require_once( __DIR__.'/init.php' );
	} );
}
else {
	error_log( 'APTO Foundation now requires PHP 8.1+' );
}